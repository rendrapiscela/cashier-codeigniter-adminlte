<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('transaksi_model');
		$this->load->model('produk_model');
		$this->load->model('stok_keluar_model');
		$this->load->model('transaksihutang_model');
		$this->load->model('pelanggan_model');
		$this->load->model('transaksihold_model');
	}

	public function index()
	{
		$this->load->view('transaksi');
	}

	public function scan()
	{
		$data['produk'] = $this->produk_model->read()->result();
		$this->load->view('transaksi_scan', $data);
	}

	public function getbarcode()
	{
		header('Content-type: application/json');
		$data = $this->produk_model->getBarcodeRow($_GET['kode_barang'], $_GET['kategori_harga']);
		if(empty($data)){ //barang tidak terdeteksi
			$data = array('nama_produk' => 'Barang Belum Terdaftar', 'stok' => '-', 'id' => '');
		}		
		echo json_encode($data);
	}

	public function read()
	{
		// header('Content-type: application/json');
		if ($this->transaksi_model->read()->num_rows() > 0) {
			$i=1;
			foreach ($this->transaksi_model->read()->result() as $transaksi) {
				$barcode = explode(',', $transaksi->barcode);
				$tanggal = new DateTime($transaksi->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'nama_produk' => '<table>'.$this->transaksi_model->getProdukNotQty($barcode, $transaksi->qty).'</table>',
					'qty' => '<table>'.$this->transaksi_model->getQty($transaksi->qty).'</table>',
					'total_bayar' => $transaksi->total_bayar,
					'jumlah_uang' => $transaksi->jumlah_uang,
					'diskon' => $transaksi->diskon,
					'pelanggan' => $transaksi->pelanggan.'<table><tr><td><b>Ket : </b>'.$transaksi->keterangan.'</td></tr></table>',
					'action' => '<a class="btn btn-sm btn-success" target="_blank" href="'.site_url('transaksi/cetak/').$transaksi->id.'">Print</a> <button style="display:none;" class="btn btn-sm btn-danger" onclick="remove('.$transaksi->id.')">Delete</button>'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$transaksi = array(
			'data' => $data
		);
		echo json_encode($transaksi);
	}

	public function add()
	{
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah
			//check stok sekarang
			$rowStok = $this->produk_model->getStok($id);
			$stokSekarang = $rowStok->stok;
			$stokUpdate = $stokSekarang - $produk->terjual;

			$totalTerjual = $rowStok->terjual + $produk->terjual; //akumulasi

			$this->transaksi_model->removeStok($id, $stokUpdate);
			$this->transaksi_model->addTerjual($id, $totalTerjual);
			array_push($barcode, $id);

			//mapping insert stok
			$stokKeluar = array(
				'tanggal' => date('Y-m-d H:i:s'),
				'barcode' => $id,
				'jumlah' => $produk->terjual,
				'keterangan' => 'penjualan',
				'outlet_id' => $this->session->userdata('outlet_id'),
				'pengguna_id' => $this->session->userdata('id')
			);
			array_push($insertStok, $stokKeluar);
		}
		//insert ke stok
		$this->db->insert_batch('stok_keluar',$insertStok);
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$subtotalDiskon = $this->input->post('subtotal_diskon');
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			'total_bayar' => $this->input->post('total_bayar'),
			'jumlah_uang' => $this->input->post('jumlah_uang'),
			'diskon' => $this->input->post('diskon'),
			'pelanggan' => $this->input->post('pelanggan'),
			'nota' => $this->input->post('nota'),
			'keterangan' => $this->input->post('keterangan'),
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			'total_persen' => $subtotalDiskon,
			'kasir' => $this->session->userdata('id')
		);
		if ($id = $this->transaksi_model->create($data)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($this->transaksi_model->delete($id)) {
			echo json_encode('sukses');
		}
	}
	public function hargaSetelahDiskon($harga, $qty, $persenDiskon, $persenDiskonHarga){
		//cek persenan bukan
		if(strpos($persenDiskon, '%') !== false){
			return $harga;
		}else{
			//hitung harga satuan
			if($persenDiskon > 0){
				return ceil($persenDiskonHarga / $qty);
			}
		}
		return $harga;
	}

	public function cetak($id)
	{
		$produk = $this->transaksi_model->getAll($id);
		
		$tanggal = new DateTime($produk->tanggal);
		$barcode = explode(',', $produk->barcode);
		$qty = explode(',', $produk->qty);
		$harga = explode(',', $produk->harga);
		$produk->tanggal = $tanggal->format('d M Y H:i:s');
		$persenDiskon = convertToArray($produk->persen_diskon);
		$persenDiskonHarga = convertToArray($produk->persen_diskon_harga);

		$pelanggan = $this->pelanggan_model->getById($produk->pelanggan);
		$namaPelanggan = "Customer";
		if($pelanggan->num_rows() > 0){
			$pel = $pelanggan->row();
			$namaPelanggan = (($pel->jenis_kelamin == 'Pria') ? 'Bpk. ' : 'Ibu. ').$pel->nama;
		}
		$dataProduk = $this->transaksi_model->getName($barcode);
		foreach ($dataProduk as $key => $value) {
			$value->total = $qty[$key];
			$value->harga_asli = $value->harga * $qty[$key];
			$value->harga_real = $harga[$key];
			$value->harga = $this->hargaSetelahDiskon($harga[$key],$qty[$key], $persenDiskon[$key], $persenDiskonHarga[$key]);
			$value->persendiskon = $persenDiskon[$key];
			$value->persendiskonharga = $persenDiskonHarga[$key];
			$diskon = false;
			if (($value->harga * $value->total) < $value->harga_asli){
				$diskon = true;
			}
			if(strpos($persenDiskon[$key], '%') !== false){
				$diskon = true;
				if(strpos($persenDiskon[$key], '0%') !== false && strlen($persenDiskon[$key]) == 2){
					$diskon = false;
				}
			}
			$value->diskon = $diskon;
		}

		$hutang = $this->transaksihutang_model->getById($id);
		$cicilan = array();
		if($hutang->num_rows() > 0){
			$cicilan = $hutang->result_array();	
		}
		$data = array(
			'nota' => $produk->nota,
			'tanggal' => $produk->tanggal,
			'produk' => $dataProduk,
			'total' => $produk->total_bayar,
			'bayar' => $produk->jumlah_uang,
			'kembalian' => $produk->jumlah_uang - ($produk->total_bayar-$produk->diskon),
			'kasir' => $produk->kasir,
			'pelanggan' => $namaPelanggan,
			'cicilan' => $cicilan,
			'diskon' => ($produk->diskon) ? $produk->diskon : 0,
			'total_persen' => ($produk->total_persen) ? $produk->total_persen : 0,
			'keterangan' => $produk->keterangan
		);
		$this->load->view('cetak', $data);
	}

	public function penjualan_bulan()
	{
		header('Content-type: application/json');
		$day = $this->input->post('day');
		foreach ($day as $key => $value) {
			$now = date($day[$key].'-m-Y');
			if ($qty = $this->transaksi_model->penjualanBulan($now) !== []) {
				$data[] = array_sum($this->transaksi_model->penjualanBulan($now));
			} else {
				$data[] = 0;
			}
		}
		echo json_encode($data);
	}

	public function transaksi_hari()
	{
		header('Content-type: application/json');
		$now = date('d m Y');
		$total = $this->transaksi_model->transaksiHari($now);
		echo json_encode($total);
	}

	public function transaksi_terakhir($value='')
	{
		//total penjualan produk hari ini (pcs)
		header('Content-type: application/json');
		$now = date('d m Y');
		// foreach ($this->transaksi_model->transaksiTerakhir($now) as $key) {
		// 	$total = explode(',', $key);
		// }
		$row = $this->transaksi_model->transaksiTerakhir($now);
		echo json_encode("Rp. ".rupiah($row->total_penjualan));
	}

	public function cicilan(){
		$data = array();
		$data['transaksi_id'] = $this->input->post('transaksi_id');
		$data['total_bayar'] = $this->input->post('jumlah_uang');
		$sisaBayar = 0;
		$statusBayar = 1;
		if($this->input->post('jumlah_uang') < $this->input->post('sisa_bayar')){
			$sisaBayar = $this->input->post('sisa_bayar') - $this->input->post('jumlah_uang');
			$statusBayar = 2;
		}
		if($statusBayar == 1){
			//update di tabel transaksi
			$update = array();
			$update['status_bayar'] = $statusBayar;
			$this->transaksi_model->update($update, $this->input->post('transaksi_id'));
		}
		$data['sisa_bayar'] = $sisaBayar;
		$data['keterangan'] = $this->input->post('keterangan');
		$data['cicilan_ke'] = ($this->input->post('cicilan_ke') + 1);
		$data['status_bayar'] = $statusBayar;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata('id');
	
		if($this->transaksihutang_model->create($data)){
			echo json_encode($this->db->insert_id());
		}
	}

	public function detail_cicilan($idTransaksi){
		$transaksi = $this->transaksi_model->readbyid($idTransaksi);
		$totalHutang = number_format(($transaksi->total_bayar), 0, '.','.');
		$data = $this->transaksihutang_model->getById($idTransaksi);
		$template = "";
		if($data->num_rows() > 0){
			foreach($data->result() as $row){
				$template.="<tr>";
				$template.="<td>".$row->created_at."</td>";
				$template.="<td style='text-align:center;'>".$row->cicilan_ke."</td>";
				$template.="<td style='text-align:right;'>Rp. ".number_format($row->total_bayar,0,'.','.')."</td>";
				$template.="<td style='text-align:right;'><span class='badge bg-warning'>Rp. ".number_format($row->sisa_bayar,0,'.','.')."</span></td>";
				$template.="<td>".$row->keterangan."</td>";
				$template.="</tr>";
			}
		}
		$return = ['history' => $template, 'total_hutang' => $totalHutang." | Uang Masuk <span class='badge bg-success'>Rp. ".number_format($transaksi->jumlah_uang,0,'.','.')."</span> | Diskon <span class='badge bg-danger'>Rp.".number_format($transaksi->diskon,0,'.','.')."</span>"];
		echo json_encode($return);
	}

	public function sisa_cicilan($idTransaksi){
		$data = $this->transaksihutang_model->getSisaCicilan($idTransaksi)->row();
		$data->rp_sisa_bayar = 'Rp.'.number_format($data->sisa_bayar, 0, '.','.');
		$data->rp_total_bayar = 'Rp.'.number_format($data->total_bayar, 0, '.','.');
		$data->rp_total_hutang = 'Rp.'.number_format($data->total_hutang, 0, '.','.');
		echo json_encode($data);
	}


	//====== TRANSKASI HOLD =============
	public function add_hold()
	{
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah

			array_push($barcode, $id);
		}
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			'nota' => $this->input->post('nota'),
			'keterangan' => '',
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			'kasir' => $this->session->userdata('id')
		);
		if ($id = $this->transaksihold_model->create($data)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}
	
	public function datahold(){
		$this->load->view('datahold');
	}

	public function read_datahold(){		

		if ($this->transaksihold_model->read()->num_rows() > 0) {
			$i=1;
			foreach ($this->transaksihold_model->read()->result() as $transaksi) {
				$barcode = explode(',', $transaksi->barcode);
				$tanggal = new DateTime($transaksi->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'nota' => $transaksi->nota,
					'nama_produk' => '<table>'.$this->transaksi_model->getProdukNotQty($barcode, $transaksi->qty).'</table>',
					'qty' => '<table>'.$this->transaksi_model->getQty($transaksi->qty).'</table>',
					'action' =>  '<button class="btn btn-sm btn-danger" onclick="remove('.$transaksi->id.')"><i class="fas fa-remove"></i> Hapus</button> <button class="btn btn-sm btn-primary" onclick="lanjutTransaksi('.$transaksi->id.')">Lanjut Transaksi</button>'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$transaksi = array(
			'data' => $data
		);
		echo json_encode($transaksi);
	}

	public function delete_hold()
	{
		$id = $this->input->post('id');
		if ($this->transaksihold_model->remove($id)) {
			echo json_encode('sukses');
		}
	}

	public function edit_transaksi_hold($idhold){
		$data = array();
		$data['idhold'] = $idhold;
		$transaksiRow = $this->transaksihold_model->read_byid($idhold);
		$row = $transaksiRow->row();

		$kategoriHarga = convertToArray($row->kategori_harga);
		$barcode = convertToArray($row->barcode);
		$harga = convertToArray($row->harga);
		$qty = convertToArray($row->qty);
		$persenDiskon = convertToArray($row->persen_diskon);
		$persenDiskonHarga = convertToArray($row->persen_diskon_harga);

		$i=0;
		$dataEdit = array();
		$totalTagihan = 0;
		$idProduk = array();
		$jenisDiskon = array();
		$hargaAwal = array();
		foreach($barcode as $code){
			$idProduk[$i] = $barcode[$i];
			//check harga awal
			$kategori = kategori_harga($kategoriHarga[$i]);
			$hargaAwal[$i] = $this->produk_model->getNama($barcode[$i],$kategori);
			
			//check is Rupiah
			$isRupiah = false;
			if(isset($persenDiskon[$i])){
				//cek %
				if(strpos($persenDiskon[$i],'%') !== false){
					$isRupiah = false;
				}else{
					$isRupiah = true;
				}
			}else{
				if(strpos($persenDiskon,'%') !== false){
					$isRupiah = false;
				}else{
					$isRupiah = true;
				}
			}
			$jenisDiskon[$i] = $isRupiah;
			$dataEdit[$i] = array( 
				(isset($kategoriHarga[$i])) ? $kategoriHarga[$i] : $kategoriHarga,
				$this->produk_model->getProdukBarcode($barcode[$i]),
				$this->produk_model->getProdukNama($barcode[$i]),
				(isset($harga[$i])) ? "Rp ".number_format($harga[$i],0,'.','.') : "Rp ".number_format($harga,0,'.','.'),
				(is_array($qty)) ? $qty[$i] : $qty,
				(isset($persenDiskon[$i])) ? $persenDiskon[$i] : $persenDiskon,
				(isset($persenDiskonHarga[$i])) ? "Rp ".number_format($persenDiskonHarga[$i],0,'.','.') : "Rp ".number_format($persenDiskonHarga,0,'.','.')
			);
			$totharga = (isset($harga[$i])) ? $harga[$i] : $harga;
			$totqty = (is_array($qty)) ? $qty[$i] : $qty;
			$totalTagihan += $persenDiskonHarga[$i];
			$i++;
		}
		$data['row'] = $row;
		$data['transaksi'] = $dataEdit;
		$data['totalTagihan'] = $totalTagihan;
		$data['idProduk'] = $idProduk;
		$data['jenis_diskon'] = $jenisDiskon;
		$data['harga_awal'] = $hargaAwal;

		$this->load->view('edit_transaksi_hold', $data);
	}

	public function add_transaksi_hold($idhold)
	{
		//remove data hold
		$this->transaksihold_model->remove($idhold);
		//===end remove 
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah
			//check stok sekarang
			$rowStok = $this->produk_model->getStok($id);
			$stokSekarang = $rowStok->stok;
			$stokUpdate = $stokSekarang - $produk->terjual;

			$totalTerjual = $rowStok->terjual + $produk->terjual; //akumulasi

			$this->transaksi_model->removeStok($id, $stokUpdate);
			$this->transaksi_model->addTerjual($id, $totalTerjual);
			array_push($barcode, $id);

			//mapping insert stok
			$stokKeluar = array(
				'tanggal' => date('Y-m-d H:i:s'),
				'barcode' => $id,
				'jumlah' => $produk->terjual,
				'keterangan' => 'penjualan',
				'outlet_id' => $this->session->userdata('outlet_id'),
				'pengguna_id' => $this->session->userdata('id')
			);
			array_push($insertStok, $stokKeluar);
		}
		//insert ke stok
		$this->db->insert_batch('stok_keluar',$insertStok);
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$subtotalDiskon = $this->input->post('subtotal_diskon');
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			'total_bayar' => $this->input->post('total_bayar'),
			'jumlah_uang' => $this->input->post('jumlah_uang'),
			'diskon' => $this->input->post('diskon'),
			'pelanggan' => $this->input->post('pelanggan'),
			'nota' => $this->input->post('nota'),
			'keterangan' => $this->input->post('keterangan'),
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			'total_persen' => $subtotalDiskon,
			'kasir' => $this->session->userdata('id')
		);
		if ($id = $this->transaksi_model->create($data)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}

	public function update_transaksi_hold($idHold){
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah

			array_push($barcode, $id);
		}
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$subtotalDiskon = $this->input->post('subtotal_diskon');
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			// 'nota' => $this->input->post('nota'),
			'keterangan' => '',
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			// 'total_persen' => $subtotalDiskon,
			'kasir' => $this->session->userdata('id')
		);
		if ($id = $this->transaksihold_model->update($data, $idHold)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}

	public function removeHarga($hargaPost){
		$value = implode(",",$hargaPost);			
		$tes = htmlentities($value);
		$tes = str_replace(" ","",$tes);
		$tes = str_replace("&nbsp;","",$tes);
		$tes = str_replace("Rp","",$tes);
		$tes = str_replace(".","",$tes);
		return $tes;
	}

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
