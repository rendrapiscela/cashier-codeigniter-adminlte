<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revisi_transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('transaksi_model');
		$this->load->model('produk_model');
		$this->load->model('stok_keluar_model');
		$this->load->model('transaksihutang_model');
		$this->load->model('pelanggan_model');
		$this->load->model('transaksihold_model');
		$this->load->model('penggunaoutlet_model');
	}

	public function index()
	{
		$this->load->view('transaksi');
	}

	public function getbarcode()
	{
		header('Content-type: application/json');
		$data = $this->produk_model->getBarcodeRow($_GET['kode_barang'], $_GET['kategori_harga']);
		if(empty($data)){ //barang tidak terdeteksi
			$data = array('nama_produk' => 'Barang Belum Terdaftar', 'stok' => '-', 'id' => '');
		}		
		echo json_encode($data);
	}

	public function read()
	{
		// header('Content-type: application/json');
		if ($this->transaksi_model->read()->num_rows() > 0) {
			$i=1;
			foreach ($this->transaksi_model->read()->result() as $transaksi) {
				$barcode = explode(',', $transaksi->barcode);
				$tanggal = new DateTime($transaksi->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'nama_produk' => '<table>'.$this->transaksi_model->getProdukNotQty($barcode, $transaksi->qty).'</table>',
					'qty' => '<table>'.$this->transaksi_model->getQty($transaksi->qty).'</table>',
					'total_bayar' => $transaksi->total_bayar,
					'jumlah_uang' => $transaksi->jumlah_uang,
					'diskon' => $transaksi->diskon,
					'pelanggan' => $transaksi->pelanggan.'<table><tr><td><b>Ket : </b>'.$transaksi->keterangan.'</td></tr></table>',
					'action' => '<a class="btn btn-sm btn-success" target="_blank" href="'.site_url('transaksi/cetak/').$transaksi->id.'">Print</a> <button style="display:none;" class="btn btn-sm btn-danger" onclick="remove('.$transaksi->id.')">Delete</button>'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$transaksi = array(
			'data' => $data
		);
		echo json_encode($transaksi);
	}

	public function add()
	{
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah
			//check stok sekarang
			$rowStok = $this->produk_model->getStok($id);
			$stokSekarang = $rowStok->stok;
			$stokUpdate = $stokSekarang - $produk->terjual;

			$totalTerjual = $rowStok->terjual + $produk->terjual; //akumulasi

			$this->transaksi_model->removeStok($id, $stokUpdate);
			$this->transaksi_model->addTerjual($id, $totalTerjual);
			array_push($barcode, $id);

			//mapping insert stok
			$stokKeluar = array(
				'tanggal' => date('Y-m-d H:i:s'),
				'barcode' => $id,
				'jumlah' => $produk->terjual,
				'keterangan' => 'penjualan',
				'outlet_id' => $this->session->userdata('outlet_id'),
				'pengguna_id' => $this->session->userdata('id')
			);
			array_push($insertStok, $stokKeluar);
		}
		//insert ke stok
		$this->db->insert_batch('stok_keluar',$insertStok);
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$subtotalDiskon = $this->input->post('subtotal_diskon');
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			'total_bayar' => $this->input->post('total_bayar'),
			'jumlah_uang' => $this->input->post('jumlah_uang'),
			'diskon' => $this->input->post('diskon'),
			'pelanggan' => $this->input->post('pelanggan'),
			'nota' => $this->input->post('nota'),
			'keterangan' => $this->input->post('keterangan'),
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			'total_persen' => $subtotalDiskon,
			'kasir' => $this->session->userdata('id')
		);
		if ($id = $this->transaksi_model->create($data)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($this->transaksi_model->delete($id)) {
			echo json_encode('sukses');
		}
	}

	public function cetak($id)
	{
		$produk = $this->transaksi_model->getAll($id);
		
		$tanggal = new DateTime($produk->tanggal);
		$barcode = explode(',', $produk->barcode);
		$qty = explode(',', $produk->qty);
		$harga = explode(',', $produk->harga);
		$produk->tanggal = $tanggal->format('d M Y H:i:s');

		$pelanggan = $this->pelanggan_model->getById($produk->pelanggan);
		$namaPelanggan = "Customer";
		if($pelanggan->num_rows() > 0){
			$pel = $pelanggan->row();
			$namaPelanggan = (($pel->jenis_kelamin == 'Pria') ? 'Bpk. ' : 'Ibu. ').$pel->nama;
		}
		$dataProduk = $this->transaksi_model->getName($barcode);
		foreach ($dataProduk as $key => $value) {
			$value->total = $qty[$key];
			$value->harga_asli = $value->harga * $qty[$key];
			$value->harga = $harga[$key];
		}

		$hutang = $this->transaksihutang_model->getById($id);
		$cicilan = array();
		if($hutang->num_rows() > 0){
			$cicilan = $hutang->result_array();	
		}
		$data = array(
			'nota' => $produk->nota,
			'tanggal' => $produk->tanggal,
			'produk' => $dataProduk,
			'total' => $produk->total_bayar,
			'bayar' => $produk->jumlah_uang,
			'kembalian' => $produk->jumlah_uang - ($produk->total_bayar-$produk->diskon),
			'kasir' => $produk->kasir,
			'pelanggan' => $namaPelanggan,
			'cicilan' => $cicilan,
			'diskon' => ($produk->diskon) ? $produk->diskon : 0,
			'total_persen' => ($produk->total_persen) ? $produk->total_persen : 0
		);
		$this->load->view('cetak', $data);
	}

	public function edit_transaksi($idhold){
		$data = array();
		$data['idhold'] = $idhold;
		$transaksiRow = $this->transaksi_model->readbyid($idhold);
		$row = $transaksiRow;
		
		$outlet_transaksi = $this->penggunaoutlet_model->getPenggunaOutlet($row->kasir);
		//simpan ke session
		$this->session->set_userdata('edit_barang_outlet',$outlet_transaksi->outlet_id);

		$kategoriHarga = convertToArray($row->kategori_harga);
		$barcode = convertToArray($row->barcode);
		$harga = convertToArray($row->harga);
		$qty = convertToArray($row->qty);
		$persenDiskon = convertToArray($row->persen_diskon);
		$persenDiskonHarga = convertToArray($row->persen_diskon_harga);

		$i=0;
		$dataEdit = array();
		$totalTagihan = 0;
		$idProduk = array();
		$jenisDiskon = array();
		foreach($barcode as $code){
			$idProduk[$i] = $barcode[$i];
			//check harga awal
			$kategori = kategori_harga($kategoriHarga[$i]);
			$hargaAwal[$i] = $this->produk_model->getNama($barcode[$i],$kategori);
			
			//check is Rupiah
			$isRupiah = false;
			if(isset($persenDiskon[$i])){
				//cek %
				if(strpos($persenDiskon[$i],'%') !== false){
					$isRupiah = false;
				}else{
					$isRupiah = true;
				}
			}else{
				if(strpos($persenDiskon,'%') !== false){
					$isRupiah = false;
				}else{
					$isRupiah = true;
				}
			}
			$jenisDiskon[$i] = $isRupiah;
			$dataEdit[$i] = array( 
				(isset($kategoriHarga[$i])) ? $kategoriHarga[$i] : $kategoriHarga,
				$this->produk_model->getProdukBarcode($barcode[$i]),
				$this->produk_model->getProdukNama($barcode[$i]),
				(isset($harga[$i])) ? "Rp ".number_format($harga[$i],0,'.','.') : "Rp ".number_format($harga,0,'.','.'),
				(is_array($qty)) ? $qty[$i] : $qty,
				(isset($persenDiskon[$i])) ? $persenDiskon[$i] : $persenDiskon,
				(isset($persenDiskonHarga[$i])) ? "Rp ".number_format($persenDiskonHarga[$i],0,'.','.') : "Rp ".number_format($persenDiskonHarga,0,'.','.')
			);
			$totharga = (isset($harga[$i])) ? $harga[$i] : $harga;
			$totqty = (is_array($qty)) ? $qty[$i] : $qty;
			$totalTagihan += $totharga*$totqty;
			$i++;
		}
		$data['row'] = $row;
		$data['transaksi'] = $dataEdit;
		$data['totalTagihan'] = $transaksiRow->total_bayar;
		$data['idProduk'] = $idProduk;
		$data['jenis_diskon'] = $jenisDiskon;
		$data['harga_awal'] = $hargaAwal;

		$this->load->view('revisi_transaksi', $data);
	}

	public function update_transaksi($idhold)
	{
		//reset dulu produk di stok keluar
		$produk = $this->transaksi_model->getAll($idhold);
		$barang = convertToArray($produk->barcode);
		$harga = convertToArray($produk->harga);
		$qty = convertToArray($produk->qty);
		for($i=0; $i < count($barang); $i++){
			$cekBarang = $barang[$i];
			$cekQty = $qty[$i];
			$this->stok_keluar_model->stok_keluar_reset($cekBarang, $cekQty);			
			$this->stok_keluar_model->reset_produk_stok($cekBarang, $cekQty);			
		}
		//=====
		$produk = json_decode($this->input->post('produk'));
		$tanggal = new DateTime($this->input->post('tanggal'));
		$barcode = array();
		$insertStok = array();
		foreach ($produk as $produk) {
			$explode = explode('_',$produk->id);
			$id = $explode[0]; //id_jumlah
			//check stok sekarang
			$rowStok = $this->produk_model->getStok($id);
			$stokSekarang = $rowStok->stok;
			$stokUpdate = $stokSekarang - $produk->terjual;

			$totalTerjual = $rowStok->terjual + $produk->terjual; //akumulasi

			$this->transaksi_model->removeStok($id, $stokUpdate);
			$this->transaksi_model->addTerjual($id, $totalTerjual);
			array_push($barcode, $id);

			//mapping insert stok
			$stokKeluar = array(
				'tanggal' => date('Y-m-d H:i:s'),
				'barcode' => $id,
				'jumlah' => $produk->terjual,
				'keterangan' => 'penjualan',
				'outlet_id' => $this->session->userdata('edit_barang_outlet'),
				'pengguna_id' => $this->session->userdata('id')
			);
			array_push($insertStok, $stokKeluar);
		}
		//insert ke stok
		$this->db->insert_batch('stok_keluar',$insertStok);
		$persenDiskon = implode("|", $this->input->post('persendiskon'));
		$hargaSetelahDiskon = $this->removeHarga($this->input->post('harga_setelah_diskon'));
		$subtotalDiskon = $this->input->post('subtotal_diskon');
		$data = array(
			// 'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => implode(',', $barcode),
			'qty' => implode(',', $this->input->post('qty')),
			'kategori_harga' => implode(',', $this->input->post('kategori_harga')),
			'harga' => $this->removeHarga($this->input->post('harga')),
			'total_bayar' => $this->input->post('total_bayar'),
			'jumlah_uang' => $this->input->post('jumlah_uang'),
			'diskon' => $this->input->post('diskon'),
			'pelanggan' => $this->input->post('pelanggan'),
			// 'nota' => $this->input->post('nota'),
			'keterangan' => $this->input->post('keterangan'),
			'persen_diskon' => $persenDiskon,
			'persen_diskon_harga' => $hargaSetelahDiskon,
			'total_persen' => $subtotalDiskon
		);
		$this->load->model('transaksihutang_model');
		$this->transaksihutang_model->insertHutang($data, $idhold);	


		if ($id = $this->transaksi_model->update($data, $idhold)) {
			echo json_encode($id);
		}
		$data = $this->input->post('form');
	}

	public function removeHarga($hargaPost){
		$value = implode(",",$hargaPost);			
		$tes = htmlentities($value);
		$tes = str_replace(" ","",$tes);
		$tes = str_replace("&nbsp;","",$tes);
		$tes = str_replace("Rp","",$tes);
		$tes = str_replace(".","",$tes);
		return $tes;
	}

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
