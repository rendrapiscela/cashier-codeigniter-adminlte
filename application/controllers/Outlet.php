<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('outlet_model');
	}

	public function index()
	{
		$this->load->view('outlet');
	}

	public function read()
	{
		header('Content-type: application/json');
		if ($this->outlet_model->read()->num_rows() > 0) {
			foreach ($this->outlet_model->read()->result() as $produk) {
				$data[] = array(
					'outlet' => '<a href="#'.$produk->nama_outlet.'" onclick="edit('.$produk->id.')">'.$produk->nama_outlet.'</a>',
					'status' => $produk->status,
					'alamat' => $produk->alamat,
					'keterangan' => $produk->keterangan,
					'action' => '<button class="btn btn-sm btn-success" onclick="edit('.$produk->id.')">Edit</button> <button class="btn btn-sm btn-danger" onclick="remove('.$produk->id.')">Delete</button>'
				);
			}
		} else {
			$data = array();
		}
		$produk = array(
			'data' => $data
		);
		echo json_encode($produk);
	}

	public function add()
	{
		$data = array(
			'nama_outlet' => $this->input->post('outlet'),
			'status' => $this->input->post('status'),
			'alamat' => $this->input->post('alamat'),
			'keterangan' => $this->input->post('keterangan'),
			'created_at' => date('Y-m-d H:i:s')
		);
		if ($this->outlet_model->create($data)) {
			echo json_encode($data);
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($this->outlet_model->delete($id)) {
			echo json_encode('sukses');
		}
	}

	public function edit()
	{
		$id = $this->input->post('id');
		$data = array(
			'nama_outlet' => $this->input->post('outlet'),
			'status' => $this->input->post('status'),
			'alamat' => $this->input->post('alamat'),
			'keterangan' => $this->input->post('keterangan'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		if ($this->outlet_model->update($id,$data)) {
			echo json_encode('sukses');
		}
	}

	public function get_outlet()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$outlet = $this->outlet_model->getById($id);
		if ($outlet->row()) {
			echo json_encode($outlet->row());
		}
	}

	public function get_option()
	{
		header('Content-type: application/json');
		$search = $this->outlet_model->read();
		foreach ($search->result() as $kategori) {
			$data[] = array(
				'id' => $kategori->id,
				'text' => $kategori->nama_outlet
			);
		}
		echo json_encode($data);
	}
	public function get_option_outlet()
	{
		header('Content-type: application/json');
		$search = $this->outlet_model->read();
		$data = array();
		$data[] = ['id' => 'all', 'text' => '--Semua Outlet--'];
		foreach ($search->result() as $kategori) {
			if($kategori->status != 'pusat'){
			$data[] = array(
				'id' => $kategori->id,
				'text' => $kategori->nama_outlet
			);
			}
		}
		echo json_encode($data);
	}
	public function get_option_all()
	{
		header('Content-type: application/json');
		$search = $this->outlet_model->read();
		$data[] = array('id' => 'all', 'text' => 'Semua Outlet');
		foreach ($search->result() as $kategori) {
			$data[] = array(
				'id' => $kategori->id,
				'text' => $kategori->nama_outlet
			);
		}
		echo json_encode($data);
	}


}

/* End of file Outlet.php */
/* Location: ./application/controllers/Outlet.php */
