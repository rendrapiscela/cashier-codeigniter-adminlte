<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('transaksi_model');
		$this->load->model('produk_model');
		$this->load->model('kategori_produk_model');
		$this->load->model('outlet_model');
	}

	public function index()
	{
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->view('laporan_penjualan');
	}

	public function filter_date(){		
		$start = $this->input->post('start');
		$end = $this->input->post('end');		
		$outlet = $this->input->post('outlet');
		$statusBayar = $this->input->post('statusbayar');
		if(isset($outlet) && $this->input->post('outlet') != ''){
			$outlet = $this->input->post('outlet');			
		}
		if ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->num_rows() > 0) {
			$i=1;
			foreach ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->result() as $transaksi) {
				//button edit transaksi
				$editTransaksi = '';
				if($this->session->userdata('role') == 'admin' || $this->session->userdata('id') == $transaksi->kasir){ //self inputer or admin
					$editTransaksi = '<a class="btn btn-sm btn-warning" href="'.site_url('revisi_transaksi/edit_transaksi/').$transaksi->id.'">Revisi</a>';
				}

				$barcode = explode(',', $transaksi->barcode);
				$tanggal = new DateTime($transaksi->tanggal);
				$diskonTotal = ($transaksi->diskon != '') ? $transaksi->diskon : 0;
				$totalPersen = ($transaksi->total_persen != '') ? $transaksi->total_persen : 0;
				$diskonSum = $diskonTotal + $totalPersen;
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'nama_produk' => '<table>'.$this->transaksi_model->getProdukNotQty($barcode, $transaksi->qty).'</table>',
					'qty' => '<table>'.$this->transaksi_model->getQty($transaksi->qty).'</table>',
					'total_bayar' => number_format($transaksi->total_bayar-$transaksi->diskon,0,'.','.'),
					'jumlah_uang' => number_format($transaksi->jumlah_uang,0,'.','.'),
					'diskon' => number_format($diskonSum,0,'.','.'),
					'pelanggan' => (($transaksi->pelanggan == '') ? 'Noname' : $transaksi->pelanggan).'<hr/>'.$transaksi->keterangan,
					'kasir' => ($transaksi->nama_kasir == '') ? 'Noname' : $transaksi->nama_kasir,
					'action' => '<a class="btn btn-sm btn-success" target="_blank" href="'.site_url('transaksi/cetak/').$transaksi->id.'">Print</a>'.$editTransaksi.(($transaksi->status_bayar == 2) ? '<button class="btn btn-sm btn-danger" onclick="formCicilan('.$transaksi->id.')">Bayar Cicilan</button> <button class="btn btn-sm btn-primary" onclick="historyCicilan('.$transaksi->id.')">History</button>' : '')
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$transaksi = array(
			'data' => $data
		);
		echo json_encode($transaksi);
	}

	public function export_excel(){
		$tanggal = $this->input->post('tanggal');
		$statusBayar = $this->input->post('statusbayar');
		$outlet = $this->input->post('outlet');
		if(isset($outlet) && $this->input->post('outlet') != ''){
			$outlet = $this->input->post('outlet');			
		}
		$date = explode(" - ",$tanggal);
		$startDate = new DateTime($date[0]);
		$endDate = new DateTime($date[1]);
		$start = $startDate->format('Y-m-d');
		$end = $endDate->format('Y-m-d');

		if ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->num_rows() > 0) {
			echo json_encode(['data' => 1]);
		}//endif
		else{
			echo json_encode(['data' => 0]);
		}

	}

	public function download(){
		$tanggal = $this->input->get('tanggal');
		$statusBayar = $this->input->get('statusbayar');
		$outlet = $this->input->get('outlet');
		$state = $this->input->get('state');
		if(($this->input->get('outlet') == 'undefined') || ($this->input->get('outlet') == 'null')){
			$outlet = 'all';			
		}else{
			if($this->session->userdata('role') == 'kasir'){
				$outlet = $this->session->userdata('outlet_id');			
			}
			else{
				$outlet = $this->input->get('outlet');			
			}
		}
		$namaOutlet = 'Semua Toko';
		if($outlet != 'all'){
			$rowOutlet = $this->outlet_model->getById($outlet)->row();
			$namaOutlet = ($rowOutlet->nama_outlet) ? $rowOutlet->nama_outlet : 'Semua Toko';
		}
		$date = explode(" - ",$tanggal);
		$startDate = new DateTime($date[0]);
		$endDate = new DateTime($date[1]);
		$start = $startDate->format('Y-m-d');
		$end = $endDate->format('Y-m-d');

		if ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->num_rows() > 0) {
			$data['title'] = "Laporan Penjualan";
			$data['mulai_tanggal'] = $startDate->format('d M Y');
			$data['sampai_tanggal'] = $endDate->format('d M Y');
			$data['nama_outlet'] = $namaOutlet;
			$data['state'] = $state;
			$result = $this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->result();
			$return = array();
			$diskon = array();
			$totalDiskon = 0;
			foreach($result as $row){
				$tanggal = new DateTime($row->tanggal);
				$date = $tanggal->format('d-m-Y H:i:s');
				$barang = convertToArray($row->barcode);
				$qty = convertToArray($row->qty);
				$harga = convertToArray($row->harga);
				$persenDiskonHarga = convertToArray($row->persen_diskon_harga);
				$kategoriHarga = convertToArray($row->kategori_harga);
				$namaPelanggan = ($row->pelanggan) ? $row->pelanggan : 'Noname';
				$namaKasir = ($row->nama_kasir) ? ucwords($row->nama_kasir) : 'Noname';
				//diskon
				if(!empty($row->diskon)){
					$totalDiskon += $row->diskon;
					$diskon[] = array('diskon' => $row->diskon, 'tanggal' => $date, 'pelanggan' => $namaPelanggan, 'kasir' => $namaKasir, 'keterangan' => $row->keterangan);
				}
				$tmp = array();
				foreach ($barang as $i => $val){
					$namaProduk = $this->produk_model->getProdukNama($val);
					$tmp['tanggal'] = $date;
					$tmp['nama_produk'] = $namaProduk;
					$tmp['qty'] = $qty[$i];
					$tmp['harga'] = $harga[$i];
					$tmp['subtotal'] = $persenDiskonHarga[$i];
					$tmp['kategori_harga'] = $kategoriHarga[$i];
					$tmp['pelanggan'] = $namaPelanggan;
					$tmp['kasir'] = $namaKasir;
					array_push($return, $tmp);
				}
			}
			$data['data'] = $return;
			$data['data_diskon'] = $diskon;
			$data['total_diskon'] = $totalDiskon;
			if($state == 'excel'){
				$this->load->view('export_xls_penjualan',$data);
			}	
			if($state == 'print'){
				$this->load->view('export_print_penjualan',$data);
			}	
		}
	}

	public function print_rekap_produk(){
		$tanggal = $this->input->get('tanggal');
		$statusBayar = $this->input->get('statusbayar');
		$outlet = $this->input->get('outlet');
		if(($this->input->get('outlet') == 'undefined') || ($this->input->get('outlet') == 'null')){
			$outlet = 'all';			
		}else{
			if($this->session->userdata('role') == 'kasir'){
				$outlet = $this->session->userdata('outlet_id');			
			}
			else{
				$outlet = $this->input->get('outlet');			
			}
		}
		$namaOutlet = 'Semua Toko';
		if($outlet != 'all'){
			$rowOutlet = $this->outlet_model->getById($outlet)->row();
			$namaOutlet = ($rowOutlet->nama_outlet) ? $rowOutlet->nama_outlet : 'Semua Toko';
		}
		$date = explode(" - ",$tanggal);
		$startDate = new DateTime($date[0]);
		$endDate = new DateTime($date[1]);
		$start = $startDate->format('Y-m-d');
		$end = $endDate->format('Y-m-d');

		if ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->num_rows() > 0) {
			$data['title'] = "Laporan Penjualan";
			$data['mulai_tanggal'] = $startDate->format('d M Y');
			$data['sampai_tanggal'] = $endDate->format('d M Y');
			$data['nama_outlet'] = $namaOutlet;
			$result = $this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->result();
			$return = array();
			$diskon = array();
			$totalDiskon = 0;
			foreach($result as $row){
				$tanggal = new DateTime($row->tanggal);
				$date = $tanggal->format('d-m-Y H:i:s');
				$barang = convertToArray($row->barcode);
				$qty = convertToArray($row->qty);
				$harga = convertToArray($row->harga);
				$persenDiskonHarga = convertToArray($row->persen_diskon_harga);
				$kategoriHarga = convertToArray($row->kategori_harga);
				$namaPelanggan = ($row->pelanggan) ? $row->pelanggan : 'Noname';
				$namaKasir = ($row->nama_kasir) ? ucwords($row->nama_kasir) : 'Noname';
				//diskon
				if(!empty($row->diskon)){
					$totalDiskon += $row->diskon;
					$diskon[] = array('diskon' => $row->diskon, 'tanggal' => $date, 'pelanggan' => $namaPelanggan, 'kasir' => $namaKasir, 'keterangan' => $row->keterangan);
				}
				$tmp = array();
				foreach ($barang as $i => $val){
					$namaProduk = $this->produk_model->getProdukNama($val);
					$tmp['id_produk'] = $val;
					$tmp['nama_produk'] = $namaProduk;
					$tmp['qty'] = $qty[$i];
					$tmp['harga'] = $harga[$i];
					$tmp['subtotal'] = $persenDiskonHarga[$i];
					$tmp['kategori_harga'] = $kategoriHarga[$i];
					$tmp['pelanggan'] = $namaPelanggan;
					$tmp['kasir'] = $namaKasir;
					array_push($return, $tmp);
				}
			}

			$data['data'] = $this->getGroupSum($return);
			$data['data_diskon'] = $diskon;
			$data['total_diskon'] = $totalDiskon;
			$this->load->view('export_print_penjualan_produk',$data);
		}
	}

	private function getGroupSum($data) {
		$groups = array();
		foreach ($data as $item) {
			$key = $item['id_produk'];
			if (!array_key_exists($key, $groups)) {
				$groups[$key] = array(
					'nama_produk' => $item['nama_produk'],
					'qty' => $item['qty'],
					'subtotal' => $item['subtotal'],
				);
			} else {
				$groups[$key]['qty'] = $groups[$key]['qty'] + $item['qty'];
				$groups[$key]['subtotal'] = $groups[$key]['subtotal'] + $item['subtotal'];
			}
		}
		return $groups;
	}

	//print per kategori ==================
	public function print_rekap_kategori(){
		$tanggal = $this->input->get('tanggal');
		$statusBayar = $this->input->get('statusbayar');
		$outlet = $this->input->get('outlet');
		if(($this->input->get('outlet') == 'undefined') || ($this->input->get('outlet') == 'null')){
			$outlet = 'all';			
		}else{
			if($this->session->userdata('role') == 'kasir'){
				$outlet = $this->session->userdata('outlet_id');			
			}
			else{
				$outlet = $this->input->get('outlet');			
			}
		}
		$namaOutlet = 'Semua Toko';
		if($outlet != 'all'){
			$rowOutlet = $this->outlet_model->getById($outlet)->row();
			$namaOutlet = ($rowOutlet->nama_outlet) ? $rowOutlet->nama_outlet : 'Semua Toko';
		}
		$date = explode(" - ",$tanggal);
		$startDate = new DateTime($date[0]);
		$endDate = new DateTime($date[1]);
		$start = $startDate->format('Y-m-d');
		$end = $endDate->format('Y-m-d');

		if ($this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->num_rows() > 0) {
			$data['title'] = "Laporan Penjualan";
			$data['mulai_tanggal'] = $startDate->format('d M Y');
			$data['sampai_tanggal'] = $endDate->format('d M Y');
			$data['nama_outlet'] = $namaOutlet;
			$result = $this->transaksi_model->filterDate($start, $end, $outlet, $statusBayar)->result();
			$return = array();
			$diskon = array();
			$totalDiskon = 0;
			foreach($result as $row){
				$tanggal = new DateTime($row->tanggal);
				$date = $tanggal->format('d-m-Y H:i:s');
				$barang = convertToArray($row->barcode);
				$qty = convertToArray($row->qty);
				$harga = convertToArray($row->harga);
				$persenDiskonHarga = convertToArray($row->persen_diskon_harga);
				$kategoriHarga = convertToArray($row->kategori_harga);
				$namaPelanggan = ($row->pelanggan) ? $row->pelanggan : 'Noname';
				$namaKasir = ($row->nama_kasir) ? ucwords($row->nama_kasir) : 'Noname';
				//diskon
				if(!empty($row->diskon)){
					$totalDiskon += $row->diskon;
					$diskon[] = array('diskon' => $row->diskon, 'tanggal' => $date, 'pelanggan' => $namaPelanggan, 'kasir' => $namaKasir, 'keterangan' => $row->keterangan);
				}
				$tmp = array();
				foreach ($barang as $i => $val){
					$namaProduk = $this->produk_model->getProdukNama($val);
					$namaKategori = $this->produk_model->getProdukNamaKategori($val);
					$tmp['nama_kategori'] = $namaKategori;
					$tmp['id_produk'] = $val;
					$tmp['nama_produk'] = $namaProduk;
					$tmp['qty'] = $qty[$i];
					$tmp['harga'] = $harga[$i];
					$tmp['subtotal'] = $persenDiskonHarga[$i];
					$tmp['kategori_harga'] = $kategoriHarga[$i];
					$tmp['pelanggan'] = $namaPelanggan;
					$tmp['kasir'] = $namaKasir;
					array_push($return, $tmp);
				}
			}
			$kategori = $this->kategori_produk_model->read();
			$result = array();
			foreach($kategori->result() as $rowKategori){
				$keyProduk = array_keys(array_column($return, 'nama_kategori'), $rowKategori->kategori);
				$tmpArray = array();
				foreach($keyProduk as $rowKey){
					$tmpArray[] = $return[$rowKey];
				}
				$result[$rowKategori->kategori] = $this->getGroupSum($tmpArray);
			}
			$data['data'] = $result;
			$data['data_diskon'] = $diskon;
			$data['total_diskon'] = $totalDiskon;
			$this->load->view('export_print_penjualan_kategori',$data);
		}
	} //end function

}

/* End of file Laporan_penjualan.php */
/* Location: ./application/controllers/Laporan_penjualan.php */
