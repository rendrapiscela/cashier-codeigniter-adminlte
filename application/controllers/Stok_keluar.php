<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('stok_keluar_model');
		$this->load->model('outlet_model');
	}

	public function reset_stok($outletId){
		$viewKartu = "SELECT *
		FROM view_kartu_stok_outlet 
		WHERE outlet_id = '".$outletId."' AND transfer_gudang != '1000'";
		$dataStok = $this->db->query($viewKartu);		
		echo "<pre>";
		// print_r($dataStok->result());
		// die();
		if($dataStok->num_rows() > 0){
			$count = 0;
			foreach($dataStok->result() as $row){
				/** 
				 * transfer_gudang lebih dari 1000, 
				 * 1. ambil dari ke table stok_keluar dari barcode.
				 * 2. hapus data 1000 dari stok_keluar
				 * 3. cek apa ada penjualan, jika ada update stok_keluar nilai penjualan + (transfer_gudang-1000)
				 * 
				**/
				$nilaiTransfer = $row->transfer_gudang;
				if($nilaiTransfer > 1000){
					$nilaiStokReal = $nilaiTransfer - 1000;
					$barcode = $row->barcode;
					$deleteStokKeluar = "DELETE FROM stok_keluar WHERE barcode = '".$barcode."' AND jumlah = '1000' AND outlet_id = '".$outletId."' and keterangan ='outlet'";
					$this->db->query($deleteStokKeluar);
					//cek penjualan
					if($row->penjualan > 0){ //ada penjualan
						$rowStokKeluar = "SELECT * FROM stok_keluar WHERE outlet_id = '".$outletId."' AND keterangan = 'outlet' AND barcode = '".$barcode."' order by tanggal desc LIMIT 1";
						$rowSTK = $this->db->query($rowStokKeluar)->row();
						//cek tanggal input
						$tgl = explode(" ",$rowSTK->tanggal);
						if($tgl[0] == '2021-03-26'){
							$updateStok = array();
							$updateStok['updated_at'] = date('Y-m-d H:i:s');
							$updateStok['jumlah']  = ($row->penjualan + $nilaiStokReal); 
							$this->stok_keluar_model->update($rowSTK->id, $updateStok);
							$count++;
						}
					}else{ //tidak ada penjualan

					}
				}
				echo $row->barcode."<br/>";
			}
			echo $count.' berhasil update!';
		}
		die();
	}

	public function reset_stok_minus($outletId){
		$viewKartu = "SELECT *
		FROM view_kartu_stok_outlet 
		WHERE outlet_id = '".$outletId."'";
		$dataStok = $this->db->query($viewKartu);
		echo "<pre>";
		if($dataStok->num_rows() > 0){
			$count = 0;
			foreach($dataStok->result() as $row){
				/** 
				 * Stok Minus 
				 * 1. update stok keluar, penjualan + transfer gudang
				 * 
				**/
				$nilaiTransfer = $row->transfer_gudang;
					$nilaiStokReal = $nilaiTransfer;
					$barcode = $row->barcode;
					//cek penjualan
					if($row->penjualan > 0){ //ada penjualan
						$rowStokKeluar = "SELECT * FROM stok_keluar WHERE outlet_id = '".$outletId."' AND keterangan = 'outlet' AND barcode = '".$barcode."' order by tanggal desc LIMIT 1";
						$rowSTK = $this->db->query($rowStokKeluar)->row();
						$tgl = explode(" ",$rowSTK->tanggal);
						if($tgl[0] == '2021-03-26'){
							$updateStok = array();
							$updateStok['updated_at'] = date('Y-m-d H:i:s');
							$updateStok['jumlah']  = ($row->penjualan + $nilaiStokReal); 
							$this->stok_keluar_model->update($rowSTK->id, $updateStok);
							$count++;
						}
					}	
				echo $row->barcode;
				echo "<br/><br/>";
			}
			echo $count.' berhasil update!';
		}
		die();
	}

	public function index()
	{
		$data['outlet'] = $this->outlet_model->read();
		$this->load->view('stok_keluar', $data);
	}

	public function read()
	{
		header('Content-type: application/json');
		$tahun = date('Y'); $bulan = date('m'); $outlet = ''; $status = ''; $start = ''; $end = '';
		if(!empty($this->input->get('tahun'))){
			$tahun = $this->input->get('tahun');
		}
		if(!empty($this->input->get('bulan'))){
			$bulan = $this->input->get('bulan');
		}
		if(!empty($this->input->get('outlet'))){
			$outlet = $this->input->get('outlet');
		}
		if(!empty($this->input->get('status'))){
			$status = $this->input->get('status');
		}
		
		if(!empty($this->input->post('outlet'))){
			$outlet = $this->input->post('outlet');
		}
		if(!empty($this->input->post('status'))){
			$status = $this->input->post('status');
		}
		
		if(!empty($this->input->post('start'))){
			$start = $this->input->post('start');
		}
		if(!empty($this->input->post('end'))){
			$end = $this->input->post('end');
		}


		if ($this->stok_keluar_model->read_filter($tahun, $bulan, $outlet, $status, $start, $end)->num_rows() > 0) {
			$i=1;
			foreach ($this->stok_keluar_model->read_filter($tahun, $bulan, $outlet, $status, $start, $end)->result() as $stok_keluar) {
				$tanggal = new DateTime($stok_keluar->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'barcode' => $stok_keluar->barcode,
					'nama_produk' => $stok_keluar->nama_produk,
					'jumlah' => $stok_keluar->jumlah,
					'keterangan' => $stok_keluar->keterangan,
					'outlet' => $stok_keluar->nama_outlet,
					'nama_pengguna' => $stok_keluar->nama_pengguna,
					'actions' => '<button class="btn btn-sm btn-success" onclick="edit('.$stok_keluar->id.')">Edit</button> <button class="btn btn-sm btn-danger" onclick="remove('.$stok_keluar->id.')">Delete</button>'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$stok_keluar = array(
			'data' => $data
		);
		echo json_encode($stok_keluar);
	}
	public function edit()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$stokMasuk = $this->stok_keluar_model->read_byid($id);
		echo json_encode($stokMasuk);
	}

	public function hapus()
	{
		$id = $this->input->post('id');
		if ($this->stok_keluar_model->delete($id)) {
			echo json_encode('sukses');
		}
	}

	public function filter()
	{
		header('Content-type: application/json');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$outlet = $this->input->post('outlet');
		$status = $this->input->post('status');
		if ($this->stok_keluar_model->laporan($start, $end, $outlet, $status)->num_rows() > 0) {
			$i=1;
			foreach ($this->stok_keluar_model->laporan($start, $end, $outlet, $status)->result() as $stok_keluar) {
				$tanggal = new DateTime($stok_keluar->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'barcode' => $stok_keluar->barcode,
					'nama_produk' => $stok_keluar->nama_produk,
					'jumlah' => $stok_keluar->jumlah,
					'keterangan' => $stok_keluar->keterangan,
					'nama_pengguna' => $stok_keluar->nama_pengguna,
					'outlet' => $stok_keluar->nama_outlet
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$stok_keluar = array(
			'data' => $data
		);
		echo json_encode($stok_keluar);
	}

	public function add()
	{
		//edit
		if($this->input->post('submit') == 'edit'){
			$id = $this->input->post('id');
			$data = array(
				'jumlah' => $this->input->post('jumlah'),
				'keterangan' => $this->input->post('keterangan'),
				'outlet_id' => ($this->input->post('outlet')) ? $this->input->post('outlet') : 1,
				'pengguna_id' => $this->session->userdata('id')
			);
			if ($this->stok_keluar_model->update($id,$data)) {
				echo json_encode('sukses');
			}
		}
		//add
		if($this->input->post('submit') == 'add'){
		$id = $this->input->post('barcode');
		$jumlah = $this->input->post('jumlah');
		$stok = $this->stok_keluar_model->getStok($id)->stok;
		$rumus = max($stok - $jumlah,0);
		if($this->input->post('keterangan') != 'outlet'){
			$addStok = $this->stok_keluar_model->addStok($id, $rumus);
		}
		$tanggal = new DateTime($this->input->post('tanggal'));
		$data = array(
			'tanggal' => $tanggal->format('Y-m-d H:i:s'),
			'barcode' => $id,
			'jumlah' => $jumlah,
			'keterangan' => $this->input->post('keterangan'),
			'outlet_id' => ($this->input->post('outlet')) ? $this->input->post('outlet') : 1,
			'pengguna_id' => $this->session->userdata('id')
		);
		if ($this->stok_keluar_model->create($data)) {
			echo json_encode('sukses');
		}
		}
	}

	public function get_barcode()
	{
		$barcode = $this->input->post('barcode');
		$kategori = $this->stok_keluar_model->getKategori($id);
		if ($kategori->row()) {
			echo json_encode($kategori->row());
		}
	}

}

/* End of file Stok_keluar.php */
/* Location: ./application/controllers/Stok_keluar.php */
