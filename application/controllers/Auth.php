<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
	}

	public function login()
	{
		if ($this->session->userdata('status') !== 'login' ) {
			if ($this->input->post('username')) {
				$username = $this->input->post('username');
				if ($this->auth_model->getUser($username)->num_rows() > 0) {
					$data = $this->auth_model->getUser($username)->row();
					$toko = $this->auth_model->getToko();
					//get outlet id
					$outlet = $this->auth_model->getOutletUser($data->id);
					if (password_verify($this->input->post('password'), $data->password)) {
						//config role
						if($data->role == '1'){
							$role = 'admin';
						}elseif($data->role == '2'){
							$role = 'kasir';
						}elseif($data->role == '3'){
							$role = 'gudang';
						}
						$userdata = array(
							'id' => $data->id,
							'username' => $data->username,
							'password' => $data->password,
							'nama' => $data->nama,
							'role' => $role,
							'status' => 'login',
							'toko' => $toko,
							'outlet_id' => ($outlet) ? $outlet->outlet_id : 0,
							'nama_outlet' => ($outlet) ? $outlet->nama_outlet : 'Admin Owner',
							'version' => rand(1,50)
						);
						$this->session->set_userdata($userdata);
						echo json_encode('sukses');
					} else {
						echo json_encode('passwordsalah');
					}
				} else {
					echo json_encode('tidakada');
				}
			} else {
				$this->load->view('login');
			}
		} else {
			redirect('/');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
