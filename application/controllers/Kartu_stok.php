<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu_stok extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('transaksi_model');
		$this->load->model('produk_model');
		$this->load->model('outlet_model');
		$this->load->model('kartu_stok_model');
	}

	public function gudang()
	{
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		//get nama gudang / pusat
		$outlet = $this->db->query("SELECT nama_outlet FROM outlet WHERE status ='pusat'")->row();
		$namaOutlet = ($outlet) ? $outlet->nama_outlet : '-';
		$data['namaOutlet'] = $namaOutlet;
		$this->load->view('kartu_stok_gudang', $data);
	}

	public function read_gudang()
	{
		header('Content-type: application/json');
		$kategori = '';
		if(!empty($this->input->get('kategori'))){
			$kategori = $this->input->get('kategori');
		}
		if ($this->kartu_stok_model->read_gudang($kategori)->num_rows() > 0) {
			$i=1;
			foreach ($this->kartu_stok_model->read_gudang($kategori)->result() as $kartu) {
				$data[] = array(
					'no' => $i,
					'kategori' => $kartu->kategori,
					'barcode' => $kartu->barcode,
					'nama_produk' => $kartu->nama_produk,
					'total_stok_masuk' => $kartu->total_stok_masuk,
					'total_stok_keluar' => $kartu->total_stok_keluar,
					'stok_gudang' => $kartu->stok_gudang
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$kartuStok = array(
			'data' => $data
		);
		echo json_encode($kartuStok);
	}

	public function outlet()
	{
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		//get nama gudang / pusat
		$outlet = $this->db->query("SELECT nama_outlet FROM outlet WHERE status ='pusat'")->row();
		$namaOutlet = ($outlet) ? $outlet->nama_outlet : '-';
		$data['namaOutlet'] = $namaOutlet;
		$this->load->view('kartu_stok_outlet', $data);
	}

	public function read_outlet()
	{
		header('Content-type: application/json');
		$kategori = ''; $outlet = ''; $status = '';
		if(!empty($this->input->get('status'))){
			$status = $this->input->get('status');
		}
		if(!empty($this->input->get('kategori'))){
			$kategori = $this->input->get('kategori');
		}
		if(!empty($this->input->get('outlet'))){
			$outlet = $this->input->get('outlet');			
			if($this->input->get('outlet') == 'all'){
				$outlet = '';
			}
		}
		if ($this->kartu_stok_model->read_outlet_filter($kategori,$outlet, $status)->num_rows() > 0) {
			$i=1;
			foreach ($this->kartu_stok_model->read_outlet_filter($kategori,$outlet, $status)->result() as $kartu) {
				$data[] = array(
					'no' => $i,
					'nama_outlet' => $kartu->nama_outlet,
					'kategori' => $kartu->kategori,
					'barcode' => $kartu->kode_barang,
					'nama_produk' => $kartu->nama_produk,
					'transfer_gudang' => $kartu->transfer_gudang,
					'penjualan' => $kartu->penjualan,
					'stok_sekarang' => $kartu->stok
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$kartuStok = array(
			'data' => $data
		);
		echo json_encode($kartuStok);
	}


	public function filter_date(){		
		$start = $this->input->post('start');
		$end = $this->input->post('end');		
		$outlet = $this->input->post('outlet');
		if(isset($outlet) && $this->input->post('outlet') != ''){
			$outlet = $this->input->post('outlet');			
		}
		if ($this->transaksi_model->filterDate($start, $end, $outlet)->num_rows() > 0) {
			$i=1;
			foreach ($this->transaksi_model->filterDate($start, $end, $outlet)->result() as $transaksi) {
				$barcode = explode(',', $transaksi->barcode);
				$tanggal = new DateTime($transaksi->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'nama_produk' => '<table>'.$this->transaksi_model->getProdukNotQty($barcode, $transaksi->qty).'</table>',
					'qty' => '<table>'.$this->transaksi_model->getQty($transaksi->qty).'</table>',
					'total_bayar' => $transaksi->total_bayar,
					'jumlah_uang' => $transaksi->jumlah_uang,
					'diskon' => $transaksi->diskon,
					'pelanggan' => ($transaksi->pelanggan == '') ? 'Noname' : $transaksi->pelanggan,
					'kasir' => ($transaksi->nama_kasir == '') ? 'Noname' : $transaksi->nama_kasir,
					'action' => '<a class="btn btn-sm btn-success" target="_blank" href="'.site_url('transaksi/cetak/').$transaksi->id.'">Print</a> <button style="display:none;" class="btn btn-sm btn-danger" onclick="remove('.$transaksi->id.')">Delete</button>'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$transaksi = array(
			'data' => $data
		);
		echo json_encode($transaksi);
	}


}

/* End of file Laporan_penjualan.php */
/* Location: ./application/controllers/Laporan_penjualan.php */
