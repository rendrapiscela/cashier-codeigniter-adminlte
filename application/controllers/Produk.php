<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('produk_model');
		$this->load->model('kategori_produk_model');
		$this->load->model('stok_masuk_model');
		$this->load->model('stok_keluar_model');
		$this->load->model('satuan_produk_model');
	}

	public function set_stok_masuk($nilaiStok, $status){
		$produks = $this->db->query("SELECT * FROM produk")->result();
		foreach($produks as $produk){
		//update stok di master produk
		$mProduk = array();
		$mProduk['stok'] = $nilaiStok;
		$this->produk_model->update($produk->id, $mProduk);

		//insert stok_masuk
			$stok = array();
			$stok['tanggal'] = date('Y-m-d H:i:s');
			$stok['barcode'] = $produk->id;
			$stok['jumlah'] = $nilaiStok;
			$stok['keterangan'] = $status;
			$stok['supplier'] = 0;
			$stok['outlet_id'] = 1;
			$stok['pengguna_id'] = 1;

			$this->stok_masuk_model->create($stok);
		} //endforeach
	}

	public function set_stok_keluar($nilaiStok, $status, $outletId){
		$produks = $this->db->query("SELECT * FROM produk")->result();
		foreach($produks as $produk){
		//update stok di master produk
		$mProduk = array();
		$mProduk['stok'] = $nilaiStok;
		$this->produk_model->update($produk->id, $mProduk);

		//insert stok_masuk
			$stok = array();
			$stok['tanggal'] = date('Y-m-d H:i:s');
			$stok['barcode'] = $produk->id;
			$stok['jumlah'] = $nilaiStok;
			$stok['keterangan'] = $status;
			$stok['outlet_id'] = $outletId;
			$stok['pengguna_id'] = 1;

			$this->stok_keluar_model->create($stok);
		} //endforeach
	}


	public function resethargadiskon(){
		$produk = $this->produk_model->read();
		$i = 1; $j = 1;
		foreach($produk->result() as $row){
			// $update=array();
			// $update['harga_before'] = $row->harga;
			// $update['harga_beli_before'] = $row->harga_beli;
			// $update['harga_reseller_before'] = $row->harga_reseller;
			// $update['harga_khusus_before'] = $row->harga_khusus;

			// if($this->produk_model->update($row->id, $update)){
			// 	$i++;
			// }

			//rollback
			// $update=array();
			// $update['harga'] = $row->harga_before;
			// $update['harga_beli'] = $row->harga_beli_before;
			// $update['harga_reseller'] = $row->harga_reseller_before;
			// $update['harga_khusus'] = $row->harga_khusus_before;

			// if($this->produk_model->update($row->id, $update)){
			// 	$i++;
			// }

			//update harga diskon 5%
			// $diskon = array();
			// $harga = 0; $harga_reseller = 0; $harga_khusus = 0;
			// $diskonHarga = $this->hitungdiskon($row->harga);
			// $diskonHargaReseller = $this->hitungdiskon($row->harga_reseller);
			// $diskonHargaKhusus = $this->hitungdiskon($row->harga_khusus);
				
			// $diskon['harga'] = CEIL($diskonHarga);
			// $diskon['harga_reseller'] = CEIL($diskonHargaReseller);
			// $diskon['harga_khusus'] = CEIL($diskonHargaKhusus);
			
			// if($this->produk_model->update($row->id, $diskon)){
			// 	$j++;
			// }
			
		} //endforeach
		echo "Updated Data : ".$i;
		echo "<br/>";
		echo "Updated Data Diskon : ".$j;
	}
	public function hitungdiskon($harga, $diskon = 5){ //diskon persen
		if(!empty($harga) && $harga > 0){
			$diskon = ($harga * ( $diskon / 100));
			$hargaAkhir = $harga + $diskon;
			return $hargaAkhir;
		}else{
			return 0;
		}
	}

	public function resetulang_code(){
		$produk = $this->produk_model->resetUlangBarcode();
	}
	public function cek_kode_terakhir()
	{
		$idKategori = $this->input->post('id');
		$maksCode = $this->produk_model->maxCode($idKategori)->makscode;
		echo json_encode(['kode' => '0'.$maksCode]);
	}

	public function index()
	{
		$data['maksCode'] = $this->produk_model->maxCode()->makscode;
		$data['kategori'] = $this->kategori_produk_model->read();
		$data['satuan'] = $this->satuan_produk_model->read();
		$this->load->view('produk', $data);
	}
	public function cek_duplikat(){
		header('Content-type: application/json');
		$barcode = $_POST['id'];
		$row = $this->produk_model->barcodeRow($barcode);
		$status = false;
		if(!empty($row)){
			$status=true;
		}
		echo json_encode($status);
	}
	public function read()
	{
		header('Content-type: application/json');
		if ($this->produk_model->read()->num_rows() > 0) {
			foreach ($this->produk_model->read()->result() as $produk) {
				$buttonDelete = '<button class="btn btn-sm btn-danger" onclick="remove('.$produk->id.')">Delete</button>';
				if($produk->stok > 0){
					$buttonDelete = ''; //hide tidak bisa dihapus
				}
				$data[] = array(
					'barcode' => '<a href="#'.$produk->barcode.'" onclick="edit('.$produk->id.')">'.$produk->barcode.'</a>',
					'nama' => $produk->nama_produk,
					'kategori' => $produk->kategori,
					'satuan' => $produk->satuan,
					'harga' => number_format($produk->harga,0,'.','.'),
					'harga_beli' => number_format($produk->harga_beli,0,'.','.'),
					'harga_khusus' => number_format($produk->harga_khusus,0,'.','.'),
					'harga_reseller' => number_format($produk->harga_reseller,0,'.','.'),
					'stok' => number_format($produk->stok,0,'.','.'),
					'action' => '<button class="btn btn-sm btn-success" onclick="edit('.$produk->id.')">Edit</button> '.$buttonDelete
				);
			}
		} else {
			$data = array();
		}
		$produk = array(
			'data' => $data
		);
		echo json_encode($produk);
	}

	public function add()
	{
		$data = array(
			'barcode' => $this->input->post('barcode'),
			'nama_produk' => $this->input->post('nama_produk'),
			'satuan' => $this->input->post('satuan'),
			'kategori' => $this->input->post('kategori'),
			'harga' => $this->input->post('harga'),
			'harga_beli' => $this->input->post('harga_beli'),
			'harga_khusus' => $this->input->post('harga_khusus'),
			'harga_reseller' => $this->input->post('harga_reseller'),
			'stok' => $this->input->post('stok'),
			'created_by' => $this->session->userdata('nama'),
			'created_at' => date('Y-m-d H:i:s')
		);
		if ($this->produk_model->create($data)) {
			echo json_encode($data);
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		if ($this->produk_model->delete($id)) {
			echo json_encode('sukses');
		}
	}

	public function edit()
	{
		$id = $this->input->post('id');
		$data = array(
			'barcode' => $this->input->post('barcode'),
			'nama_produk' => $this->input->post('nama_produk'),
			'satuan' => $this->input->post('satuan'),
			'kategori' => $this->input->post('kategori'),
			'harga' => $this->input->post('harga'),
			'harga_beli' => $this->input->post('harga_beli'),
			'harga_khusus' => $this->input->post('harga_khusus'),
			'harga_reseller' => $this->input->post('harga_reseller'),
			'stok' => $this->input->post('stok'),
			'updated_by' => $this->session->userdata('nama'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		if ($this->produk_model->update($id,$data)) {
			echo json_encode('sukses');
		}
	}

	public function get_produk()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$kategori = $this->produk_model->getProduk($id);
		if ($kategori->row()) {
			echo json_encode($kategori->row());
		}
	}

	public function get_barcode()
	{
		header('Content-type: application/json');
		$barcode = $this->input->post('barcode');
		$search = $this->produk_model->getBarcode($barcode);
		foreach ($search as $barcode) {
			$data[] = array(
				'id' => $barcode->id,
				'text' => $barcode->barcode.' - '.$barcode->nama_produk
			);
		}
		echo json_encode($data);
	}
	
	public function get_barcode_price()
	{
		header('Content-type: application/json');
		$barcode = $this->input->post('barcode');
		$kategori = $this->input->post('kategori');
		$search = $this->produk_model->getBarcodeLimit($barcode,30);
		foreach ($search as $barcode) {
			$kategori = ($kategori == 'harga') ? $kategori.'_umum' : $kategori;			
			$data[] = array(
				'id' => $barcode->id,
				'text' => $barcode->barcode.' - '.$barcode->nama_produk." | Rp. ".number_format($barcode->$kategori,0,'.','.')
			);
		}
		echo json_encode($data);
	}

	public function get_nama()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori_harga');
		echo json_encode($this->produk_model->getNama($id, $kategori));
	}

	public function get_stok()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori_harga');
		echo json_encode($this->produk_model->getStok($id, $kategori));
	}

	public function produk_terlaris()
	{
		header('Content-type: application/json');
		$produk = $this->produk_model->produkTerlaris();
		foreach ($produk as $key) {
			$label[] = $key->nama_produk;
			$data[] = $key->terjual;
		}
		$result = array(
			'label' => $label,
			'data' => $data,
		);
		echo json_encode($result);
	}

	public function data_stok()
	{
		header('Content-type: application/json');
		$produk = $this->produk_model->dataStok();
		echo json_encode($produk);
	}

}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */
