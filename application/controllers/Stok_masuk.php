<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		
		$this->load->model('stok_masuk_model');
	}

	public function index()
	{
		$this->load->view('stok_masuk');
	}

	public function read()
	{
		header('Content-type: application/json');
		$tahun = date('Y'); $bulan = date('m');
		if(!empty($this->input->get('tahun'))){
			$tahun = $this->input->get('tahun');
		}
		if(!empty($this->input->get('bulan'))){
			$bulan = $this->input->get('bulan');
		}

		if ($this->stok_masuk_model->read_filter($tahun, $bulan)->num_rows() > 0) {
			foreach ($this->stok_masuk_model->read_filter($tahun, $bulan)->result() as $stok_masuk) {
				$tanggal = new DateTime($stok_masuk->tanggal);
				$data[] = array(
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'barcode' => $stok_masuk->barcode,
					'nama_produk' => $stok_masuk->nama_produk,
					'jumlah' => $stok_masuk->jumlah,
					'keterangan' => ($stok_masuk->keterangan == 'penambahan') ? 'Belanja Barang' : $stok_masuk->keterangan,
					'supplier' => ($stok_masuk->nama_supplier == '') ? '-' : $stok_masuk->nama_supplier,
					'oleh' => ($stok_masuk->nama_pengguna == '') ? '' : $stok_masuk->nama_pengguna,
					'actions' => '<button class="btn btn-sm btn-success" onclick="edit('.$stok_masuk->id.')">Edit</button> <button class="btn btn-sm btn-danger" onclick="remove('.$stok_masuk->id.')">Delete</button>'
				);
			}
		} else {
			$data = array();
		}
		$stok_masuk = array(
			'data' => $data
		);
		echo json_encode($stok_masuk);
	}

	public function add()
	{
		if($this->input->post('add') == 'add'){
		$id = $this->input->post('barcode');
		$jumlah = $this->input->post('jumlah');
		$stok = $this->stok_masuk_model->getStok($id)->stok;
		$rumus = max($stok + $jumlah,0);
		$addStok = $this->stok_masuk_model->addStok($id, $rumus);
		
		if ($addStok) {
			$tanggal = new DateTime($this->input->post('tanggal'));
			$data = array(
				'tanggal' => $tanggal->format('Y-m-d H:i:s'),
				'barcode' => $id,
				'jumlah' => $jumlah,
				'keterangan' => $this->input->post('keterangan'),
				'supplier' => ($this->input->post('supplier')) ? $this->input->post('supplier') : 0,
				'outlet_id' => '1',
				'pengguna_id' => $this->session->userdata('id')
			);
			if ($this->stok_masuk_model->create($data)) {
				echo json_encode('sukses');
			}
		}
		} //end add
		//edit
		if($this->input->post('add') == 'edit'){
			$id = $this->input->post('id');
			$data = array(
				'jumlah' => $this->input->post('jumlah'),
				'keterangan' => $this->input->post('keterangan'),
				'supplier' => ($this->input->post('supplier')) ? $this->input->post('supplier') : '',
				'outlet_id' => '1',
				'pengguna_id' => $this->session->userdata('id')
			);
			if ($this->stok_masuk_model->update($id,$data)) {
				echo json_encode('sukses');
			}			
		}
	}
	public function edit()
	{
		header('Content-type: application/json');
		$id = $this->input->post('id');
		$stokMasuk = $this->stok_masuk_model->read_byid($id);
		echo json_encode($stokMasuk);
	}

	public function hapus()
	{
		$id = $this->input->post('id');
		if ($this->stok_masuk_model->delete($id)) {
			echo json_encode('sukses');
		}
	}


	public function get_barcode()
	{
		$barcode = $this->input->post('barcode');
		$kategori = $this->stok_masuk_model->getKategori($id);
		if ($kategori->row()) {
			echo json_encode($kategori->row());
		}
	}

	public function laporan()
	{
		header('Content-type: application/json');
		if ($this->stok_masuk_model->laporan()->num_rows() > 0) {
			$i=1;
			foreach ($this->stok_masuk_model->laporan()->result() as $stok_masuk) {
				$tanggal = new DateTime($stok_masuk->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'barcode' => $stok_masuk->barcode,
					'nama_produk' => $stok_masuk->nama_produk,
					'jumlah' => $stok_masuk->jumlah,
					'keterangan' => $stok_masuk->keterangan,
					'supplier' => ($stok_masuk->supplier) ? $stok_masuk->supplier : '-'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$stok_masuk = array(
			'data' => $data
		);
		echo json_encode($stok_masuk);
	}

	public function laporan_filter()
	{
		header('Content-type: application/json');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		if ($this->stok_masuk_model->laporan($start, $end)->num_rows() > 0) {
			$i=1;
			foreach ($this->stok_masuk_model->laporan($start, $end)->result() as $stok_masuk) {
				$tanggal = new DateTime($stok_masuk->tanggal);
				$data[] = array(
					'no' => $i,
					'tanggal' => $tanggal->format('d-m-Y H:i:s'),
					'barcode' => $stok_masuk->barcode,
					'nama_produk' => $stok_masuk->nama_produk,
					'jumlah' => $stok_masuk->jumlah,
					'keterangan' => $stok_masuk->keterangan,
					'supplier' => ($stok_masuk->supplier) ? $stok_masuk->supplier : '-'
				);
				$i++;
			}
		} else {
			$data = array();
		}
		$stok_masuk = array(
			'data' => $data
		);
		echo json_encode($stok_masuk);
	}

	public function stok_hari()
	{
		header('Content-type: application/json');
		$now = date('d m Y');
		$total = $this->stok_masuk_model->stokHari($now);
		echo json_encode($total->total === null ? 0 : $total);
	}

}

/* End of file Stok_masuk.php */
/* Location: ./application/controllers/Stok_masuk.php */
