<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') !== 'login' ) {
			redirect('/');
		}
		$this->load->model('pengguna_model');
		$this->load->model('penggunaoutlet_model');
	}

	public function index()
	{
		$this->load->view('pengguna');
	}

	public function read()
	{
		header('Content-type: application/json');
		if ($this->pengguna_model->read()->num_rows() > 0) {
			foreach ($this->pengguna_model->read()->result() as $pengguna) {
				$rowPenggunaOutlet = $this->penggunaoutlet_model->get_po_byidpengguna($pengguna->id);
				$data[] = array(
					'outlet' => $rowPenggunaOutlet->nama_outlet,
					'username' => $pengguna->username,
					'nama' => $pengguna->nama,
					'action' => '<button class="btn btn-sm btn-success" onclick="edit('.$pengguna->id.')">Edit</button> <button class="btn btn-sm btn-danger" onclick="remove('.$pengguna->id.')">Delete</button>'
				);
			}
		} else {
			$data = array();
		}
		$pengguna = array(
			'data' => $data
		);
		echo json_encode($pengguna);
	}

	public function add()
	{
		$outlet_id = $this->input->post('outlet');
		$data = array(
			'username' => $this->input->post('username'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'nama' => $this->input->post('nama'),
			'role' => ($outlet_id == 1) ? 3 : 2
		);
		if ($this->pengguna_model->create($data)) {
			$last_id = $this->db->insert_id();
			$this->penggunaoutlet_model->create($last_id, $outlet_id);
			echo json_encode('sukses');
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		//hapus penggunaoutlet
		$this->penggunaoutlet_model->delete_bypengguna($id);
		if ($this->pengguna_model->delete($id)) {
			echo json_encode('sukses');
		}
	}

	public function edit()
	{
		$id = $this->input->post('id');
		$outlet_id = $this->input->post('outlet');
		$data = array(
			'username' => $this->input->post('username'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'nama' => $this->input->post('nama'),
			'role' => ($outlet_id == 1) ? 3 : 2
		);
		if ($this->pengguna_model->update($id,$data)) {
			$this->penggunaoutlet_model->create($id, $outlet_id);
			echo json_encode('sukses');
		}
	}

	public function get_pengguna()
	{
		$this->load->model('penggunaoutlet_model');
		$id = $this->input->post('id');
		$pengguna = $this->penggunaoutlet_model->get_po_byidpengguna($id);
		if ($pengguna) {
			echo json_encode($pengguna);
		}
	}

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
