<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet_model extends CI_Model {

	private $table = 'outlet';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function read()
	{
		$this->db->from($this->table);
		return $this->db->get();
	}

	public function getById($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		return $this->db->get();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}


}

/* End of file Outlet_model.php */
/* Location: ./application/models/Outlet_model.php */
