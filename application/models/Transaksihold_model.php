<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksihold_model extends CI_Model {

	private $table = 'transaksi_hold';
	
	public function create($data)
	{
		$this->db->insert($this->table, $data);
		$lastId = $this->db->insert_id();
		return $lastId;	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function remove($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function read()
	{
		$this->db->select('transaksi_hold.id, transaksi_hold.tanggal, transaksi_hold.barcode, transaksi_hold.qty, pengguna.nama as nama_kasir, transaksi_hold.harga, transaksi_hold.kategori_harga, transaksi_hold.keterangan, transaksi_hold.nota');
		$this->db->from($this->table);
		$this->db->join('pengguna', 'transaksi_hold.kasir = pengguna.id');
		$this->db->where('transaksi_hold.kasir', $this->session->userdata('id'));
		return $this->db->get();
	}

	public function read_byid($id){
		$this->db->from($this->table);
		$this->db->where('id',$id);
		return $this->db->get();
	}
    
}
