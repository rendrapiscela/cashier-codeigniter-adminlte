<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggunaoutlet_model extends CI_Model {

	private $table = 'pengguna_outlet';
	private $view = 'view_pengguna';

	public function create($pengguna_id, $outlet_id)
	{
		$cek = $this->getPenggunaOutlet($pengguna_id);
		$data = [
			'pengguna_id' => $pengguna_id,
			'outlet_id' => $outlet_id
		];
		if(empty($cek)){
			$data['created_at'] = date('Y-m-d H:i:s');
			return $this->db->insert($this->table, $data);
		}else{
			//update
			$data['updated_at'] = date('Y-m-d H:i:s');
			$this->db->where('pengguna_id', $pengguna_id);
			$this->db->update($this->table, $data);
		}

	}

	public function get_pengguna_outlet()
	{
		$this->db->from($this->view);
		return $this->db->get();
	}
	public function get_po_byidpengguna($pengguna_id)
	{
		$this->db->from($this->view);
		$this->db->where('id',$pengguna_id);
		return $this->db->get()->row();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}
	public function delete_bypengguna($id)
	{
		$this->db->where('pengguna_id', $id);
		return $this->db->delete($this->table);
	}

	public function getPenggunaOutlet($pengguna_id){
		$this->db->from($this->table);
		$row = $this->db->where('pengguna_id',$pengguna_id);
		return $row->get()->row();
	}

}

/* End of file Penggunaoutlet_model.php */
/* Location: ./application/models/Penggunaoutlet_model.php */
