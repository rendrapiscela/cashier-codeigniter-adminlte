<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

	private $table = 'produk';

	public function resetUlangBarcode(){
		//kategori produk
		$kategori = $this->db->from('kategori_produk')->get()->result();
		$count=1;
		foreach($kategori as $kate){
			$kategoriId = $kate->id;
			
			$produk = $this->db->from('produk');
			$produk = $this->db->where('kategori',$kategoriId);
			$produk = $this->db->order_by('nama_produk','ASC');
			$produk = $this->db->get()->result();

			$kodeKategori = '0'.$kategoriId;
			$awal = 1;
			foreach($produk as $pro){
				//updating
				$cekPanjang = strlen($awal);
				$sisa = 6-$cekPanjang;
				$nol = '';
				for ($i=1; $i <= $sisa; $i++) { 
					$nol.='0';	
				}
				$updateKode = $kodeKategori.$nol.$awal;

				$updateProduk = array();
				$updateProduk['barcode'] = $updateKode;

				$this->update($pro->id, $updateProduk);

				$i++;
				$awal++;
				$count++;
			}
		}
		echo $count." Data Terupdate"; die();
	}
	public function barcodeRow($kode){
		$this->db->select("produk.*");
		$this->db->from($this->table);
		$this->db->where("barcode = '".$kode."'");
		return $this->db->get()->row();
	}
	public function maxCode($idKategori = null){
		$this->db->select("max(barcode * 1) as makscode");
		$this->db->from($this->table);
		if(!empty($idKategori)){
			$this->db->where("kategori",$idKategori);
		}
		return $this->db->get()->row();
	}
	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function read()
	{
		$this->db->select('produk.id, produk.barcode, produk.nama_produk, produk.harga, kategori_produk.kategori, satuan_produk.satuan, produk.harga_beli, produk.harga_khusus, produk.harga_reseller, (SELECT COALESCE(stok_gudang,0) FROM view_kartu_stok WHERE view_kartu_stok.id = produk.id LIMIT 1) AS stok, produk.harga_before, produk.harga_reseller_before, produk.harga_beli_before, produk.harga_khusus_before');
		$this->db->from($this->table);
		$this->db->join('kategori_produk', 'produk.kategori = kategori_produk.id');
		$this->db->join('satuan_produk', 'produk.satuan = satuan_produk.id');
		return $this->db->get();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function getProduk($id)
	{
		$this->db->select('produk.id, produk.barcode, produk.nama_produk, produk.harga, kategori_produk.id as kategori_id, kategori_produk.kategori, satuan_produk.id as satuan_id, satuan_produk.satuan, produk.harga_beli, produk.harga_khusus, produk.harga_reseller, (SELECT COALESCE(stok_gudang,0) FROM view_kartu_stok WHERE view_kartu_stok.id = produk.id LIMIT 1) AS stok');
		$this->db->from($this->table);
		$this->db->join('kategori_produk', 'produk.kategori = kategori_produk.id');
		$this->db->join('satuan_produk', 'produk.satuan = satuan_produk.id');
		$this->db->where('produk.id', $id);
		return $this->db->get();
	}

	public function getBarcode($search='')
	{
		$where = "(barcode like '%".$search."%' OR nama_produk like '%".$search."%')";
		$this->db->select('produk.id, produk.barcode, produk.nama_produk, harga as harga_umum, harga_reseller, harga_khusus');
		$this->db->where($where);
		return $this->db->get($this->table)->result();
	}
	public function getBarcodeLimit($search='', $limit = null)
	{
		$where = "(barcode like '%".$search."%' OR nama_produk like '%".$search."%')";
		$this->db->select('produk.id, produk.barcode, produk.nama_produk, harga as harga_umum, harga_reseller, harga_khusus');
		$this->db->where($where);
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		return $this->db->get($this->table)->result();
	}

	public function getBarcodeRow($kode, $kategori = null)
	{
		if(empty($kode)){
			return ['status' => 'pilih_barang'];
		}	
		$this->db->select('produk.*, nama_produk, stok, harga as harga_umum, harga_reseller, harga_khusus');
		$this->db->like('barcode', $kode);
		$row = $this->db->get($this->table)->row();
		if(!empty($kategori)){
			$stok = $this->getViewStok($row->id);
			$kategori = ($kategori == 'harga') ? $kategori.'_umum' : $kategori;			
			$row->harga = number_format($row->$kategori,0,'.','.');
			$row->stok = $stok;
			return $row;
		}	
	}

	public function cekProdukParam($slug, $findValue){
		$this->db->from("params");
		$this->db->where('slug', $slug);
		$row = $this->db->get()->row();
		if(!empty($row)){
			$setValue = json_decode($row->value, 1);
			if(in_array($findValue, $setValue)){
				return $row->slug;
			}
		}
		return 'tidak';
	}

	public function getNama($id, $kategori = null)
	{	
		if(empty($id)){
			return ['status' => 'pilih_barang'];
		}	
		$this->db->select('id, nama_produk, stok, harga as harga_umum, harga_reseller, harga_khusus');
		$this->db->where('id', $id);
		$row = $this->db->get($this->table)->row();
		if(!empty($kategori)){
			$stok = $this->getViewStok($row->id);
			$kategori = ($kategori == 'harga') ? $kategori.'_umum' : $kategori;			
			$row->harga = number_format($row->$kategori,0,'.','.');
			$row->stok = $stok;
			//cek spesial case harga
			$row->params = $this->cekProdukParam('harga-gram', $row->id);
			return $row;
		}
	}

	public function getStok($id, $kategori = null)
	{
		$this->db->select('id, stok, nama_produk, harga as harga_umum, barcode, harga_beli, harga_reseller, harga_khusus, terjual');
		$this->db->where('id', $id);
		$row = $this->db->get($this->table)->row();
		if(!empty($kategori)){
			$stok = $this->getViewStok($row->id);
			$kategori = ($kategori == 'harga') ? $kategori.'_umum' : $kategori;			
			$row->harga = $row->$kategori;
			$row->stok = $stok;
		}
		return $row;
	}

	public function produkTerlaris()
	{
		return $this->db->query('SELECT nama_produk, penjualan as terjual FROM `view_kartu_stok_outlet` 
		ORDER BY CONVERT(penjualan,decimal)  DESC LIMIT 5')->result();
	}

	public function dataStok()
	{
		return $this->db->query('SELECT nama_outlet, nama_produk, stok FROM `view_kartu_stok_outlet` ORDER BY CONVERT(stok, decimal) DESC LIMIT 50')->result();
	}
	public function getViewStok($produk_id){
		$outlet_id = $this->session->userdata('outlet_id');
		//cek role
		if($this->session->userdata('role') == 'admin'){
			$outlet_id = $this->session->userdata('edit_barang_outlet');
		}
		// $query = $this->db->query("SELECT stok FROM view_kartu_stok_outlet WHERE outlet_id = '".$outlet_id."' AND barcode = '".$produk_id."' LIMIT 1")->row();
		$query = $this->db->query(
			"SELECT
			(CASE((sum((
					CASE
						WHEN ( `stok_keluar`.`Keterangan` = 'outlet' ) THEN
						`stok_keluar`.`jumlah` ELSE 0 
						END 
					)) - ( 
					sum(( CASE WHEN ( `stok_keluar`.`Keterangan` = 'penjualan' ) THEN `stok_keluar`.`jumlah` ELSE 0 END )) % 1 )) > 0) 
				WHEN 1 THEN
				round((
						sum((
							CASE
								WHEN ( `stok_keluar`.`Keterangan` = 'outlet' ) THEN
								`stok_keluar`.`jumlah` ELSE 0 
							END 
								)) - sum((
							CASE
								WHEN ( `stok_keluar`.`Keterangan` = 'penjualan' ) THEN
								`stok_keluar`.`jumlah` ELSE 0 
							END 
							))),
							2 
							) ELSE (
							sum((
								CASE							
										WHEN ( `stok_keluar`.`Keterangan` = 'outlet' ) THEN
										`stok_keluar`.`jumlah` ELSE 0 
								END 
							)) - sum((
								CASE								
										WHEN ( `stok_keluar`.`Keterangan` = 'penjualan' ) THEN
										`stok_keluar`.`jumlah` ELSE 0 
								END 
								))) 
							END 
			) AS stok
			FROM stok_keluar
			WHERE stok_keluar.outlet_id > 0 AND stok_keluar.outlet_id = '".$outlet_id."' AND stok_keluar.barcode = '".$produk_id."'
			LIMIT 1
			"
		)->row();
		return (!empty($query)) ? $query->stok : 0;
	}
	public function getProdukNama($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		return $this->db->get()->row()->nama_produk;
	}
	public function getProdukBarcode($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		return $this->db->get()->row()->barcode;
	}
	public function getProdukNamaKategori($id){
		$this->db->select('kategori_produk.kategori');
		$this->db->from($this->table);
		$this->db->join('kategori_produk','kategori_produk.id = produk.kategori');
		$this->db->where('produk.id', $id);
		return $this->db->get()->row()->kategori;
	}
}

/* End of file Produk_model.php */
/* Location: ./application/models/Produk_model.php */
