<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_masuk_model extends CI_Model {

	private $table = 'stok_masuk';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function read()
	{
		$this->db->select('stok_masuk.id, stok_masuk.tanggal, stok_masuk.jumlah, stok_masuk.keterangan, produk.barcode, produk.nama_produk,
			(SELECT supplier.nama FROM supplier WHERE stok_masuk.supplier = supplier.id LIMIT 1) as nama_supplier,
			(SELECT pengguna.nama FROM pengguna WHERE stok_masuk.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_masuk.barcode');
		return $this->db->get();
	}
	public function read_filter($tahun = null, $bulan = null)
	{
		if(!empty($tahun) && $tahun != 'all'){
			$this->db->where("YEAR(stok_masuk.tanggal) = '".$tahun."'");
		}
		if(!empty($bulan) && $bulan != 'all'){
			$this->db->where("MONTH(stok_masuk.tanggal) = '".$bulan."'");
		}
		$this->db->select('stok_masuk.id, stok_masuk.tanggal, stok_masuk.jumlah, stok_masuk.keterangan, produk.barcode, produk.nama_produk,
			(SELECT supplier.nama FROM supplier WHERE stok_masuk.supplier = supplier.id LIMIT 1) as nama_supplier,
			(SELECT pengguna.nama FROM pengguna WHERE stok_masuk.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_masuk.barcode');
		return $this->db->get();
	}
	public function read_byid($id)
	{
		$this->db->select('stok_masuk.id, stok_masuk.tanggal, stok_masuk.jumlah, stok_masuk.keterangan, produk.barcode, produk.nama_produk,
			stok_masuk.supplier as id_supplier,
			(SELECT supplier.nama FROM supplier WHERE stok_masuk.supplier = supplier.id LIMIT 1) as nama_supplier,
			(SELECT pengguna.nama FROM pengguna WHERE stok_masuk.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_masuk.barcode');
		$this->db->where('stok_masuk.id', $id);
		return $this->db->get()->row();
	}
	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function laporan($start = null, $end = null)
	{
		$this->db->select('stok_masuk.tanggal, stok_masuk.jumlah, stok_masuk.keterangan, produk.barcode, produk.nama_produk, supplier.nama as supplier');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_masuk.barcode');
		$this->db->join('supplier', 'supplier.id = stok_masuk.supplier', 'left outer');
		if(!empty($start) && !empty($end)){
			$this->db->where('DATE(stok_masuk.tanggal) >=', $start);
			$this->db->where('DATE(stok_masuk.tanggal) <=', $end);
		}
		return $this->db->get();
	}

	public function getStok($id)
	{
		$this->db->select('stok');
		$this->db->where('id', $id);
		return $this->db->get('produk')->row();
	}

	public function addStok($id,$stok)
	{
		$this->db->where('id', $id);
		$this->db->set('stok', $stok);
		return $this->db->update('produk');
	}

	public function stokHari($hari)
	{
		$filter = substr($hari,0,2);
		if(strpos($filter,'-')){
			$hari = '0'.$hari;
		}
		return $this->db->query("SELECT SUM(jumlah) AS total FROM stok_masuk WHERE DATE_FORMAT(tanggal, '%d %m %Y') = '$hari'")->row();
	}

}

/* End of file Stok_masuk_model.php */
/* Location: ./application/models/Stok_masuk_model.php */
