<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiHutang_model extends CI_Model {

	private $table = 'transaksi_hutang';

	public function insertHutang($data, $transaksiId){
		//update status bayar transaksi
		$this->load->model('transaksi_model');
		$update = array();
		
		//cek hutang
		if($data['jumlah_uang'] < $data['total_bayar']){			
			$update['status_bayar'] = 2;

			$diskon = ($data['diskon']) ? $data['diskon'] : 0;
			$totalBayar = $data['jumlah_uang'] + $diskon;
			$sisaBayar = $data['total_bayar'] - $totalBayar;
			$array = array();
			$array['transaksi_id'] = $transaksiId;
			$array['total_bayar'] = $totalBayar;
			$array['sisa_bayar'] = $sisaBayar;
			$array['cicilan_ke'] = 0;
			$array['keterangan'] = 'Down Payment';
			$array['status_bayar'] = 2;
			$array['created_at'] = date('Y-m-d H:i:s');
			$array['created_by'] = $this->session->userdata('id');
			$this->create($array);
		}else{
			$update['status_bayar'] = 1;
		}
		//updating
		return $this->transaksi_model->update($update, $transaksiId);
	}

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function getAll(){
		return $this->db->get($this->table);
	}

	public function getById($idTransaksi){
		$this->db->select("transaksi_hutang.*, pengguna.nama");
		$this->db->from("transaksi_hutang");
		$this->db->join("pengguna", 'pengguna.id = transaksi_hutang.created_by');
//		$this->db->join('transaksi','transaksi.id','transaksi_hutang.transaksi_id')
		$this->db->where("transaksi_hutang.transaksi_id", $idTransaksi);
		$this->db->order_by("transaksi_hutang.created_at", 'ASC');
		return $this->db->get();
	}

	public function getSisaCicilan($idTransaksi){
		$this->db->select('transaksi_hutang.*, transaksi.total_bayar as total_hutang');
		$this->db->from("transaksi_hutang");
		$this->db->join('transaksi','transaksi.id = transaksi_hutang.transaksi_id');
		$this->db->where("transaksi_hutang.transaksi_id", $idTransaksi);
		$this->db->order_by('id','DESC');
		$this->db->limit(1);
		return $this->db->get();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function update($data,$id)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}


}

/* End of file Transaksihutang_model.php */
/* Location: ./application/models/Transaksihutang_model.php */
