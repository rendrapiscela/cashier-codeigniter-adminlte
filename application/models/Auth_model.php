<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function login($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		return $this->db->get('pengguna')->row();
	}

	public function getUser($username)
	{
		$this->db->where('username', $username);
		return $this->db->get('pengguna');
	}

	public function getToko()
	{
		$this->db->select('nama, alamat, logo');
		return $this->db->get('toko')->row();
	}
	public function getOutletUser($idpengguna){
		$this->db->select('pengguna_outlet.*, outlet.nama_outlet');
		$this->db->from('pengguna_outlet');
		$this->db->join('outlet','outlet.id = pengguna_outlet.outlet_id','left');
		$this->db->where('pengguna_id', $idpengguna);
		return $this->db->get()->row();
	}	

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */
