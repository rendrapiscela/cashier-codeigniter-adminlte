<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu_stok_model extends CI_Model {

	private $stok_gudang = 'view_kartu_stok';
	private $stok_outlet = 'view_kartu_stok_outlet';

	public function create($data)
	{
		return $this->db->insert($this->stok_gudang, $data);
	}

	public function read_gudang($kategori = null)
	{
		if(!empty($kategori)){
			$this->db->where('kategori_id', $kategori);
		}
		$this->db->from($this->stok_gudang);
		return $this->db->get();
	}

	public function read_outlet($kategori = null, $outlet = null)
	{
		if(!empty($kategori)){
			$this->db->where('kategori_id', $kategori);
		}
		if(!empty($outlet)){
			$this->db->where('outlet_id', $outlet);
		}
		$this->db->from($this->stok_outlet);
		$this->db->where('outlet_id !=','1');
		return $this->db->get();
	}

	public function read_outlet_filter($kategori = null, $outlet = null, $status = null)
	{
		if(!empty($kategori)){
			$this->db->where('kategori_id', $kategori);
		}
		if(!empty($outlet)){
			$this->db->where('outlet_id', $outlet);
		}
		if(!empty($status)){
			//hampir habis < 20
			if($status == 'kurang_dari'){
				$this->db->where('stok <= 20');
			}
			//tampilkan semua
		}
		$this->db->from($this->stok_outlet);
		$this->db->where('outlet_id !=','1');
		return $this->db->get();
	}

	public function getById($id)
	{
		$this->db->from($this->stok_gudang);
		$this->db->where('id',$id);
		return $this->db->get();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->stok_gudang, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->stok_gudang);
	}


}

/* End of file Kartu_stok_model.php */
/* Location: ./application/models/Outlet_model.php */
