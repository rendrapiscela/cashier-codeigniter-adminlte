<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_keluar_model extends CI_Model {

	private $table = 'stok_keluar';

	public function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function read()
	{
		$this->db->select('stok_keluar.id, stok_keluar.tanggal, stok_keluar.jumlah, stok_keluar.keterangan, produk.barcode, produk.nama_produk,
			stok_keluar.outlet_id,
			(SELECT outlet.nama_outlet FROM outlet WHERE outlet.id = stok_keluar.outlet_id LIMIT 1) as nama_outlet,
			(SELECT pengguna.nama FROM pengguna WHERE stok_keluar.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_keluar.barcode');
		return $this->db->get();
	}	
	public function read_filter($tahun = null, $bulan = null, $outlet = null, $status = null, $start = null, $end = null)
	{
		if(!empty($start) && !empty($end)){
			$this->db->where('DATE(stok_keluar.tanggal) >=', $start);
			$this->db->where('DATE(stok_keluar.tanggal) <=', $end);
		}else{
			if(!empty($tahun) && $tahun != 'all'){
				$this->db->where("YEAR(stok_keluar.tanggal) = '".$tahun."'");
			}
			if(!empty($bulan) && $bulan != 'all'){
				$this->db->where("MONTH(stok_keluar.tanggal) = '".$bulan."'");
			}
		}

		if(!empty($outlet) && $outlet != 'all'){
			$this->db->where("stok_keluar.outlet_id = '".$outlet."'");
		}		
		if(!empty($status) && $status != 'all'){
			$this->db->where("stok_keluar.keterangan = '".$status."'");
		}

		$this->db->select('stok_keluar.id, stok_keluar.tanggal, stok_keluar.jumlah, stok_keluar.keterangan, produk.barcode, produk.nama_produk,
			stok_keluar.outlet_id,
			(SELECT outlet.nama_outlet FROM outlet WHERE outlet.id = stok_keluar.outlet_id LIMIT 1) as nama_outlet,
			(SELECT pengguna.nama FROM pengguna WHERE stok_keluar.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_keluar.barcode');
		return $this->db->get();
	}	

	public function read_byid($id)
	{
		$this->db->select('stok_keluar.id, stok_keluar.tanggal, stok_keluar.jumlah, stok_keluar.keterangan, produk.barcode, produk.nama_produk,
			stok_keluar.outlet_id,
			(SELECT outlet.nama_outlet FROM outlet WHERE outlet.id = stok_keluar.outlet_id LIMIT 1) as nama_outlet,
			(SELECT pengguna.nama FROM pengguna WHERE stok_keluar.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_keluar.barcode');
		$this->db->where('stok_keluar.id', $id);
		return $this->db->get()->row();
	}	

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function laporan_limit($start = null, $end = null)
	{
		$this->db->select('stok_keluar.tanggal, stok_keluar.jumlah, stok_keluar.keterangan, produk.barcode, produk.nama_produk');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_keluar.barcode');
		if(!empty($start) && !empty($end)){
			$this->db->where('DATE(stok_keluar.tanggal) >=', $start);
			$this->db->where('DATE(stok_keluar.tanggal) <=', $end);
		}else{
		$this->db->order_by('stok_keluar.tanggal','DESC');
		$this->db->limit(10);
		}
		return $this->db->get();
	}

	public function laporan($start = null, $end = null, $outlet = null, $status = null)
	{
		$this->db->select('stok_keluar.id, stok_keluar.tanggal, stok_keluar.jumlah, stok_keluar.keterangan, produk.barcode, produk.nama_produk,
			stok_keluar.outlet_id,
			(SELECT outlet.nama_outlet FROM outlet WHERE outlet.id = stok_keluar.outlet_id LIMIT 1) as nama_outlet,
			(SELECT pengguna.nama FROM pengguna WHERE stok_keluar.pengguna_id = pengguna.id LIMIT 1) as nama_pengguna
		');
		$this->db->from($this->table);
		$this->db->join('produk', 'produk.id = stok_keluar.barcode');
		if(!empty($start) && !empty($end)){
			$this->db->where('DATE(stok_keluar.tanggal) >=', $start);
			$this->db->where('DATE(stok_keluar.tanggal) <=', $end);
		}
		if(!empty($outlet) && $outlet != 'all'){
			$this->db->where('stok_keluar.outlet_id', $outlet);
		}
		if(!empty($status) && $status != 'all'){
			$this->db->where("stok_keluar.keterangan = '".$status."'");
		}		
		return $this->db->get();
	}

	public function getStok($id)
	{
		$this->db->select('stok');
		$this->db->where('id', $id);
		return $this->db->get('produk')->row();
	}

	public function addStok($id,$stok)
	{
		$this->db->where('id', $id);
		$this->db->set('stok', $stok);
		return $this->db->update('produk');
	}

	public function stok_keluar_reset($idproduk, $qty)
	{
		$this->db->where('barcode', $idproduk);
		$this->db->where('jumlah', $qty);
		$this->db->where('keterangan', 'penjualan');
		$this->db->where('outlet_id', $this->session->userdata('edit_barang_outlet'));
		$this->db->limit(1);
		return $this->db->delete('stok_keluar');
	}

	public function reset_produk_stok($idproduk, $qty){
		$this->db->from('produk');
		$this->db->where('id', $idproduk);
		$produk = $this->db->get()->row();
		//setting update
		$data = array();
		$data['stok'] = ($produk->stok + $qty);
		$data['terjual'] = ($produk->terjual - $qty);
		
		$this->updateProduk($data, $idproduk);
	}

	public function updateProduk($data, $id){
		$this->db->where('id', $id);
		return $this->db->update('produk', $data);
	}

}

/* End of file Stok_keluar_model.php */
/* Location: ./application/models/Stok_keluar_model.php */
