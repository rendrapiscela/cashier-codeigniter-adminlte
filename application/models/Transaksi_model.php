<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model {

	private $table = 'transaksi';

	public function removeStok($id, $stok)
	{
		$this->db->where('id', $id);
		$this->db->set('stok', $stok);
		return $this->db->update('produk');
	}

	public function addTerjual($id, $jumlah)
	{
		$this->db->where('id', $id);
		$this->db->set('terjual', $jumlah);
		return $this->db->update('produk');;
	}

	public function create($data)
	{
		$this->load->model('transaksihutang_model');
		$this->db->insert($this->table, $data);
		$lastId = $this->db->insert_id();
		$this->transaksihutang_model->insertHutang($data, $lastId);	
		return $lastId;	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function read()
	{
		$this->db->select('transaksi.id, transaksi.tanggal, transaksi.barcode, transaksi.qty, transaksi.total_bayar, transaksi.jumlah_uang, transaksi.diskon, pelanggan.nama as pelanggan, transaksi.harga, transaksi.kategori_harga, transaksi.keterangan, transaksi.persen_diskon, transaksi.persen_diskon_harga, transaksi.total_persen');
		$this->db->from($this->table);
		$this->db->join('pelanggan', 'transaksi.pelanggan = pelanggan.id', 'left outer');
		return $this->db->get();
	}

	public function readbyid($id)
	{
		$this->db->select('transaksi.id, transaksi.tanggal, transaksi.barcode, transaksi.qty, transaksi.total_bayar, transaksi.jumlah_uang, transaksi.diskon, transaksi.pelanggan as id_pelanggan, pelanggan.nama as pelanggan, transaksi.harga, transaksi.kategori_harga, transaksi.keterangan, transaksi.kasir,transaksi.persen_diskon, transaksi.persen_diskon_harga, transaksi.total_persen');
		$this->db->from($this->table);
		$this->db->where('transaksi.id', $id);
		$this->db->join('pelanggan', 'transaksi.pelanggan = pelanggan.id', 'left outer');
		return $this->db->get()->row();
	}

	public function filterDate($start, $end, $outlet = null, $statusBayar = null)
	{

		$this->db->select('transaksi.id, transaksi.tanggal, transaksi.barcode, transaksi.qty, transaksi.total_bayar, transaksi.jumlah_uang, transaksi.diskon, pelanggan.nama as pelanggan, transaksi.harga, transaksi.kategori_harga, transaksi.kasir, view_pengguna.nama_pengguna as nama_kasir, view_pengguna.outlet_id, transaksi.keterangan, transaksi.status_bayar, transaksi.persen_diskon_harga, transaksi.total_persen');
		$this->db->from($this->table);
		$this->db->join('view_pengguna','view_pengguna.id = transaksi.kasir');
		$this->db->join('pelanggan', 'transaksi.pelanggan = pelanggan.id', 'left outer');
		if(!empty($start) && !empty($end)){
			$this->db->where('DATE(transaksi.tanggal) >=', $start);
			$this->db->where('DATE(transaksi.tanggal) <=', $end);
		}else{
			//default hari ini
			$this->db->where('DATE(transaksi.tanggal) =', date('Y-m-d'));
		}
		//cek jika ada outlet
		if((isset($outlet) && $outlet != '')){
			if($outlet != 'all'){
				$this->db->where('view_pengguna.outlet_id', $outlet);
			}
		}
		//role selain admin
		if($this->session->userdata('role') != 'admin'){
			$this->db->where('view_pengguna.outlet_id', $this->session->userdata('outlet_id'));
		}
		//cek jika ada status bayar
		if((isset($statusBayar) && $statusBayar != '')){
			$this->db->where('transaksi.status_bayar', $statusBayar);
		}

		return $this->db->get();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

	public function getProduk($barcode, $qty)
	{
		$total = explode(',', $qty);
		foreach ($barcode as $key => $value) {
			$this->db->select('nama_produk');
			$this->db->where('id', $value);
			$data[] = '<tr><td>'.$this->db->get('produk')->row()->nama_produk.' ('.$total[$key].')</td></tr>';
		}
		return join($data);
	}

	public function getProdukNotQty($barcode, $qty)
	{
		$total = explode(',', $qty);
		foreach ($barcode as $key => $value) {
			$this->db->select('nama_produk');
			$this->db->where('id', $value);
			$data[] = '<tr><td>'.$this->db->get('produk')->row()->nama_produk.'</td></tr>';
		}
		return join($data);
	}
	
	public function getQty($qty)
	{
		$total = explode(',', $qty);
		foreach ($total as $key) {
			$data[] = '<tr><td>'.$key.'</td></tr>';
		}
		return join($data);
	}


	public function penjualanBulan($date)
	{
		$filter = substr($date,0,2);
		if(strpos($filter,'-')){
			$date = '0'.$date;
		}
		$qty = $this->db->query("SELECT qty FROM transaksi WHERE DATE_FORMAT(tanggal, '%d-%m-%Y') like '$date%'")->result();
		$d = [];
		$data = [];
		foreach ($qty as $key) {
			$d[] = explode(',', $key->qty);
		}
		foreach ($d as $key) {
			$data[] = array_sum($key);
		}
		return $data;
	}

	public function transaksiHari($hari)
	{
		$filter = substr($hari,0,2);
		if(strpos($filter,'-')){
			$hari = '0'.$hari;
		}
		return $this->db->query("SELECT COUNT(*) AS total FROM transaksi WHERE DATE_FORMAT(tanggal, '%d %m %Y') = '$hari'")->row();
	}

	public function transaksiTerakhir($hari)
	{
		$filter = substr($hari,0,2);
		if(strpos($filter,'-')){
			$hari = '0'.$hari;
		}
		return $this->db->query("SELECT SUM(transaksi.total_bayar-transaksi.diskon) as total_penjualan FROM transaksi WHERE DATE_FORMAT(tanggal, '%d %m %Y') = '$hari' LIMIT 1")->row();
	}

	public function getAll($id)
	{
		$this->db->select('transaksi.nota, transaksi.tanggal, transaksi.barcode, transaksi.qty, transaksi.total_bayar, transaksi.jumlah_uang, pengguna.nama as kasir, transaksi.harga, transaksi.kategori_harga, transaksi.pelanggan, transaksi.diskon, transaksi.persen_diskon, transaksi.persen_diskon_harga, transaksi.total_persen, transaksi.keterangan');
		$this->db->from('transaksi');
		$this->db->join('pengguna', 'transaksi.kasir = pengguna.id');
		$this->db->where('transaksi.id', $id);
		return $this->db->get()->row();
	}

	public function getName($barcode)
	{
		foreach ($barcode as $b) {
			$this->db->select('nama_produk, harga');
			$this->db->where('id', $b);
			$data[] = $this->db->get('produk')->row();
		}
		return $data;
	}
	
	public function getNameKategori($barcode, $kategori)
	{		
		foreach ($barcode as $idx => $b) {
			$this->db->select('nama_produk');
			$this->db->where('id', $b);
			//get kategori harga
			if($kategori[$idx] == "Harga Umum"){
				$this->db->select('harga');
			}
			else if($kategori[$idx] == "Harga Grosir/Khusus"){
				$this->db->select('harga_khusus as harga');
			}
			else if($kategori[$idx] == "Harga Reseller"){
				$this->db->select('harga_reseller as harga');
			}
			else{
				$this->db->select('harga');
			}

			$data[] = $this->db->get('produk')->row();
		}
		return $data;
	}

}

/* End of file Transaksi_model.php */
/* Location: ./application/models/Transaksi_model.php */
