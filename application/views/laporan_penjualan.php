<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laporan Penjualan</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.css') ?>">
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>">
  <?php $this->load->view('partials/head'); ?>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Laporan Penjualan</h1>
					</div><!-- /.col -->
					<div class="col-md-6" style="background:white; padding:1%;">
					<?php if($this->session->userdata('role') == 'admin'){ ?>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
							<span class="input-group-text">&nbsp;Outlet &nbsp;&nbsp;<i class="fa fa-home"></i></span>
							</div>
							<select name="outlet" id="outlet" class="form-control select2"></select>
						</div>
					</div>
					<?php } ?>
					<!-- form-group -->	
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
							<span class="input-group-text">&nbsp;Pembayaran &nbsp; <i class="fa fa-book"></i></span>
							</div>
							<select name="status_bayar" id="status_bayar" class="form-control select2">
								<option value="1">Lunas</option>
								<option value="2">Piutang</option>
							</select>
						</div>
					</div>
					<!-- form-group -->	
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">
									Tanggal &nbsp;&nbsp; <i class="far fa-calendar-alt"></i>
								</span>
							</div>
							<input type="text" class="form-control float-right" id="reservation">
						</div>
						<!-- /.input group -->
					</div>
					<!-- /.form group -->		
						<div style="display: none; background:white; padding:5px;" id="loading">
							<img src="<?php echo base_url('assets/loading_dutajayaputra.gif') ?>" width="100" height="80">
							<span class="text-danger" style="font-size:13pt;"><b>Mohon Tunggu...</b></span>
						</div>			
					</div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
	  <a href="javascript:;" onclick="report_excel()" id="export_excel" class="btn btn-sm btn-success"><i class="fas fa-file"></i> Export Excel</a> 
	  <a href="#" onclick="report_print()" id="print" class="btn btn-sm btn-primary"><i class="fas fa-print"></i> Print</a> 
	  <a href="#" onclick="report_print_rekap()" id="print_rekap" class="btn btn-sm btn-warning"><i class="fas fa-print"></i> Print Rekap</a> 
	  <a href="#" onclick="report_print_kategori()" id="print_kategori" class="btn btn-sm btn-danger"><i class="fas fa-print"></i> Print Per Kategori</a> 

        <div class="card">
		<div class="card-body">
            <table class="table w-100 table-bordered table-hover" id="laporan_penjualan" class="display" style="width:100%;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th style="width: 50%;">Nama Produk</th> 
                  <th>Qty</th> 
                  <th>Total Bayar</th> 
                  <th>Jumlah Uang</th> 
                  <th>Diskon</th> 
                  <th>Pelanggan</th> 
                  <th>Kasir</th> 
                  <th>Action</th> 
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
	<!-- /.content-wrapper -->
	
<div class="modal fade" id="formCicilan">
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Sisa Pembayaran </h5> &nbsp; <span style="font-size:16pt; color:darkgreen;" class="sisa_pembayaran"></span>
				<button class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="form-cicilan">
					<div class="form-group">
						<label>Tanggal</label>
						<input type="text" class="form-control input-group date" name="tanggal" id="tanggal" required>
						<input type="hidden" name="transaksi_id" value="" id="transaksi_id"/>
						<input type="hidden" name="sisa_bayar" value="" id="sisa_bayar"/>
						<input type="hidden" name="cicilan_ke" value="" id="cicilan_ke"/>
					</div>
					<div class="form-group">
						<label>Jumlah Uang</label>
						<input placeholder="Jumlah Uang" type="number" class="form-control" id="jumlah_uang" name="jumlah_uang" onkeyup="uangbayar()" required>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<textarea name="keterangan" id="keterangan" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<div id="notif"></div>
					</div>
					<button id="add" class="btn btn-success" type="submit" onclick="bayar()">Bayar</button>
					<button id="cetak" class="btn btn-success" type="submit" onclick="bayarCetak()">Bayar Dan Cetak</button>
					<button class="btn btn-danger" data-dismiss="modal">Close</button>
				</form>
			</div>
		</div>
		</div>
</div>

<div class="modal fade" id="detailCicilan">
		<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Total Pembelian </h5> &nbsp; <span style="font-size:16pt; color:darkgreen;" id="total_hutang"></span>
				<button class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr class="info" style="background-color:darkorange; color:white;">
							<th>Tanggal</th>
							<th>Cicilan Ke</th>
							<th>Pembayaran</th>
							<th>Sisa Bayar</th>
							<th>Keterangan</th>
							</tr>
						</thead>
						<tbody id="history"></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button name="bayarCicilan" id="bayarCicilan" class="btn btn-danger">Bayar Cicilan</button>
			</div>
		</div>
		</div>
</div>

</div>
<!-- ./wrapper -->
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<!-- <script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script> -->
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js"') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<!-- <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/pdfmake/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jszip/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>


<script>
  var readUrl = '<?php echo site_url('transaksi/read') ?>';
  var outletUrl = '<?php echo site_url('outlet/get_option_all') ?>';
	var deleteUrl = '<?php echo site_url('transaksi/delete') ?>';
	var filterDate = "<?php echo site_url('laporan_penjualan/filter_date'); ?>";
	var addCicilan = "<?php echo site_url('transaksi/cicilan'); ?>";
	var viewDetailCicilan = "<?php echo site_url('transaksi/detail_cicilan'); ?>";
	var getSisaCicilan = "<?php echo site_url('transaksi/sisa_cicilan'); ?>";
	var linkPrint = "<?php echo site_url('transaksi/cetak'); ?>";
	var exportExcel = "<?php echo site_url('laporan_penjualan/export_excel'); ?>";
	var downloadExcel = "<?php echo site_url('laporan_penjualan/download'); ?>";
	var printRekap = "<?php echo site_url('laporan_penjualan/print_rekap_produk'); ?>";
	var printKategori = "<?php echo site_url('laporan_penjualan/print_rekap_kategori'); ?>";
</script>
<script src="<?php echo base_url('assets/js/unminify/laporan_penjualan.js?v='.$this->session->userdata('version')) ?>"></script>
</body>
</html>
