<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Kartu Stok Outlet</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
  <?php $this->load->view('partials/head'); ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Kartu Stok Persediaan Outlet</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
		<section class="content">
      <div class="container-fluid">
        <div class="card">
					<div class="card-header">
						
						<div class="row">
							<div class="col-md-4">	
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Urutkan</span>
										</div>
										<select name="status" id="status" class="form-control" onchange="changeStatus()">
											<option value="kurang_dari">Hampir Habis < 20</option>
											<option value="all">Tampilkan Semua Produk</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">	
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Kategori</span>
										</div>
										<select name="kategori" id="kategori" class="form-control select2" onchange="changeKategori()">
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">	
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Outlet</span>
										</div>
										<select name="outlet" id="outlet" class="form-control select2" onchange="changeOutlet()">
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2 loading" style="display: none;">
							<img src="<?php echo base_url('assets/loading_dutajayaputra.gif') ?>" width="100" height="80"><br/>
							<span class="text-danger" style="font-size:13pt;"><b>Mohon Tunggu...</b></span>
							</div>
						</div>

          </div>
          <div class="card-body">
            <table class="table w-100 table-bordered table-hover" id="stok_outlet" class="display">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Outlet</th>
                  <th>Kategori</th>
                  <th>Barcode</th>
                  <th>Nama Produk</th>
                  <th>Transfer Dari Gudang</th>
                  <th>Total Penjualan</th>
                  <th>Stok Barang Saat Ini</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  </div>
  <!-- /.content-wrapper -->

</div>

<!-- ./wrapper -->
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js"') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>

<!-- <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
 <script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/pdfmake/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jszip/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>
<script>
  var readUrl = '<?php echo site_url('kartu_stok/read_outlet') ?>';
  var kategoriUrl = '<?php echo site_url('kategori_produk/search') ?>';
  var outletUrl = '<?php echo site_url('outlet/get_option_outlet') ?>';
</script>
<script src="<?php echo base_url(); ?>assets/js/unminify/kartu_stok_outlet.js"></script>
</body>
</html>
