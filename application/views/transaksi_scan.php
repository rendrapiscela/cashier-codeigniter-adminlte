<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Transaksi</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>">
  <?php $this->load->view('partials/head'); ?>
  <style>
    @media(max-width: 576px){
      .nota{
        justify-content: center !important;
        text-align: center !important;
      }
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Transaksi Scanner</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
        <div class="row">
          <div class="col-sm-6">
					<div class="form-group">
              <label>Kategori Harga</label>
              <div class="form-inline">
                <select class="form-control select2 col-sm-6" id="kategori_harga" onchange="getHargaKategori()">
									<option value="harga">Harga Umum</option>
									<option value="harga_khusus">Harga Grosir/Khusus</option>
									<option value="harga_reseller">Harga Reseller</option>
								</select>                
              </div>
              <small class="form-text text-muted"></small>
            </div>
            <div class="form-group">
              <label>Barcode</label>
              <div class="form-inline">
							<div style="border: dotted 3px red; padding:10px;">														
								<canvas style="width:100%;"></canvas>
							</div>	
							<div style="display: none;">
							<select id="barcode" class="form-control select2 col-sm-12" onchange="getNama()">
							<?php 
								echo "<option value=''>Pilih Barang</option>";
							foreach($produk as $row){
								echo "<option value='".$row->id."'>".$row->nama_produk."</option>";
							}
							?>
							</select>
							</div>
							<span style="text-align: center;">
								<span class="ml-3 text-muted" id="harga_produk"></span>
								<h3 class="ml-6 text-muted" id="nama_produk"></h3>
								<h3 class="form-text text-muted" id="sisa"></h3>
							</span>
              </div>
						</div>
						
            <div class="form-group">
              <label>Jumlah</label>
              <input type="number" class="form-control col-sm-6" placeholder="Jumlah" id="jumlah" onkeyup="checkEmpty()">
            </div>
            <div class="form-group">
              <button id="tambah" class="btn btn-success" onclick="checkStok()" disabled>Tambah</button>
              <button id="bayar" class="btn btn-success" data-toggle="modal" data-target="#modal" disabled>Bayar</button>
            </div>
          </div>
          <div class="col-sm-6 d-flex justify-content-end text-right nota">
            <div>
              <div class="mb-0">
                <b class="mr-2">Nota</b> <span id="nota"></span>
              </div>
              <span id="total" style="font-size: 80px; line-height: 1" class="text-danger">0</span>
            </div>
          </div>
        </div>
        </div>
        <div class="card-body">
          <table class="table w-100 table-bordered table-hover" id="transaksi">
            <thead>
              <tr>
								<th>Kategori Harga</th>
								<th>Barcode</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<div class="modal fade" id="modal">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title">Bayar</h5>
    <button class="close" data-dismiss="modal">
      <span>&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <form id="form">
      <div class="form-group">
        <label>Tanggal</label>
        <input type="text" class="form-control" name="tanggal" id="tanggal" required>
      </div>
      <div class="form-group">
        <label>Pelanggan <div class="btn btn-sm btn-primary" onclick="addPelanggan()"><i class="fa fa-plus"></i></div></label>
        <select name="pelannggan" id="pelanggan" class="form-control select2"></select>
      </div>
      <div class="form-group">
        <label>Jumlah Uang</label>
        <input placeholder="Jumlah Uang" type="number" class="form-control" name="jumlah_uang" onkeyup="kembalian()" required>
      </div>
      <div class="form-group">
        <label>Diskon</label>
        <input placeholder="Diskon" type="number" class="form-control" onkeyup="kembalian()" name="diskon">
      </div>
      <div class="form-group">
        <b>Total Bayar:</b> <span class="total_bayar"></span>
      </div>
      <div class="form-group">
        <b>Kembalian:</b> <span class="kembalian"></span>
      </div>
      <button id="add" class="btn btn-success" type="submit" onclick="bayar()" disabled>Bayar</button>
      <button id="cetak" class="btn btn-success" type="submit" onclick="bayarCetak()" disabled>Bayar Dan Cetak</button>
      <button class="btn btn-danger" data-dismiss="modal">Close</button>
    </form>
  </div>
</div>
</div>
</div>
<div class="modal fade" id="modal-pelanggan">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title">Daftarkan Pelanggan</h5>
    <button class="close" data-dismiss="modal">
      <span>&times;</span>
    </button>
  </div>
  <div class="modal-body">
	<form id="form-pelanggan">
      <input type="hidden" name="id">
      <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" placeholder="Nama" name="nama" required>
      </div>
      <div class="form-group">
        <label>Jenis Kelamin</label>
        <select name="jenis_kelamin" class="form-control">
          <option value="Pria">Pria</option>
          <option value="Wanita">Wanita</option>
          <option value="Lainya">Lainya</option>
        </select>
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control" placeholder="Alamat" name="alamat" required>
      </div>
      <div class="form-group">
        <label>Telepon</label>
        <input type="number" class="form-control" placeholder="Telepon" name="telepon" required>
      </div>
      <div class="btn btn-success" type="submit" onclick="insertPelanggan()">Add</div>
      <div class="btn btn-danger" data-dismiss="modal">Close</div>
    </form>
  </div>
</div>
</div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/webcam/qrcodelib.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/webcam/webcodecamjquery.js"></script>

<script>
  var produkGetNamaUrl = '<?php echo site_url('produk/get_nama') ?>';
  var produkGetStokUrl = '<?php echo site_url('produk/get_stok') ?>';
  var addUrl = '<?php echo site_url('transaksi/add') ?>';
  var getBarcodeUrl = '<?php echo site_url('produk/get_barcode') ?>';
  var pelangganSearchUrl = '<?php echo site_url('pelanggan/search') ?>';
  var cetakUrl = '<?php echo site_url('transaksi/cetak/') ?>';
</script>
<script src="<?php echo base_url('assets/js/unminify/transaksi.js') ?>"></script>
<script type="text/javascript">
		var arg = {
			resultFunction: function(result) {
				var kode_barang = result.code;
				$.ajax({
								url: '<?php echo base_url()."transaksi/getbarcode?kode_barang=";?>'+kode_barang+'&kategori_harga='+$("#kategori_harga").val(),
								type: 'GET',
								dataType: 'json',
								success: function(data){									
									$("#barcode").val(data.id);
										if(data.id != ''){
													$("#nama_produk").html(data.nama_produk);
													$("#sisa").html(`Sisa ${data.stok}`);
													$("#harga_produk").html('<b>Harga : '+data.harga+'</b>');
													checkEmpty()
										}	else {
													$("#nama_produk").html('<i style="color:orange;"> Produk Anda Belum Terdaftar!</i>');
													$("#sisa").html('');
													checkEmpty()
										}

									// $.ajax({
									// 		url: produkGetNamaUrl,
									// 		type: "post",
									// 		dataType: "json",
									// 		data: {
									// 				id: data.id
									// 		},
									// 		success: res => {
									// 			// if(res.id != ''){
									// 				$("#nama_produk").html(res.nama_produk);
									// 				$("#sisa").html(`Sisa ${res.stok}`);
									// 				checkEmpty()
									// 			// }	else {
									// 			// 	$("#nama_produk").html('<i style="color:orange;"> Produk Anda Belum Terdaftar!</i>');
									// 			// 	$("#sisa").html('');
									// 			// 	checkEmpty()
									// 			// }
									// 		},
									// 		error: err => {
									// 				console.log(err)
									// 		}
									// })
								}//endsuccess
				});//endajax
			}
		};
		$("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery.play(); //scanner active

</script>
</body>
</html>
