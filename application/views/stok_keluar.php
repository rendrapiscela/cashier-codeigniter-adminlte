<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Stok Keluar</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css'); ?>">
  <?php $this->load->view('partials/head'); ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Stok Keluar</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-header">
						<div class="row">
							<div class="col-md-3">
							<button class="btn btn-success" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i> Input Stok Barang Keluar</button>
							</div>
							<div class="col-md-3" style="display: none;">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Periode</span>
										</div>
										<select name="bulan" id="bulan" class="form-control select2" onchange="changeBulan()">
												<?php 
												$bulan = bulan_option();
												$idx = 1;
												$bulanNow = date('n');
												echo "<option value='all'>Semua Bulan</option>";
												foreach($bulan as $row){
													$selectBulan = '';
													if($bulanNow == $idx){
														$selectBulan = 'selected';
													}
													echo "<option value='".$idx."' $selectBulan>".$row."</option>";
													$idx++;
												}
												?>
										</select>
										&nbsp;&nbsp;
										<select name="tahun" id="tahun" class="form-control select2" onchange="changeTahun()">
												<?php 
													$tahun = tahun_option();
													echo "<option value='all'>Semua Tahun</option>";
													$tahunNow = date('Y');
												foreach($tahun as $row){
													$selectTahun = "";
													if($tahunNow == $row){
														$selectTahun = 'selected';
													}
													echo "<option value='".$row."' $selectTahun>".$row."</option>";
												}
												?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3">	
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Status Stok</span>
										</div>
										<select name="status" id="status" class="form-control select2"> <!-- onchange="changeStatus()" -->
											<option value="all">Semua Status</option>
											<option value="outlet" selected>Kirim Outlet</option>
											<option value="penjualan">Barang Terjual</option>
											<option value="rusak">Rusak</option>
											<option value="hilang">Hilang</option>
											<option value="kadaluarsa">Kadaluarsa</option>
											<option value="mati">Ikan Mati</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3">
							<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
										<span class="input-group-text">&nbsp; Outlet</span>
										</div>
										<select name="outlet" id="outlet" class="form-control select2"> <!-- onchange="changeOutlet()" -->
												<?php 
												echo "<option value='all'>Semua Outlet</option>";
												foreach($outlet->result() as $row){
													echo "<option value='".$row->id."'>".$row->nama_outlet."</option>";
												}
												?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-3">
							<div class="form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											Tanggal &nbsp;&nbsp; <i class="far fa-calendar-alt"></i>
										</span>
									</div>
									<input type="text" class="form-control float-right" id="reservation">
								</div>
								<!-- /.input group -->
							</div>
							</div>
							<div style="display: none; background:white; padding:5px;" id="loading">
								<img src="<?php echo base_url('assets/loading_dutajayaputra.gif') ?>" width="100" height="80">
								<span class="text-danger" style="font-size:13pt;"><b>Mohon Tunggu...</b></span>
							</div>			
						</div>
          </div>
          <div class="card-body">
            <table class="table w-100 table-bordered table-hover" id="stok_keluar">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Barcode</th>
                  <th>Nama Produk</th>
                  <th>Jumlah</th>
									<th>Keterangan</th>
									<th>Outlet</th>
									<th>Actions</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<div class="modal fade" id="modal">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title">Add Data</h5>
    <button class="close" data-dismiss="modal">
      <span>&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <form id="form">
      <div class="form-group tgl">
				<label>Tanggal</label>
				<input type="hidden" value="" name="id"/>
        <input id="tanggal" type="text" class="form-control" placeholder="Kategori" name="tanggal" required>
      </div>
      <div class="form-group">
        <label>Outlet</label>
        <select name="outlet" id="outlet" class="form-control select2">
					<?php 
					if($outlet->num_rows() > 0){
						foreach($outlet->result() as $row){
							if($row->status == 'cabang'){
								echo "<option value='".$row->id."'>".$row->nama_outlet."</option>";
							}
						}
					}
					?>
				</select>
      </div>
      <div class="form-group prd">
        <label>Barcode</label>
				<select name="barcode" id="barcode" class="form-control select2" onchange="getStok()" required></select>				
			</div>
			<div class="form-group">
				<span id="notif_stok"></span>
			</div>
      <div class="form-group">
        <label>Jumlah</label>
        <input type="number" class="form-control" placeholder="Jumlah" name="jumlah" required>
      </div>
      <div class="form-group">
        <label>Keterangan</label>
        <select class="form-control" placeholder="Keterangan" name="keterangan" required>
          <option value="outlet">Kirim Outlet</option>
          <option value="rusak">Rusak</option>
          <option value="hilang">Hilang</option>
          <option value="kadaluarsa">Kadaluarsa</option>
          <option value="mati">Ikan Mati</option>
        </select>
      </div>
      <button class="btn btn-success" id="submitAdd" name="submit" value="add" type="submit">Add</button>
      <button class="btn btn-danger" data-dismiss="modal">Close</button>
    </form>
  </div>
</div>
</div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>

<!-- <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script> -->
 <script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.flash.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/pdfmake/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jszip/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.js') ?>"></script>

<script>
  var readUrl = '<?php echo site_url('stok_keluar/read') ?>';
  var addUrl = '<?php echo site_url('stok_keluar/add') ?>';
  var editUrl = '<?php echo site_url('stok_keluar/edit') ?>';
  var deleteUrl = '<?php echo site_url('stok_keluar/hapus') ?>';
  var getBarcodeUrl = '<?php echo site_url('produk/get_barcode') ?>';
  var getOutlet = '<?php echo site_url('outlet/get_option') ?>';
  var getStokUrl = '<?php echo site_url('produk/get_produk') ?>';
</script>
<script src="assets/js/unminify/stok_keluar.js?v=<?php echo $this->session->userdata('version');?>"></script>
</body>
</html>
