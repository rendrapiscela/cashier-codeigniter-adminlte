<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Cetak Struk</title>
	<style>
		.font-sm{
			font-size:7pt;
		}
		.font-xm{
			font-size:5pt;
		}
		body{
			/* font-family: "Verdana"; */
			font-family: 'Century Gothic', 'Apple Gothic', sans-serif;
			font-size : 7pt;
			margin : 10px 10px;
			/* font-weight: bold;			 */
		}
		center{
			border : 1px dotted;
			padding : 5px;
		}
	</style>	
</head>
<body>
		<div style='text-align:center;'>
		<?php 
		$toko = $this->auth_model->getToko();
		$logo = $toko->logo;
		$path = $logo;
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		echo "<img src=".$base64." class='img-logo img-circle' style='width:70pt;'/>";	
		?>
		<?php 
			//logo bank
			$logoBcaPath = "uploads/Logo-Bca.png";
			$bca = file_get_contents($logoBcaPath);
			$base64bca = 'data:image/png' . ';base64,' . base64_encode($bca);
			//logo mandiri
			$logoMandiriPath = "uploads/Logo-Mandiri.png";
			$mandiri = file_get_contents($logoMandiriPath);
			$base64mandiri = 'data:image/png' . ';base64,' . base64_encode($mandiri);
		?>
			<br/>
			<?php echo "<strong>".$this->session->userdata('toko')->nama."</strong>"; ?><br>
			<b><?php echo $this->session->userdata('toko')->alamat; ?></b><br><br>
		</div>	
			<table width="95%">
				<tr>
					<td><b style="font-size:5pt;"><?php echo $nota ?></b></td>
					<td align="right"><b style="font-size:5pt;"><?php echo $tanggal ?></b></td>
				</tr>
			</table>
			<hr/>
			<table width="95%">
				<tr>
					<td width="50%"></td>
					<td width="3%"></td>
					<td width="10%" align="right"></td>
					<td align="right" width="17%"><b style="font-size:6pt;"><?php echo ucwords($kasir); ?></b></td>
				</tr>
				<?php 
				$subtotal = 0;
				foreach ($produk as $key): ?>
					<tr>
						<td><?php echo $key->nama_produk ?></td>
						<td></td>
						<?php 
						if($key->harga_asli > 0){
							$subtotal += $key->harga_asli;
						}else{
							$subtotal += $key->harga;
						}
						if(!$key->diskon){ //cek diskon
						?>
							<td align="right"><?php echo $key->total.' x '.number_format($key->harga,0,'.','.') ?></td>
							<td align="right"><b><?php echo number_format(($key->harga*$key->total),0,'.','.') ?></b></td>
						<?php }else{ //diskon ?>
							<td align="right"><?php echo $key->total.' x '.number_format($key->harga,0,'.','.') ?></td>
							<td align="right"><b><?php echo "<s>".number_format($key->harga_asli,0,'.','.')."</s> &nbsp;".number_format($key->persendiskonharga,0,'.','.') ?></b></td>
						<?php } ?>	

					</tr>
				<?php endforeach ?>
			</table>
			<hr>
			<table width="95%">
				<tr style="display:none;">
					<td width="75%" align="right">
						<b>Sub Total</b>
					</td>
					<td width="20%" align="right">
						<?php echo number_format($subtotal,0,'.','.') ?>
					</td>
				</tr>
				<tr>
					<td width="75%" align="right">
						<b>Total Diskon Produk</b>
					</td>
					<td width="20%" align="right">
						(<?php echo number_format($diskon+$total_persen,0,'.','.') ?>)
					</td>
				</tr>
			</table>
			<hr>
			<table width="95%" height="90%" border="0">
				<tr style="line-height: 10pt;">
					<td width="79%" align="right">
						<b>Total</b>
					</td>
					<td width="30%" align="right">
						<b><?php echo number_format($total-$diskon,0,'.','.') ?></b>
					</td>
				</tr>
				<tr style="line-height: 10pt;">
					<td width="79%" align="right">
						<b>Bayar</b>
					</td>
					<td width="30%" align="right">
						<?php echo number_format($bayar,0,'.','.') ?>
					</td>
				</tr>
				<tr style="line-height: 10pt;">
					<td width="79%" align="right">
						<b><?php echo ($bayar < $total) ? 'Sisa Pembayaran/Hutang' : 'Kembalian';?></b>
					</td>
					<td width="30%" align="right">
						<?php echo number_format($kembalian,0,'.','.') ?>
					</td>
				</tr>
			</table>
			<div style="display:flex;">
			<table width="100%" border="0">
				<tr>
					<?php if(!empty($keterangan)){ ?>
					<td width="60%" style="border:1px dotted black; padding:10px; text-align:justify;">
						<strong class="font-xm">Keterangan :</strong> <br/>
						<i class="font-xm"><?php echo empty($keterangan) ? '-' : $keterangan; ?></i>
					</td>
					<?php } ?>
				</tr>
			</table>
			</div>
			<br/>
			<!-- Cicilan -->
			<?php if(count($cicilan) > 0){ ?>
			<legend style="text-align:left; color:#1e9acd;"><u>History Pembayaran</u></legend>
			<table width="95%">
				<tr style="background: #ed7d26">
					<td align="center" width="30%">
						<b>Tgl Bayar</b>
					</td>
					<td align="center" width="5%">
						<b>Cicilan Ke</b>
					</td>
					<td align="center">
						<b>Telah Dibayar</b>
					</td>
					<td align="center">
						<b>Sisa Bayar</b>
					</td>
				</tr>
				<?php 
				foreach($cicilan as $row){
					echo "<tr>";
					echo "<td>".$row['created_at']."</td>";
					echo "<td align='center'>".$row['cicilan_ke']."</td>";
					echo "<td align='right'>".number_format($row['total_bayar'],0,'.','.')."</td>";
					echo "<td align='right'>".number_format($row['sisa_bayar'],0,'.','.')."</td>";
					echo "</tr>";
				}
				?>
			</table>
				
			<?php } ?>
			<div style="text-align: center; margin:0 auto;">
			<table border="1" style="border-collapse: collapse; border: 1pt dashed black; margin:0 auto;">
				<tr>
					<td colspan="2" align="center"><strong class="font-xm">Transfer Via A.N YAHYA</strong></td>
				</tr>
				<tr style="border: 1pt dashed black;">
					<td style="text-align:center;"><?php echo "<img src=".$base64bca." class='img-logo img-circle' style='width:30px;'/>"; ?></td>
					<td align="center">&nbsp; &nbsp;<span style="color:#1e9acd; letter-spacing: 0.6px;" class="font-xm"><u>5745 &nbsp; 294 &nbsp; 836</u></span></td>
				</tr>
				<tr style="border: 1pt dashed black;">
					<td style=""><?php echo "<img src=".$base64mandiri." class='img-logo img-circle' style='width:30px;'/>"; ?></td>
					<td align="center" style="padding:10px;"><span style="color:#1e9acd; letter-spacing: 0.6px;" class="font-xm"><u>1730 &nbsp; 09609 &nbsp; 9990</u></span></td>
				</tr>
			</table>
			</div>

			<hr/>

			<div style="text-align: center;">	
			<b class="font-sm">Terima Kasih</b><br>
			<b class="font-sm"><?php echo ucwords($pelanggan) ?></b>
			<br/>
			<i class="font-sm" style="font-size:7pt;">*) Barang yang sudah dibeli, tidak dapat <b>DITUKAR / DIKEMBALIKAN</b>.</i>
			</div>
	<script>
		window.print()
	</script>
</body>
</html>
