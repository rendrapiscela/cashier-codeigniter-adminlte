<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laporan Stok Keluar</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.css') ?>">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
	<?php $this->load->view('partials/head'); ?>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Laporan Stok Keluar</h1>
					</div><!-- /.col -->
					<div class="col-md-6">
            					<!-- /.form group -->
          <?php if($this->session->userdata('role') == 'admin'){ ?>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
							<span class="input-group-text">&nbsp;Outlet &nbsp;&nbsp;<i class="fa fa-home"></i></span>
							</div>
							<select name="outlet" id="outlet" class="form-control select2"></select>
						</div>
					</div>
					<?php } ?>
          
          <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                <span class="input-group-text">&nbsp; Status Stok</span>
                </div>
                <select name="status" id="status" class="form-control">
                  <option value="all">Semua Status</option>
                  <option value="outlet">Kirim Outlet</option>
                  <option value="penjualan">Barang Terjual</option>
                  <option value="rusak">Rusak</option>
                  <option value="hilang">Hilang</option>
                  <option value="kadaluarsa">Kadaluarsa</option>
                  <option value="mati">Ikan Mati</option>
                </select>
              </div>
						</div>

            <!-- form-group -->
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    Tanggal &nbsp;&nbsp; <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" class="form-control float-right" id="reservation">
              </div>
              <!-- /.input group -->
            </div>
            
              <div style="display: none; background:white; padding:5px;" id="loading">
                <img src="<?php echo base_url('assets/loading_dutajayaputra.gif') ?>" width="100" height="80">
                <span class="text-danger" style="font-size:13pt;"><b>Mohon Tunggu...</b></span>
              </div>			
					</div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card">
          <div class="card-body">
            <table class="table w-100 table-bordered table-hover" id="laporan_stok_keluar">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Barcode</th> 
                  <th>Nama Produk</th> 
                  <th>Jumlah</th> 
                  <th>Keterangan</th>
                  <th>Outlet</th>
                  <th>Oleh</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<!-- <script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script> -->
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js"') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.js') ?>"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>


<script>
  var outletUrl = '<?php echo site_url('outlet/get_option_all') ?>';
  var readUrl = '<?php echo site_url('stok_keluar/read') ?>';
  var readUrlFilter = '<?php echo site_url('stok_keluar/filter') ?>';
  var deleteUrl = '<?php echo site_url('transaksi/delete') ?>';
</script>
<!-- <script src="<?php echo base_url('assets/js/unminify/laporan_stok_keluar.js') ?>"></script> -->
<script src="assets/js/unminify/laporan_stok_keluar.js?v=<?php echo $this->session->userdata('version');?>"></script>
</body>
</html>
