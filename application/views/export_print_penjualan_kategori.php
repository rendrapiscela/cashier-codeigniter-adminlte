<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $this->auth_model->getToko()->nama; ?> Penjualan <?php echo $mulai_tanggal." - ".$sampai_tanggal; ?></title>
  <style>
		.font-sm{
			font-size:7pt;
		}
		body{
			/* font-family: "Verdana"; */
			font-family: 'Century Gothic', 'Apple Gothic', sans-serif;
			font-size : 7pt;
			margin : 10px 10px;
			/* font-weight: bold;			 */
		}
		center{
			border : 1px dotted;
			padding : 5px;
		}
	</style>	
</head>
<body>
	<div style='text-align:center;'>
		<?php 
		$toko = $this->auth_model->getToko();
		$logo = $toko->logo;
		$path = $logo;
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$img = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
		echo "<img src=".$base64." class='img-logo img-circle' style='width:70pt;'/>";	
		?>
			<br/>
			<?php echo "<strong>".$this->session->userdata('toko')->nama."</strong>"; ?><br>
			<b><?php echo $this->session->userdata('toko')->alamat; ?></b><br><br>
		</div>	
		<b style="font-size:5pt;">
			Tgl : <?php echo $mulai_tanggal." - ".$sampai_tanggal; ?>
		</b>
		<span style="float:right;"><b style="font-size:8pt;"><?php echo $nama_outlet; ?></b></span>
		<br/>
		<hr>
			<table width="95%">
				<?php 
				$totalQty = 0; $totalPenjualan = 0;
				$summary = array(); 
				foreach($data as $namaKategori => $kategori):
					$summaryQty = 0; $summaryPenjualan = 0;

					echo "<tr>";
					echo "<td><strong><u>".$namaKategori."</u></strong></td>";
					echo "<td colspan='3'>&nbsp;</td>";
					echo "</tr>";

				foreach ($kategori as $val => $key): 
				?>
					<tr>
						<td>&nbsp;&nbsp; <?php echo $key['nama_produk'] ?></td>
						<td></td>
						<td align="right"><?php echo number_format($key['qty'],0,'.','.') ?></td>
						<td align="right"><b><?php echo number_format(($key['subtotal']),0,'.','.') ?></b></td>
					</tr>
				<?php 
					$totalQty += $key['qty'];
					$totalPenjualan += $key['subtotal'];		
					
					$summaryQty += $key['qty']; 
					$summaryPenjualan += $key['subtotal'];
				endforeach; 
					$summary[$namaKategori] = ['qty' => $summaryQty, 'penjualan' => $summaryPenjualan];
				?>

				<?php 
				echo "<tr><td>&nbsp;</td><td colspan='3'>&nbsp;</td></tr>";
			endforeach; ?>
			</table>
			<hr>
			<table width="95%">
				<?php foreach($summary as $category => $keys) :?>
				<tr>
					<td width="75%" align="right">
						<b><?php echo $category; ?></b>
					</td>
					<td width="12%" align="right">
						<b><?php echo number_format($keys['qty'],0,'.','.') ?></b>
					</td>
					<td width="" align="right">
						<b><?php echo number_format($keys['penjualan'],0,'.','.') ?></b>
					</td>
				</tr>
				<?php endforeach; //end catergory ?>
			</table>
			<hr/>
			<table width="95%">
				<tr>
					<td width="75%" align="right">
						<b>Grand Total</b>
					</td>
					<td width="12%" align="right">
						<b><?php echo number_format($totalQty,0,'.','.') ?></b>
					</td>
					<td width="" align="right">
						<b><?php echo number_format($totalPenjualan,0,'.','.') ?></b>
					</td>
				</tr>
				<tr>
					<td width="75%" align="right">
						<b>Total Diskon</b>
					</td>
					<td></td>
					<td width="" align="right">
						<b><?php echo number_format($total_diskon,0,'.','.') ?></b>
					</td>
				</tr>
			</table>
			<hr/>
			<table width="95%">
				<tr>
					<td width="75%" align="right">
						<b>Total Pendapatan</b>
					</td>
					<td></td>
					<td width="" align="right">
						<b><?php echo number_format($totalPenjualan-$total_diskon,0,'.','.') ?></b>
					</td>
				</tr>
			</table>

	<script>
		window.print()
	</script>
</body>
</html>
