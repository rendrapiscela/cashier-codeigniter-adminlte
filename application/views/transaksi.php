<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Transaksi</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>">
  <?php $this->load->view('partials/head'); ?>
  <style>
    @media(max-width: 576px){
      .nota{
        justify-content: center !important;
        text-align: center !important;
      }
    }
		#tombolScrollTop
		{
			cursor: pointer;
			position:fixed;
			right : 1.5%;
			bottom: 0;
			display:none;
			width: 60px;
			text-shadow: 2px 2px 0px #FFFFFF, 5px 4px 0px rgba(0,0,0,0.15);
		}
  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('includes/nav'); ?>

  <?php $this->load->view('includes/aside'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <h1 class="m-0 text-dark">Transaksi</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
      <div class="card">
        <div class="card-header">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Kategori Harga</label>
              <div class="form-inline">
                <select class="form-control select2 col-sm-6" id="kategori_harga" onchange="getHargaKategori()">
									<option value="harga">Harga Umum</option>
									<option value="harga_khusus">Harga Grosir/Khusus</option>
									<option value="harga_reseller">Harga Reseller</option>
								</select>                
              </div>
              <small class="form-text text-muted"></small>
            </div>
           
					  <div class="form-group">
              <label>Produk</label>
              <div class="form-inline">
                <select id="barcode" class="form-control select2 col-sm-12" onchange="getNama()"></select>
                <span style="display:none;" class="ml-3 text-muted" id="nama_produk"></span>
                <badge class="ml-10 badge badge-primary" style="font-size: 15pt;" id="harga_produk"></badge>
								<span id="processing" style="display: none;"> <img src="<?php echo base_url().'assets/loading.gif';?>" width="50"/> <i style="font-size: 10pt;"><b> Sedang Membaca Data..</i> </b></span>
              </div>
              <badge class="form-text badge badge-warning" style="font-size: 13pt;" id="sisa"></badge>
              <badge class="form-text badge badge-success" style="font-size: 9pt;" id="params_keterangan"></badge>
							<input value="" type="hidden" name="params" id="params">
						</div>
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label>Jumlah</label>
									<input type="number" class="form-control" placeholder="Jumlah" value="1" min="1" id="jumlah" onfocus="checkEmpty()">
								</div>
							</div>
							<div class="col-md-7">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input" type="checkbox" id="myCheck" onclick="checkPotongan()">
										<label for="myCheck" class="custom-control-label" style="font-size:9pt;">Ganti Jenis Diskon ?</label>
										<span class="badge badge-info" style="float: right; font-size:9pt;" id="jenisDiskon">Rupiah (Rp)</span> 
									</div>
									<input type="number" class="form-control" name="persen_diskon" value="0" placeholder="" min="0" max="100" id="persen_diskon" onkeyup="countDiskon()" style="margin-top:8px;">
								</div>
								<input type="hidden" class="form-control" name="harga_produk_awal" id="harga_produk_awal" value=""/>
								<input type="hidden" class="form-control" name="harga_produk_setelah_diskon" id="harga_produk_setelah_diskon" value=""/>
							</div>
							<div id="view_harga_setelah_diskon" style="display: none;">
								<label>Harga Setelah Diskon : <span id="harga_diskon"></span> </label>
							</div>
						</div>
						<br/>
            <div class="form-group">
              <button id="tambah" class="btn btn-success" onclick="checkStok()" disabled><i class="fas fa-shopping-cart"></i> Tambah</button>
							&nbsp; &nbsp;
              <button id="hold" onclick="add_hold()" class="btn btn-danger"><i class="fas fa-ban"></i> Hold</button>
							&nbsp; &nbsp;
              <button id="bayar" class="btn btn-primary" data-toggle="modal" data-target="#modal" disabled><i class="fas fa-credit-card"></i> Bayar</button>
            </div>
          </div>
          <div class="col-sm-6 d-flex justify-content-end text-right nota">
            <div>
              <div class="mb-0">
                <b class="mr-2">Nota</b> <span id="nota"></span>
              </div>
              <span id="total" style="font-size: 32pt; line-height: 1; display:none;" class="text-danger">0</span>
              <span id="txtTotal" style="font-size: 32pt; line-height: 1;" class="text-danger">0</span>
            </div>
          </div>
        </div>
        </div>
        <div class="card-body">
          <table class="table w-100 table-bordered table-hover" id="transaksi">
            <thead>
              <tr>
                <th>Kategori Harga</th>
                <th>Barcode</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Diskon (%/Rp)</th>
                <th>Subtotal</th>
                <th>Actions</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<div class="modal fade" id="modal">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title">Total Bayar </h5> &nbsp; <span style="font-size:16pt; color:darkgreen;" class="total_bayar"></span>
    <button class="close" data-dismiss="modal">
      <span>&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <form id="form">
      <div class="form-group">
        <label>Tanggal</label>
        <input type="text" class="form-control" name="tanggal" id="tanggal" required>
      </div>
      <div class="form-group">
        <label>Pelanggan <div class="btn btn-sm btn-primary" onclick="addPelanggan()"><i class="fa fa-plus"></i></div></label>
        <select name="pelannggan" id="pelanggan" class="form-control select2"></select>
      </div>
      <div class="form-group">
        <label>Jumlah Uang</label>
        <input placeholder="Jumlah Uang" type="number" class="form-control" name="jumlah_uang" onkeyup="kembalian()" required>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Potongan Harga Lain</label>
						<input placeholder="Rp." type="number" class="form-control" onkeyup="kembalian()" name="diskon">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="alert alert-success">Subtotal Diskon Produk : <span id="view_subtotal_diskon"></span></label>
						<input placeholder="Subtotal" type="hidden" class="form-control" name="subtotal_diskon" id="subtotal_diskon" readonly>
					</div>
				</div>
			</div>
      <div class="form-group">
        <label>Keterangan</label>
				<textarea name="keterangan" id="keterangan" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <b style="font-size:18pt;">Total Bayar:</b> <input type="number" style="font-size:16pt; display:none;" class="total_bayar" value="0"/>
				<span style="font-size:16pt;" id="txtTotalBayar"></span>
				<br/>
				<b id="textkembalian" style="font-size:18pt;">Kembalian:</b> <span style="font-size:16pt; display:none;" class="kembalian"></span>
				<span style="font-size:16pt;" id="txtKembalian"></span>
      </div>
      <button id="add" class="btn btn-success" type="submit" onclick="bayar()" disabled>Bayar</button>
      <button id="cetak" class="btn btn-success" type="submit" onclick="bayarCetak()" disabled>Bayar Dan Cetak</button>
      <button class="btn btn-danger" data-dismiss="modal">Close</button>
    </form>
  </div>
</div>
</div>
</div>

<div class="modal fade" id="modal-pelanggan">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title">Daftarkan Pelanggan</h5>
    <button class="close" data-dismiss="modal">
      <span>&times;</span>
    </button>
  </div>
  <div class="modal-body">
	<form id="form-pelanggan">
      <input type="hidden" name="id">
      <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" placeholder="Nama" name="nama" required>
      </div>
      <div class="form-group">
        <label>Jenis Kelamin</label>
        <select name="jenis_kelamin" class="form-control">
          <option value="Pria">Pria</option>
          <option value="Wanita">Wanita</option>
          <option value="Lainya">Lainya</option>
        </select>
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <input type="text" class="form-control" placeholder="Alamat" name="alamat" required>
      </div>
      <div class="form-group">
        <label>Telepon</label>
        <input type="number" class="form-control" placeholder="Telepon" name="telepon" required>
      </div>
      <div class="btn btn-success" type="submit" onclick="insertPelanggan()">Add</div>
      <div class="btn btn-danger" data-dismiss="modal">Close</div>
    </form>
  </div>
</div>
</div>
</div>
<!-- ./wrapper -->
<div id="tombolScrollTop" onclick="scrolltotop()" style="text-align: center;">
		<img src="<?php echo base_url().'uploads/logo.png'; ?>" width="60" height="60"/>
		<b style="font-size: 8pt;">Scroll Atas</b>
</div>

<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('partials/footer'); ?>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/select2/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vendor/adminlte/plugins/moment/moment.min.js') ?>"></script>
<script>
  var produkGetNamaUrl = '<?php echo site_url('produk/get_nama') ?>';
  var produkGetStokUrl = '<?php echo site_url('produk/get_stok') ?>';
  var addUrl = '<?php echo site_url('transaksi/add') ?>';
  var holdUrl = '<?php echo site_url('transaksi/add_hold') ?>';
  var getBarcodeUrl = '<?php echo site_url('produk/get_barcode_price') ?>';
  var pelangganSearchUrl = '<?php echo site_url('pelanggan/search') ?>';
	var cetakUrl = '<?php echo site_url('transaksi/cetak/') ?>';
	var addUrlPelanggan = '<?php echo site_url('pelanggan/add') ?>';

</script>
<script src="<?php echo base_url('assets/js/unminify/transaksi.js?v='.$this->session->userdata('version')) ?>"></script>
<script src="<?php echo base_url('assets/js/unminify/transaksi_hold.js?v='.$this->session->userdata('version')) ?>"></script>
<script>
	$(window).scroll(function(){
		if ($(window).scrollTop() > 120) {
			$('#tombolScrollTop').fadeIn();
		} else {
			$('#tombolScrollTop').fadeOut();
		}
	});
	function scrolltotop()
{
	$('html, body').animate({scrollTop : 0},500);
}
</script>
</body>
</html>
