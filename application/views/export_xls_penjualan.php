<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Excel Penjualan</title>
</head>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=".$title.'_'.$mulai_tanggal.'_sampai_'.$sampai_tanggal.".xls");
?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
		<h3><?php echo $title; ?></h3>  
		<h3>Tanggal : <?php echo $mulai_tanggal." - ".$sampai_tanggal; ?></h3>
        <div class="card">
		<div class="card-body">
            <table class="table w-100 table-bordered table-hover" id="laporan_penjualan" class="display" border="1" style="border-collapse: collapse;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nama Produk</th> 
                  <th>Harga</th> 
                  <th>Qty</th> 
                  <th>Total</th> 
                  <th>Kategori Harga</th> 
                  <th>Pelanggan</th> 
                  <th>Kasir</th> 
				</tr>				
			  </thead>
			  <tbody>
				  <?php 
				  $i=1;
				  $totalQty = 0; $totalPenjualan = 0;
				  foreach($data as $row) { 
					  echo "<tr>";
					  echo "<td>".$i++."</td>";
					  echo "<td>".$row['tanggal']."</td>";
					  echo "<td>".$row['nama_produk']."</td>";
					  echo "<td>".rupiah($row['harga'])."</td>";
					  echo "<td>".rupiah($row['qty'])."</td>";
					  echo "<td>".rupiah($row['subtotal'])."</td>";
					  echo "<td>".$row['kategori_harga']."</td>";
					  echo "<td>".$row['pelanggan']."</td>";
					  echo "<td>".$row['kasir']."</td>";
					  echo "</tr>";

					  $totalQty += $row['qty'];
					  $totalPenjualan += $row['subtotal'];
				  } 
				  //totalan
					echo "<tr>";
					echo "<td colspan='4' align='center'><b>Total</b></td>";
					echo "<td><b>".rupiah($totalQty)."</b></td>";
					echo "<td><b>".rupiah($totalPenjualan)."</b></td>";
					echo "<td colspan='3'></td>";
				  	echo "</tr>";
				  ?>				  
			  </tbody>
			</table>
			<br/>
			<br/>
			<table class="table w-100 table-bordered table-hover" id="laporan_penjualan" class="display" border="1" style="border-collapse: collapse;">
				  <thead>
					  <th>No</th>
					  <th>Tanggal</th>
					  <th>Pelanggan</th>
					  <th>Kasir</th>
					  <th>Diskon</th>
					  <th>Keterangan</th>
				  </thead>
				  <tbody>
					  <?php 
					  $no=1;
					  if(count($data_diskon) > 0)
					  foreach($data_diskon as $val){
						echo "<tr>";
						echo "<td>".$no++."</td>";
						echo "<td>".$val['tanggal']."</td>";
						echo "<td>".$val['pelanggan']."</td>";
						echo "<td>".$val['kasir']."</td>";
						echo "<td>".rupiah($val['diskon'])."</td>";
						echo "<td>".$val['keterangan']."</td>";
						echo "</tr>";
					  }
					  echo "<tr>";
					  echo "<td colspan='4'><b>Total</b></td>";
					  echo "<td><b>".rupiah($total_diskon)."</b></td>";
					  echo "<td></td>";
					  echo "</tr>";
					  ?>
				  </tbody>
			</table>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
	<!-- /.content-wrapper -->
	
</div>
<!-- ./wrapper -->
</body>
</html>
