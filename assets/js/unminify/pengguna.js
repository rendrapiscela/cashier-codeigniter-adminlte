let url, pengguna = $("#pengguna").DataTable({
    responsive: true,
    scrollX: true,
    ajax: readUrl,
    columnDefs: [{
        searcable: false,
        orderable: false,
        targets: 0
    }],
    order: [
        [0, "asc"]
    ],
    columns: [{
        data: null
    }, {
        data: "outlet"
    },{
        data: "username"
    }, {
        data: "nama"
    }, {
        data: "action"
    }]
});

function reloadTable() {
    pengguna.ajax.reload()
}

function addData() {
    $.ajax({
        url: addUrl,
        type: "post",
        dataType: "json",
        data: $("#form").serialize(),
        success: () => {
            $(".modal").modal("hide");
            Swal.fire("Sukses", "Sukses Menambahkan Data", "success");
            reloadTable()
        },
        error: err => {
            console.log(err)
        }
    })
}

function remove(id) {
    Swal.fire({
        title: "Hapus",
        text: "Hapus data ini?",
        type: "warning",
		showCancelButton: true,
		closeOnCancel: true,
    }).then((result) => {
		if(result.value === true){
			$.ajax({
				url: deleteUrl,
				type: "post",
				dataType: "json",
				data: {
					id: id
				},
				success: () => {
					Swal.fire("Sukses", "Sukses Menghapus Data", "success");
					reloadTable()
				},
				error: err => {
					console.log(err)
				}
			})
		}
    })
}

function editData() {
    $.ajax({
        url: editUrl,
        type: "post",
        dataType: "json",
        data: $("#form").serialize(),
        success: () => {
            $(".modal").modal("hide");
            Swal.fire("Sukses", "Sukses Mengedit Data", "success"), reloadTable()
        },
        error: err => {
            console.log(err)
        }
    })
}

function add() {
    url = "add";
    $(".modal-title").html("Add Data");
    $('.modal button[type="submit"]').html("Add")
}

function edit(id) {
	document.getElementById("password").removeAttribute("required");
    $.ajax({
        url: getPenggunaUrl,
        type: "post",
        dataType: "json",
        data: {
            id: id
        },
        success: res => {
            $('[name="id"]').val(res.id);
			$('[name="outlet"]').append(`<option value='${res.outlet_id}'>${res.nama_outlet}</option>`);
			$('[name="username"]').val(res.username);
            $('[name="nama"]').val(res.nama_pengguna);
            $(".modal").modal("show");
            $(".modal-title").html("Edit Data");
            $('.modal button[type="submit"]').html("Edit");
            url = "edit"
        },
        error: err => {
            console.log(err)
        }
    })
}
pengguna.on("order.dt search.dt", () => {
    pengguna.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, err) => {
        el.innerHTML = err + 1
    })
});
$("#form").validate({
    errorElement: "span",
    errorPlacement: (err, el) => {
        err.addClass("invalid-feedback"), el.closest(".form-group").append(err)
    },
    submitHandler: () => {
        "edit" == url ? editData() : addData()
    }
});
$("#outlet").select2({
    placeholder: "Outlet",
    ajax: {
        url: getOutlet,
        type: "post",
        dataType: "json",
        data: params => ({
            kategori: params.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});

$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
