
var formatRupiah = new Intl.NumberFormat('id-ID', { style:'currency', currency : 'IDR', maximumSignificantDigits: 10 });	

function add_hold() {
    let data = transaksi.rows().data(),
		qty = [];
		kategori_harga = [];
		harga_input = [];
		persen_diskon = [];
		hargaAfterDiskon = [];
    $.each(data, (index, value) => {
		qty.push(value[4])
		kategori_harga.push(value[0])
		harga_input.push(value[3])
		persen_diskon.push(value[5])
		hargaAfterDiskon.push(value[6])
	});
	if(data.length < 1){
		Swal.fire("Hold Gagal", "Data Transaksi Kosong!", "warning");
		return;
	}
    $.ajax({
        url: holdUrl,
        type: "post",
        dataType: "json",
        data: {
            produk: JSON.stringify(produk),
            tanggal: $("#tanggal").val(),
			qty: qty,
			kategori_harga : kategori_harga,
			harga : harga_input,
			persendiskon : persen_diskon,
			harga_setelah_diskon : hargaAfterDiskon,
            keterangan: $("#keterangan").val(),
            nota: $("#nota").html()
        },
        success: res => {
                Swal.fire("Sukses", "Hold Transaksi", "success").
                    then(() => window.location.reload())
        },
        error: err => {
            console.log(err)
        }
    })
}

function edit_hold() {
    let data = transaksi.rows().data(),
		qty = [];
		kategori_harga = [];
		harga_input = [];
		persen_diskon = [];
		hargaAfterDiskon = [];
    $.each(data, (index, value) => {
		qty.push(value[4])
		kategori_harga.push(value[0])
		harga_input.push(value[3])
		persen_diskon.push(value[5])
		hargaAfterDiskon.push(value[6])
	});
	if(data.length < 1){
		Swal.fire("Hold Gagal", "Data Transaksi Kosong!", "warning");
		return;
	}
    $.ajax({
        url: EditHoldUrl,
        type: "post",
        dataType: "json",
        data: {
            produk: JSON.stringify(produk),
            tanggal: $("#tanggal").val(),
			qty: qty,
			kategori_harga : kategori_harga,
			harga : harga_input,
			persendiskon : persen_diskon,
			harga_setelah_diskon : hargaAfterDiskon,
			keterangan: $("#keterangan").val(),
            nota: $("#nota").html()
        },
        success: res => {
                Swal.fire("Sukses", "Hold Transaksi", "success").
                    then(() => window.location.reload())
        },
        error: err => {
            console.log(err)
        }
    })
}

function bayarCetak() {
    isCetak = true
}

function bayar() {
    isCetak = false
}
