var laporan_stok_masuk=$("#laporan_stok_masuk").DataTable( {
	processing: true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 'print'
	],
    responsive:true,
    scrollX:true,
    ajax:laporanUrl,
    columnDefs:[ {
        searcable: false,
        orderable: false,
        targets: 0
    }],
    order:[
        [1, "asc"]],
        columns:[ {
        data: "no"
    }
    , {
        data: "tanggal"
    }
    , {
        data: "barcode"
    }
    , {
        data: "nama_produk"
    }
    , {
        data: "jumlah"
    }
    , {
        data: "keterangan"
    }
    , {
        data: "supplier"
    }
    ]
}

);
function reloadTable() {
    laporan_stok_masuk.ajax.reload()
}

function remove(id) {
    Swal.fire( {
        title: "Hapus",
        text: "Hapus data ini?",
        type: "warning",
        showCancelButton: true
    }).then(()=> {
        $.ajax( {
            url:deleteUrl,
            type:"post",
            dataType:"json",
            data: {
                id: id
            },
            success:()=> {
                Swal.fire("Sukses", "Sukses Menghapus Data", "success");
                reloadTable()
            },
            error:err=> {
                console.log(err)
            }
        })
    })
}

laporan_stok_masuk.on("order.dt search.dt", ()=> {
    laporan_stok_masuk.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, err)=> {
        el.innerHTML=err+1
    })
});
$(".modal").on("hidden.bs.modal", ()=> {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
 //Date range picker
 $('#reservation').daterangepicker({
	rangeSplitter: 'to',
	datepickerOptions: {
		changeMonth: true,
		changeYear: true
	 },
	 ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
 });

 $('#reservation').on('apply.daterangepicker', function(ev, picker) {
	var startDate = picker.startDate.format('YYYY-MM-DD');
	var endDate = picker.endDate.format('YYYY-MM-DD');
	$.ajax({
		url:laporanUrlFilter,
		type: "post",
		dataType: "json",
		data: {
			start: startDate,
			end : endDate
		},
		success: (newdata)=> {
			laporan_stok_masuk.clear().draw();
			$.each(newdata.data, function(index, value) {
				laporan_stok_masuk.row.add(value).draw();
			});
			laporan_stok_masuk.draw();
		},
		error:err=> {
			console.log(err)
		}
	})
  });
