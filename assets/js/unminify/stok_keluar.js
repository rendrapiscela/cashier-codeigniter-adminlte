let stok_keluar = $("#stok_keluar").DataTable({
	processing: true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 
		{ extend : 'print',
		  exportOptions : {
			  columns : [1, 2, 3, 4, 5, 6]
		  }	
		}
	],
    responsive:true,
    scrollX:true,
    ajax: {
		url : readUrl,
		data : function (d) {
			d.tahun = $('#tahun').val(),
			d.bulan = $('#bulan').val(),
			d.outlet = $('#outlet').val(),
			d.status = $('#status').val()
		}
	},
    columnDefs: [{
        searcable: false,
        orderable: false,
        targets: 0
    }],
    order: [
        [1, "desc"]
    ],
    columns: [{
        data: 'no'
    }, {
        data: "tanggal"
    }, {
        data: "barcode"
    }, {
        data: "nama_produk"
    }, {
        data: "jumlah"
    }, {
        data: "keterangan"
	},{
		data: "outlet"
	},
	{
		data : "actions"
	}
	]
});

function reloadTable() {
    stok_keluar.ajax.reload()
}
function changeTahun(){
	reloadTable();
}
function changeBulan(){
	reloadTable();
}
function changeOutlet(){
	reloadTable();
}
function changeStatus(){
	reloadTable();
}

function addData() {
	$("#submitAdd").prop('disabled',true);
    $.ajax({
        url: addUrl,
        type: "post",
        dataType: "json",
        data: $("#form").serialize(),
        success: () => {
			$("#submitAdd").removeAttr('disabled');
            Swal.fire(
				{
					title : "Sukses Memperbaharui Data",
					timer : 1000,
					showConfirmButton : false,
					position: 'top-end',
					icon: 'success'
				});
            reloadTable()
        },
        error: err => {
            console.log(err)
        }
    })
}
function edit(id) {
	$("#alert").html('');
    $.ajax({
        url: editUrl,
        type: "post",
        dataType: "json",
        data: {
            id: id
        },
        success: res => {
            $('[name="id"]').val(res.id);
            $('.tgl').hide();
            $('.prd').hide();
            $('[name="jumlah"]').val(res.jumlah);
            $('[name="keterangan"]').val(res.keterangan);
			// $('[name="outlet"]').append(`<option value='${ (res.outlet_id == 0) ? '' : res.outlet_id }'>${res.nama_outlet}</option>`);
			$("#outlet").val((res.outlet_id == 0) ? '' : res.outlet_id);
            $(".modal").modal("show");
            $(".modal-title").html(""+res.nama_produk+'<br/> Tanggal : '+res.tanggal);
            $('.modal button[type="submit"]').html("Edit");
            $('#submitAdd').attr("value",'edit');
			url = "edit";
        },
        error: err => {
            console.log(err)
        }
    });
}
function remove(id) {
    Swal.fire({
        title: "Hapus",
        text: "Menghapus data ini akan mempengaruhi nilai stok?",
        type: "warning",
        showCancelButton: true
    }).then((result) => {
		if(result.value === true){
        $.ajax({
            url: deleteUrl,
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: () => {
                Swal.fire("Sukses", "Sukses Menghapus Data", "success");
                reloadTable();
            },
            error: (a) => {
                console.log(a);
            }
		})
		}
    })
}
function getStok(){
	var barcode = $('[name="barcode"]').val();
	$.ajax({
		url : getStokUrl,
		type: "post",
		dataType: "json",
		data: {
			id : barcode
		},
		success: (newdata) => {
			$("#notif_stok").html("Stok Gudang : <b> "+newdata.stok+"</b>");
			if(newdata.stok == 0){
				$("#submitAdd").hide();
			}else{
				$("#submitAdd").show();
			}
		},
		error:err=> {
			console.log(err)
		}
	})
}
stok_keluar.on("order.dt search.dt", () => {
    stok_keluar.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, val) => {
        el.innerHTML = val + 1
    })
});
$("#form").validate({
    errorElement: "span",
    errorPlacement: (err, el) => {
        err.addClass("invalid-feedback"), el.closest(".form-group").append(err)
    },
    submitHandler: () => {
        addData()
    }
});
$("#tanggal").datetimepicker({
    format: "dd-mm-yyyy h:ii:ss"
});
$("#barcode").select2({
    placeholder: "Barcode",
    ajax: {
        url: getBarcodeUrl,
        type: "post",
        dataType: "json",
        data: params => ({
            barcode: params.term
        }),
        processResults: res => ({
            results: res
        })
    }
});
// $("#outlet").select2({
//     placeholder: "Outlet",
//     ajax: {
//         url: getOutlet,
//         type: "post",
//         dataType: "json",
//         data: params => ({
//             barcode: params.term
//         }),
//         processResults: res => ({
//             results: res
//         })
// 	},
// 	cache : true
// });
$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
	$(".tgl").show();
	$(".prd").show();
	$(".modal-title").html("Add Data");
	$('#submitAdd').attr('value','add');
	$('.modal button[type="submit"]').html("Add");
});
$(".modal").on("show.bs.modal", () => {
    let a = moment().format("D-MM-Y H:mm:ss");
    $("#tanggal").val(a)
});

 //Date range picker
 $('#reservation').daterangepicker({
	rangeSplitter: 'to',
	datepickerOptions: {
		changeMonth: true,
		changeYear: true
	 },
	 ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
 });

 $('#reservation').on('apply.daterangepicker', function(ev, picker) {
	var startDate = picker.startDate.format('YYYY-MM-DD');
	var endDate = picker.endDate.format('YYYY-MM-DD');
	var outletId = $("#outlet").val();
	var status = $("#status").val();
	$("#loading").show();
	$.ajax({
		url:readUrl,
		type: "post",
		dataType: "json",
		data: {
			start: startDate,
			end : endDate,
			outlet : outletId,
			status : status
		},
		success: (newdata)=> {
			stok_keluar.clear().draw();
			$.each(newdata.data, function(index, value) {
				stok_keluar.row.add(value).draw();
			});
			stok_keluar.draw();
			$("#loading").hide('slow');
		},
		error:err=> {
			console.log(err)
		}
	})
  });