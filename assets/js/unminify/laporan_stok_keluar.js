var laporan_stok_keluar=$("#laporan_stok_keluar").DataTable( {
	processing: true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 'print'
	],
	responsive:true,
    scrollX:true,
    ajax:readUrl,
    columnDefs:[ {
        searcable: false,
        orderable: false,
        targets: 0
    }],
    order:[[1, "asc"]], columns:[ {
        data: "no"
    }
    , {
        data: "tanggal"
    }
    , {
        data: "barcode"
    }
    , {
        data: "nama_produk"
    }
    , {
        data: "jumlah"
    }
    , {
        data: "keterangan"
    }
    , {
        data: "outlet"
    }
    , {
        data: "nama_pengguna"
    }
    ]
});

$("#outlet").select2({
    placeholder: "Pilih",
    ajax: {
        url: outletUrl,
        type: "post",
        dataType: "json",
        data: paras => ({
            satuan: paras.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});

function reloadTable() {
    laporan_stok_keluar.ajax.reload()
}

function remove(id) {
    Swal.fire( {
        title: "Hapus",
        text: "Hapus data ini?",
        type: "warning",
        showCancelButton: true
    }).then(()=> {
        $.ajax( {
            url:deleteUrl, type:"post", dataType:"json", data: {
                id: id
            },
            success:()=> {
                Swal.fire("Sukses", "Sukses Menghapus Data", "success");
                reloadTable()
            },
            error:err=> {
                console.log(err)
            }
        }
        )
    }
    )
}

laporan_stok_keluar.on("order.dt search.dt", ()=> {
    laporan_stok_keluar.column(0, {
        search: "applied", order: "applied"
    }).nodes().each((el, err)=> {
        el.innerHTML=err+1
    })
});
$(".modal").on("hidden.bs.modal", ()=> {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
 //Date range picker
 $('#reservation').daterangepicker({
	rangeSplitter: 'to',
	datepickerOptions: {
		changeMonth: true,
		changeYear: true
	 },
	 ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
 });

 $('#reservation').on('apply.daterangepicker', function(ev, picker) {
	var startDate = picker.startDate.format('YYYY-MM-DD');
	var endDate = picker.endDate.format('YYYY-MM-DD');
    $("#loading").show();
	$.ajax({
		url:readUrlFilter,
		type: "post",
		dataType: "json",
		data: {
			start: startDate,
			end : endDate,
            outlet: $("#outlet").val(),
            status : $("#status").val()
		},
		success: (newdata)=> {
			laporan_stok_keluar.clear().draw();
			$.each(newdata.data, function(index, value) {
				laporan_stok_keluar.row.add(value).draw();
			});
			laporan_stok_keluar.draw();
            $("#loading").hide('slow');
		},
		error:err=> {
			console.log(err)
		}
	})
  });
