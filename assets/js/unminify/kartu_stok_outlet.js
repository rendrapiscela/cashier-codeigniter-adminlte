let tableData = $("#stok_outlet").DataTable({
	processing: true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 'print'
	],
    responsive:true,
    scrollX:true,
	ajax: {
		url : readUrl,
		data : function(d){
			d.kategori = $('[name="kategori"]').val(),
			d.outlet = $('[name="outlet"]').val(),
			d.status = $('#status').val()
		}
	},
	language: {
		loadingRecords: '&nbsp;',
		processing: "<img src='../assets/loading_dutajayaputra.gif'>"
	},      
    columnDefs: [{
        searcable: false,
        orderable: false,
		targets: 0
	}],
    order: [
        [7, "asc"]
    ],
    columns: [{
        data: "no"
	}, {
		data : "nama_outlet"
	},{
        data: "kategori"
    }, {
        data: "barcode"
    }, {
        data: "nama_produk"
    }, {
        data: "transfer_gudang"
	}, {
		data : "penjualan"
	},{
		data : "stok_sekarang"
	}]
});

function reloadTable() {
    tableData.ajax.reload()
}

$("#kategori").select2({
    placeholder: "Pilih",
    ajax: {
        url: kategoriUrl,
        type: "post",
        dataType: "json",
        data: paras => ({
            satuan: paras.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});
$("#outlet").select2({
    placeholder: "Pilih",
    ajax: {
        url: outletUrl,
        type: "post",
        dataType: "json",
        data: paras => ({
            satuan: paras.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});

tableData.on("order.dt search.dt", () => {
    tableData.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, val) => {
        el.innerHTML = val + 1
    })
});

$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
$(".modal").on("show.bs.modal", () => {
    let a = moment().format("D-MM-Y H:mm:ss");
    $("#tanggal").val(a)
});

function changeKategori(){
	reloadTable();
}
function changeOutlet(){
	reloadTable();
}
function changeStatus(){
	reloadTable();
}
