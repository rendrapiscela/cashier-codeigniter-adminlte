var isCetak = false;
function bayarCetak(){
	isCetak = true;
}
function bayar(){
	isCetak = false;
}
var laporan_penjualan = $("#laporan_penjualan").DataTable( {
	processing: true,
	// dom: 'Bfrtip',
	// buttons: [
	// 	'csv', 'excel', 'pdf', 'print'
	// ],
    responsive:true,
    scrollX:true,
	ajax: filterDate,
    columnDefs:[{
        searcable: false,
        orderable: false,
        targets: 0
	}],
    order:[
        [1, "asc"]],
        columns:[ {
            data: "no"
        }
        , {
            data: "tanggal"
        }
        , {
            data: "nama_produk"
        }
        , {
            data: "qty"
        }
        , {
            data: "total_bayar"
        }
        , {
            data: "jumlah_uang"
        }
        , {
            data: "diskon"
        }
        , {
            data: "pelanggan"
        },{
			data: "kasir"
		}
        , {
            data: "action"
        }
        ]
}

);
function reloadTable() {
    laporan_penjualan.ajax.reload()
}

function remove(id) {
    Swal.fire( {
        title: "Hapus",
        text: "Hapus data ini?",
        type: "warning",
        showCancelButton: true
    }).then(()=> {
        $.ajax( {
            url:deleteUrl,
            type:"post",
            dataType:"json",
            data: {
                id: id
            },
            success:()=> {
                Swal.fire("Sukses", "Sukses Menghapus Data", "success");
                reloadTable()
            },
            error:err=> {
                console.log(err)
            }
        })
    })
}

laporan_penjualan.on("order.dt search.dt", ()=> {
    laporan_penjualan.column(0, {
        search: "applied", order: "applied"
    }).nodes().each((el, err)=> {
        el.innerHTML=err+1
    })
});

 //Date range picker
 $('#reservation').daterangepicker({
	rangeSplitter: 'to',
	datepickerOptions: {
		changeMonth: true,
		changeYear: true
	 },
	 ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
 });

 $('#reservation').on('apply.daterangepicker', function(ev, picker) {
	var startDate = picker.startDate.format('YYYY-MM-DD');
	var endDate = picker.endDate.format('YYYY-MM-DD');
	var outletId = $("#outlet").val();
	var statusBayar = $("#status_bayar").val();
	$("#loading").show();
	$.ajax({
		url:filterDate,
		type: "post",
		dataType: "json",
		data: {
			start: startDate,
			end : endDate,
			outlet : outletId,
			statusbayar : statusBayar
		},
		success: (newdata)=> {
			laporan_penjualan.clear().draw();
			$.each(newdata.data, function(index, value) {
				laporan_penjualan.row.add(value).draw();
			});
			laporan_penjualan.draw();
			$("#loading").hide('slow');
		},
		error:err=> {
			console.log(err)
		}
	})
  });
  function report_excel(){
	var tanggal = $("#reservation").val();
	var outletId = $("#outlet").val();
	var statusBayar = $("#status_bayar").val();
	$("#report_excel").prop('disabled',true);
	$.ajax({
		url: exportExcel,
		type: "post",
		dataType: "json",
		data: {
			tanggal : tanggal,
			outlet : outletId,
			statusbayar : statusBayar
		},
		success: (res) => {
			$("#report_excel").removeAttr("disabled");
			if(res.data === 0){
                Swal.fire("Gagal", "Tidak ada data!", "warning");
			}else{
				window.open(downloadExcel+'?state=excel&tanggal='+tanggal+'&outlet='+outletId+'&statusbayar='+statusBayar,'_blank' );
			}
		},
		error:err=> {
			console.log(err)
		}
	});
  }
  function report_print(){
	var tanggal = $("#reservation").val();
	var outletId = $("#outlet").val();
	var statusBayar = $("#status_bayar").val();
	$("#report_print").prop('disabled',true);
	$.ajax({
		url: exportExcel,
		type: "post",
		dataType: "json",
		data: {
			tanggal : tanggal,
			outlet : outletId,
			statusbayar : statusBayar
		},
		success: (res) => {
			$("#report_print").removeAttr('disabled');
			if(res.data === 0){
                Swal.fire("Gagal", "Tidak ada data!", "warning");
			}else{
				window.open(downloadExcel+'?state=print&tanggal='+tanggal+'&outlet='+outletId+'&statusbayar='+statusBayar,'_blank' );
			}
		},
		error:err=> {
			console.log(err)
		}
	});
  }
  
  function report_print_rekap(){
	var tanggal = $("#reservation").val();
	var outletId = $("#outlet").val();
	var statusBayar = $("#status_bayar").val();
	$("#print_rekap").prop('disabled',true);
	$.ajax({
		url: exportExcel,
		type: "post",
		dataType: "json",
		data: {
			tanggal : tanggal,
			outlet : outletId,
			statusbayar : statusBayar
		},
		success: (res) => {
			$("#print_rekap").removeAttr('disabled');
			if(res.data === 0){
                Swal.fire("Gagal", "Tidak ada data!", "warning");
			}else{
				window.open(printRekap+'?tanggal='+tanggal+'&outlet='+outletId+'&statusbayar='+statusBayar,'_blank' );
			}
		},
		error:err=> {
			console.log(err)
		}
	});
  }
 
  function report_print_kategori(){
	var tanggal = $("#reservation").val();
	var outletId = $("#outlet").val();
	var statusBayar = $("#status_bayar").val();
	$("#print_kategori").prop('disabled',true);
	$.ajax({
		url: exportExcel,
		type: "post",
		dataType: "json",
		data: {
			tanggal : tanggal,
			outlet : outletId,
			statusbayar : statusBayar
		},
		success: (res) => {
			$("#print_kategori").removeAttr('disabled');
			if(res.data === 0){
                Swal.fire("Gagal", "Tidak ada data!", "warning");
			}else{
				window.open(printKategori+'?tanggal='+tanggal+'&outlet='+outletId+'&statusbayar='+statusBayar,'_blank' );
			}
		},
		error:err=> {
			console.log(err)
		}
	});
  }

  $("#outlet").select2({
    placeholder: "Pilih",
    ajax: {
        url: outletUrl,
        type: "post",
        dataType: "json",
        data: paras => ({
            satuan: paras.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});
//modal function utang
$(".modal").on("show.bs.modal", () => {
	let now = moment().format("DD-MM-Y H:mm:ss");
	$("#tanggal").val(now);
	$("#tanggal").datetimepicker({
		format: "dd-mm-yyyy hh:ii:ss"
	});
});
$("#form-cicilan").validate({
    errorElement: "span",
    errorPlacement: (err, el) => {
        err.addClass("invalid-feedback"), el.closest(".form-group").append(err)
    },
    submitHandler: () => {
        add()
    }
});

function formCicilan(idTransaksi){
	$("#jumlah_uang").val('');
	$("#keterangan").val('');
	$.ajax({
        url: getSisaCicilan+'/'+idTransaksi,
        type: "post",
		dataType: "json",
        success: res => {
			$(".sisa_pembayaran").html(res.rp_sisa_bayar);
			$("#transaksi_id").val(idTransaksi);
			$("#sisa_bayar").val(res.sisa_bayar);
			$("#cicilan_ke").val(res.cicilan_ke);

			$("#formCicilan").modal('show');
        },
        error: err => {
            console.log(err)
        }
	}) //end ajax
	
}

function historyCicilan(idTransaksi){
	$("#bayarCicilan").click(function(){
		$("#detailCicilan").modal('hide');
		formCicilan(idTransaksi);
	})
	$.ajax({
        url: viewDetailCicilan+'/'+idTransaksi,
        type: "post",
        dataType: "json",
        success: res => {
			$("#history").html(res.history);
			$("#total_hutang").html(res.total_hutang);
			$("#detailCicilan").modal('show');
        },
        error: err => {
            console.log(err)
        }
    })
}

function uangbayar(){
	let jumlah_uang = $('[name="jumlah_uang"').val();
	let	sisa_bayar = parseInt($("#sisa_bayar").val());

	if(jumlah_uang > sisa_bayar){
		$("#notif").html('<div class="alert alert-danger">Jumlah yang dimasukkan melebihi sisa cicilan.</div>');
        $("#add").attr("disabled", "disabled");
        $("#cetak").attr("disabled", "disabled")
    } else {
		$("#notif").html('');
		$("#add").removeAttr("disabled");
        $("#cetak").removeAttr("disabled")
	}	

}

function add() {
    $.ajax({
        url: addCicilan,
        type: "post",
        dataType: "json",
		data : {
			tanggal : $('#tanggal').val(),
			transaksi_id : $('#transaksi_id').val(),
			sisa_bayar : $("#sisa_bayar").val(),
			jumlah_uang : $('#jumlah_uang').val(),
			keterangan : $('#keterangan').val(),
			cicilan_ke : $('#cicilan_ke').val(),
		},
        success: res => {
			if(res){
				if (isCetak) {
					var linkCetak = linkPrint+"/"+$('#transaksi_id').val();
					Swal.fire("Sukses", "Sukses Membayar Cicilan", "success").
					    then(() => window.location.reload(), window.open(linkCetak, '_blank')) //window.location.href = `${cetakUrl}${res}` )
				} else {
					Swal.fire("Sukses", "Sukses Membayar Cicilan", "success").
					    then(() => window.location.reload())
				}
			}
        },
        error: err => {
            console.log(err)
        }
    })
}
