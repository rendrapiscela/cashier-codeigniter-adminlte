
var formatRupiah = new Intl.NumberFormat('id-ID', { style:'currency', currency : 'IDR', maximumSignificantDigits: 10 });	
var dataSet = JSON.parse(dataEdit);
var idproduk = JSON.parse(idProduk);
var jdiskon = JSON.parse(jenis_diskon);
var hargaawal = JSON.parse(harga_awal);
let produk = [];
dataSet.forEach( (index, val) => {
    let idx = idproduk[val]+"_"+index[4].replace(/\./g,'_');
    index.push(`<button name="${ idx }" class="btn btn-sm btn-danger" onclick="remove('${ idx }')">Hapus</button>`);
    produk.push({
        id: idproduk[val]+'_'+index[4].replace(/\./g,'_'),
        stok: 0,
		terjual: index[4],
		jenis_diskon : jdiskon[val],
		harga_awal : parseInt(hargaawal[val].harga.replace(/\./g,''))
    });
})
let isCetak = false,
	isRupiah = true,
    transaksi = $("#transaksi").DataTable({
        responsive: true,
        lengthChange: false,
        searching: false,
		scrollX: true,
		ordering : false,
		data : dataSet,        
        columns:[ 
        {
            title: "Kategori"
        },
        {
            title: "Barcode"
		},
        {
            title: "Nama Produk"
		},
		{
            title: "Harga"
        }
        , {
            title: "Jumlah"
		},
		{
			title : "Diskon (%/Rp)"
		},
		{
			title : "Subtotal"
		},
        {
            title: "action"
        }
        ]
	});

function reloadTable() {
    transaksi.ajax.reload()
}

function nota(jumlah) {
    let hasil = "",
        char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        total = char.length;
    for (var r = 0; r < 5; r++) hasil += char.charAt(Math.floor(Math.random() * total));
    return hasil
}

function checkPotongan(){
	var checkBox = document.getElementById("myCheck");
	if (checkBox.checked == false){
		isRupiah = true;
		$("#jenisDiskon").text('Rupiah (Rp)');
		$("#persen_diskon").removeAttr('max');
	  } else {
		isRupiah = false;  
		$("#jenisDiskon").text('Persentase (%)');
		$("#persen_diskon").attr('max','100');
	}
	checkEmpty()
	countDiskon()
}

function getNama() {
	$("#processing").show();
    $.ajax({
        url: produkGetNamaUrl,
        type: "post",
        dataType: "json",
        data: {
			id: $("#barcode").val(),
			kategori_harga : $("#kategori_harga").val()
        },
        success: res => {
			if(res.status != 'pilih_barang'){
				$("#nama_produk").html(res.nama_produk);
				$("#harga_produk").html('<b>Harga : '+res.harga+'</b>');
				$("#sisa").html(`Sisa Stok : ${res.stok}`);
				$("#params").val(res.params)
				if(res.params != 'tidak'){
					$("#params_keterangan").html(`<i class="fa fa-check"></i> Hitungan khusus ${res.params}`);
				}else{
					$("#params_keterangan").html('');
				}
				//set harga produk diskon
				$("#harga_produk_awal").val(parseInt(res.harga.replace(/\./g,'')));
				checkEmpty()
				countDiskon()
				$("#processing").hide();
			}else{
				$("#harga_produk").html('<b>Silahkan Pilih Barang!</b>');
				$("#processing").hide();
			}
        },
        error: err => {
            console.log(err)
        }
    })
}

function getHargaKategori() {
	$("#processing").show();
    $.ajax({
        url: produkGetNamaUrl,
        type: "post",
        dataType: "json",
        data: {
			id: $("#barcode").val(),
			kategori_harga : $("#kategori_harga").val()
        },
        success: res => {
			if(res.status != 'pilih_barang'){
				$("#nama_produk").html(res.nama_produk);
				$("#harga_produk").html('<b>Harga : '+res.harga+'</b>');
				$("#sisa").html(`Sisa ${res.stok}`);
				$("#params").val(res.params)
				if(res.params != 'tidak'){
					$("#params_keterangan").html(`<i class="fa fa-check"></i> Hitungan khusus ${res.params}`);
				}else{
					$("#params_keterangan").html('');
				}
				//set harga produk diskon
				$("#harga_produk_awal").val(parseInt(res.harga.replace(/\./g,'')));
				checkEmpty()
				countDiskon()
				$("#processing").hide();
			}else{
				$("#harga_produk").html('<b>Silahkan Pilih Barang!</b>');
				$("#processing").hide();
			}
        },
        error: err => {
            console.log(err)
        }
    })
}

function getKodeBarcode(kodeBarcode) {
    $.ajax({
        url: produkGetNamaUrl,
        type: "post",
        dataType: "json",
        data: {
            id: kodeBarcode
        },
        success: res => {
            $("#nama_produk").html(res.nama_produk);
            $("#sisa").html(`Sisa ${res.stok}`);
            checkEmpty()
        },
        error: err => {
            console.log(err)
        }
    })
}

function checkStok() {
	if($("#barcode").val() == null){ 
		Swal.fire("Periksa Kembali", "Anda belum memilih produk!", "warning");   
		return;
	}
	if(parseFloat($("#jumlah").val()) < 0.1){ 
		Swal.fire("Periksa Kembali", "Jumlah minimal 0.1", "warning");   
		return;
	}
    $.ajax({
        url: produkGetStokUrl,
        type: "post",
        dataType: "json",
        data: {
			id: $("#barcode").val(),
			kategori_harga : $("#kategori_harga").val()
        },
        success: res => {

			let kategori = $("#kategori_harga").val();
			if(kategori == 'harga'){
				kategori = 'Harga Umum';
			}else if(kategori == 'harga_khusus'){
				kategori = 'Harga Grosir/Khusus';
			}else if(kategori == 'harga_reseller'){
				kategori = 'Harga Reseller';
			}

            let barcode = $("#barcode").val(),
                nama_produk = res.nama_produk,
                jumlah = parseFloat($("#jumlah").val()),
				idx_jumlah = $("#jumlah").val().replace(/\./g, '_'),
                stok = parseInt(res.stok),
				harga = parseInt(res.harga),
				kategori_harga = kategori,
				dataBarcode = res.barcode,
				persen_diskon = $('#persen_diskon').val(),				
				harga_diskon_produk = hitung_produk_diskon(harga, persen_diskon),
				hargaSetelahDiskon = hitung_diskon(harga, persen_diskon, jumlah),
                total = parseInt($("#total").html());
            if (stok < jumlah) Swal.fire("Gagal", "Stok Tidak Cukup", "warning");
            else {
				let a = transaksi.rows().indexes().filter(function(a, t) {
					dataBarcode === transaksi.row(a).data()[0];
				}); 
                if (a.length > 0) {
                    let row = transaksi.row(a[1]),
						data = row.data();
                    if (stok < data[4] + jumlah) {
                        Swal.fire('stok', "Stok Tidak Cukup", "warning")
                    } else {
                        row.data(data).draw();
                        indexProduk = produk.findIndex(a => a.id == barcode+'_'+idx_jumlah);
						produk[indexProduk].stok = stok - data[4];
						$("#total").html(total + hargaSetelahDiskon)
						$("#txtTotal").html(formatRupiah.format(total + hargaSetelahDiskon))
					}
                } else {
                    produk.push({
                        id: barcode+'_'+idx_jumlah,
                        stok: stok - jumlah,
						terjual: jumlah,
						jenis_diskon : isRupiah,
						harga_awal : harga
                    });
                    transaksi.row.add([
						kategori_harga,
                        dataBarcode,
                        nama_produk,
                        formatRupiah.format(harga_diskon_produk),
						jumlah,
						(isRupiah) ? persen_diskon : persen_diskon+'%',
						formatRupiah.format(hargaSetelahDiskon),
                        `<button name="${barcode+'_'+jumlah}" class="btn btn-sm btn-danger" onclick="remove('${barcode+'_'+idx_jumlah}')">Hapus</btn>`]).draw();
						$("#total").html(total + hargaSetelahDiskon);
						$("#txtTotal").html(formatRupiah.format(total + hargaSetelahDiskon));
						$("#jumlah").val(1);
						$("#persen_diskon").val(0);
						$("#tambah").attr("disabled", "disabled");
						$("#bayar").removeAttr("disabled")
						$("#barcode").click();
						hitung_subtotal_diskon();
                } 
            }
        }
    })
}

function bayarCetak() {
    isCetak = true
}

function bayar() {
    isCetak = false
}

function checkEmpty() {
	countDiskon();
    let barcode = $("#barcode").val(),
        jumlah = $("#jumlah").val();
    if (barcode !== "" && jumlah !== "" && parseFloat(jumlah) >= 1) {
        $("#tambah").removeAttr("disabled")    
    } else {
        $("#tambah").attr("disabled", "disabled")
    }
}

function checkUang() {
    let jumlah_uang = $('[name="jumlah_uang"').val(),
		total_bayar = parseInt($("#total").html());
	
		console.log(formatRupiah.format(total_bayar));
    if (jumlah_uang !== "") {
        $("#add").removeAttr("disabled");
        $("#cetak").removeAttr("disabled")
    } else {
        $("#add").attr("disabled", "disabled");
        $("#cetak").attr("disabled", "disabled")
	}
	if(jumlah_uang >= total_bayar){
		$("#textkembalian").text('Kembalian : ');
	}
	if(jumlah_uang < total_bayar){
		$("#textkembalian").html('<i style="color:red;">Sisa Pembayaran / Hutang</i> : ');
	}
}

function remove(nama) {
	let idx = produk.findIndex(function(item) {
		return item.id === nama;
	});
	if(idx !== -1) produk.splice(idx,1);

	let data = transaksi.row($("[name=" + nama + "]").closest("tr")).data(),
		kategori_harga = data[0],
		dataBarcode = data[1],
        stok = data[4],
        harga = data[3],
		total = parseInt($("#total").html());
		let	txtsubtotal = data[6].replace("Rp","");
		let	subtotal = parseInt(txtsubtotal.replace(/\./g,''))	
		akhir = total - subtotal;
    $("#total").html(akhir);
    $("#txtTotal").html(formatRupiah.format(akhir));
    transaksi.row($("[name=" + nama + "]").closest("tr")).remove().draw();
    $("#tambah").attr("disabled", "disabled");
    if (akhir < 1) {
        $("#bayar").attr("disabled", "disabled")
    }
}

function hitung_subtotal_diskon(){
	let data = transaksi.rows().data();
	var	totalHargaSetelahDiskon = [];
	$.each(data, (index, value) => {
		let	txtsubtotal = value[3].replace("Rp","");
		let	subtotal = parseInt(txtsubtotal.replace(/\./g,''))
		let result = hitung_diskon(subtotal, parseFloat(value[5]), parseFloat(value[4]));
			if(produk[index].jenis_diskon == true){
				totalHargaSetelahDiskon.push(parseInt(value[5]));
			}else{
				let diskon = value[5].replace('%','');
				if(diskon > 0){
					totalHargaSetelahDiskon.push((parseFloat(diskon)/100) * (produk[index].harga_awal * parseInt(value[4])))
				}
			}		
	});
	return totalHargaSetelahDiskon.reduce(sum);
}
function sum(total, num) {
	return total + Math.round(num);
}

function hitung_produk_diskon(hargaAwal, persenDiskon){
	var hargaSetelahDiskon = 0;
	if(parseFloat(persenDiskon) > 0){
		if(isRupiah == true){
			hargaSetelahDiskon = hargaAwal;
			hargaSetelahDiskon = Math.ceil(hargaSetelahDiskon)
		}else{
			hargaSetelahDiskon = (hargaAwal) - ((parseFloat(persenDiskon)/100) * (hargaAwal));
			hargaSetelahDiskon = Math.ceil(hargaSetelahDiskon)
		}

	}else{
		hargaSetelahDiskon = hargaAwal;
	}
	return hargaSetelahDiskon;
}

function hitung_diskon(hargaAwal, persenDiskon, jumlah){
	var hargaSetelahDiskon = 0;
	if(parseFloat(persenDiskon) > 0){
		if(isRupiah == true){
			hargaSetelahDiskon = (hargaAwal*jumlah) - persenDiskon;
		}else{
		hargaSetelahDiskon = (hargaAwal*jumlah) - ((parseFloat(persenDiskon)/100) * (hargaAwal*jumlah));
		hargaSetelahDiskon = Math.ceil(hargaSetelahDiskon)
		}
	}else{
		hargaSetelahDiskon = hargaAwal*jumlah;
	}
	return hargaSetelahDiskon;
}

function countDiskon(){
	var persenDiskon = $('#persen_diskon').val();
	var hargaAwal = $('#harga_produk_awal').val();
	var jumlah = $('#jumlah').val();
	var hargaSetelahDiskon = hitung_diskon(hargaAwal, persenDiskon, jumlah);
	if(persenDiskon > 0){
		$("#view_harga_setelah_diskon").show('slow');
		$("#harga_diskon").html(formatRupiah.format(hargaSetelahDiskon));
		$("#harga_produk_setelah_diskon").val(hargaSetelahDiskon);
	}else{
		$("#view_harga_setelah_diskon").hide('slow');
		$("#harga_diskon").html(hargaSetelahDiskon);
		$("#harga_produk_setelah_diskon").val(hargaSetelahDiskon);
	}
}

function add() {
	$("#add").prop('disabled',true);
	$("#cetak").prop('disabled',true);
	let data = transaksi.rows().data(),
		qty = [];
		kategori_harga = [];
		harga_input = [];
		persen_diskon = [];
		hargaAfterDiskon = [];
    $.each(data, (index, value) => {
		qty.push(value[4])
		kategori_harga.push(value[0])
		harga_input.push(value[3])
		persen_diskon.push(value[5])
		hargaAfterDiskon.push(value[6])
    });
    $.ajax({
        url: addUrl,
        type: "post",
        dataType: "json",
        data: {
            produk: JSON.stringify(produk),
            tanggal: $("#tanggal").val(),
			qty: qty,
			kategori_harga : kategori_harga,
			harga : harga_input,
			persendiskon : persen_diskon,
			harga_setelah_diskon : hargaAfterDiskon,
			subtotal_diskon : $("#subtotal_diskon").val(),
            total_bayar: $("#total").html(),
            jumlah_uang: $('[name="jumlah_uang"]').val(),
            diskon: $('[name="diskon"]').val(),
            pelanggan: $("#pelanggan").val(),
            keterangan: $("#keterangan").val(),
            nota: $("#nota").html()
        },
        success: res => {
            if (isCetak) {
				var linkCetak = "../../transaksi/cetak/"+res;
                Swal.fire("Sukses", "Transaksi Berhasil Direvisi", "success").
                    then(() => window.location.href = '../../laporan_penjualan', window.open(linkCetak, '_blank')) //window.location.href = `${cetakUrl}${res}` )
            } else {
                Swal.fire("Sukses", "Transaksi Berhasil Direvisi", "success").
                    then(() => window.location.href = '../../laporan_penjualan')
            }
        },
        error: err => {
            console.log(err)
        }
    })
}

function kembalian() {
    let total = $("#total").html(),
        jumlah_uang = $('[name="jumlah_uang"').val(),
        diskon = $('[name="diskon"]').val();
	$(".kembalian").html(jumlah_uang - total - diskon);
	if(jumlah_uang < total){
		var t = total - diskon;
		$("#txtTotalBayar").html(formatRupiah.format(parseInt(t)));
		let kembali = t - jumlah_uang;
		$("#txtKembalian").html(formatRupiah.format(parseInt(kembali)));
	}
	if(jumlah_uang >= total){
		var d = total - diskon;
		$("#txtTotalBayar").html(formatRupiah.format(parseInt(d)));
		let kembali = jumlah_uang - d;
		$("#txtKembalian").html(formatRupiah.format(parseInt(kembali)));
	}
    checkUang()
}
$("#barcode").select2({
	// minimumInputLength : 3,
    placeholder: "Barcode",
    ajax: {
        url: getBarcodeUrl,
        type: "post",
        dataType: "json",
        data: params => ({
			barcode: params.term,
			kategori : $("#kategori_harga").val()
        }),
        processResults: res => ({
            results: res
		}),
        cache: true
    }
});

$("#tanggal").datetimepicker({
    format: "dd-mm-yyyy h:ii:ss"
});
$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
$(".modal").on("show.bs.modal", () => {
    let now = moment().format("DD-MM-Y H:mm:ss"),
        total = $("#total").html(),
        jumlah_uang = $('[name="jumlah_uang"').val();
		$("#tanggal").val(now), $("#txtTotalBayar").html(formatRupiah.format(total)), $(".total_bayar").html(formatRupiah.format(total)), $("#txtKembalian").html(formatRupiah.format(Math.max(jumlah_uang - total, 0)))
		var subtotalDiskon = hitung_subtotal_diskon();
		$("#view_subtotal_diskon").html(formatRupiah.format(subtotalDiskon));
		$("#subtotal_diskon").val(subtotalDiskon);
});
$("#form").validate({
    errorElement: "span",
    errorPlacement: (err, el) => {
        err.addClass("invalid-feedback"), el.closest(".form-group").append(err)
    },
    submitHandler: () => {
		$("#submitAdd").prop('disabled',true);
		add()
    }
});
$("#nota").html(nota(15));

function addPelanggan() {
	url = "add";
	$("#modal-pelanggan").modal();
    // $(".modal-title").html("Add Data");
    // $('.modal button[type="submit"]').html("Add")
}

function insertPelanggan() {
    $.ajax({
        url: addUrlPelanggan,
        type: "post",
        dataType: "json",
        data: $("#form-pelanggan").serialize(),
        success: () => {
			Swal.fire("Sukses", "Sukses Menambahkan Data", "success");
            $("#modal-pelanggan").modal("hide");
        },
        error: err => {
            console.log(err)
        }
    })
}

function add_hold() {
	let data = transaksi.rows().data(),
		qty = [];
		kategori_harga = [];
		harga_input = [];
		persen_diskon = [];
		hargaAfterDiskon = [];
    $.each(data, (index, value) => {
		qty.push(value[4])
		kategori_harga.push(value[0])
		harga_input.push(value[3])
		persen_diskon.push(value[5])
		hargaAfterDiskon.push(value[6])
	});
	if(data.length < 1){
		Swal.fire("Hold Gagal", "Data Transaksi Kosong!", "warning");
		return;
	}
    $.ajax({
        url: holdUrl,
        type: "post",
        dataType: "json",
        data: {
            produk: JSON.stringify(produk),
            tanggal: $("#tanggal").val(),
			qty: qty,
			kategori_harga : kategori_harga,
			harga : harga_input,
			persendiskon : persen_diskon,
			harga_setelah_diskon : hargaAfterDiskon,
            keterangan: $("#keterangan").val(),
            nota: $("#nota").html()
        },
        success: res => {
                Swal.fire("Sukses", "Hold Transaksi", "success").
                    then(() => window.location.reload())
        },
        error: err => {
            console.log(err)
        }
    })
}

function edit_hold() {
    let data = transaksi.rows().data(),
		qty = [];
		kategori_harga = [];
		harga_input = [];
		persen_diskon = [];
		hargaAfterDiskon = [];
    $.each(data, (index, value) => {
		qty.push(value[4])
		kategori_harga.push(value[0])
		harga_input.push(value[3])
		persen_diskon.push(value[5])
		hargaAfterDiskon.push(value[6])
	});
	if(data.length < 1){
		Swal.fire("Revisi Gagal", "Data Transaksi Kosong!", "warning");
		return;
	}
    $.ajax({
        url: EditHoldUrl,
        type: "post",
        dataType: "json",
        data: {
            produk: JSON.stringify(produk),
            tanggal: $("#tanggal").val(),
			qty: qty,
			kategori_harga : kategori_harga,
			harga : harga_input,
			persendiskon : persen_diskon,
			harga_setelah_diskon : hargaAfterDiskon,
            keterangan: $("#keterangan").val(),
            nota: $("#nota").html()
        },
        success: res => {
                Swal.fire("Sukses", "Revisi Transaksi & Stok Barang", "success").
                    then(() => window.location.reload())
        },
        error: err => {
            console.log(err)
        }
    })
}
