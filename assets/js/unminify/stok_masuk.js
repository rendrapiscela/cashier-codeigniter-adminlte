let stok_masuk = $("#stok_masuk").DataTable({
	processing : true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 'print'
	],
    responsive: true,
    scrollX: true,
    ajax: {
		url : readUrl,
		data : function(r){
			r.bulan = $('#bulan').val(),
			r.tahun = $('#tahun').val()
		}
	},
    columnDefs: [{
        searcable: false,
        orderable: false,
        targets: 0
    }],
    order: [
        [1, "desc"]
    ],
    columns: [{
        data: null
    }, {
        data: "tanggal"
    }, {
        data: "barcode"
    }, {
        data: "nama_produk"
    }, {
        data: "jumlah"
    }, {
        data: "keterangan"
    }, {
        data: "supplier"
    }, {
        data: "oleh"
    },{
		data : "actions"
	}]
});

function reloadTable() {
    stok_masuk.ajax.reload()
}

function changeBulan(){
	reloadTable();
}
function changeTahun(){
	reloadTable();
}

function checkKeterangan(obj) {
    if (obj.value == "lain") {
        $(".supplier").hide();
        $("#supplier").attr("disabled", "disabled");
        $(".lain").removeClass("d-none") 
    } else {
        $(".lain").addClass("d-none");
        $("#supplier").removeAttr("disabled");
        $(".supplier").show()
    }
}

function addData() {
	$("#sbt").prop('disabled',true);
    $.ajax({
        url: addUrl,
        type: "post",
        dataType: "json",
        data: $("#form").serialize(),
        success: () => {
            Swal.fire(
				{
					title : "Sukses Memperbaharui Data",
					timer : 1000,
					showConfirmButton : false,
					position: 'top-end',
					icon: 'success'
				});
			$("#sbt").removeAttr('disabled');
            reloadTable()
        },
        error: err => {
            console.log(err)
        }
    })
}
function edit(id) {
	$("#alert").html('');
    $.ajax({
        url: editUrl,
        type: "post",
        dataType: "json",
        data: {
            id: id
        },
        success: res => {
            $('[name="id"]').val(res.id);
            $('.tgl').hide();
            $('.prd').hide();
            $('[name="jumlah"]').val(res.jumlah);
            $('[name="keterangan"]').val(res.keterangan);
			$('[name="supplier"]').val(`<option value='${ (res.id_supplier == 0) ? '' : res.id_supplier }'>${res.nama_supplier}</option>`);
            $(".modal").modal("show");
            $(".modal-title").html(""+res.nama_produk+'<br/> Tanggal : '+res.tanggal);
            $('.modal button[type="submit"]').html("Edit");
            $('#sbt').attr("value",'edit');
			url = "edit";
			$("#form").attr('action',updateUrl);
        },
        error: err => {
            console.log(err)
        }
    });
}
function remove(id) {
    Swal.fire({
        title: "Hapus",
        text: "Menghapus data ini akan mempengaruhi nilai stok?",
        type: "warning",
        showCancelButton: true
    }).then((result) => {
		if(result.value === true){
        $.ajax({
            url: deleteUrl,
            type: "post",
            dataType: "json",
            data: {
                id: id
            },
            success: () => {
                Swal.fire("Sukses", "Sukses Menghapus Data", "success");
                reloadTable();
            },
            error: (a) => {
                console.log(a);
            }
		})
		}
    })
}
stok_masuk.on("order.dt search.dt", () => {
    stok_masuk.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, val) => {
        el.innerHTML = val + 1
    })
});
$("#form").validate({
    errorElement: "span",
    errorPlacement: (err, el) => {
        err.addClass("invalid-feedback"), el.closest(".form-group").append(err)
    },
    submitHandler: () => {
        addData()
    }
});
$("#tanggal").datetimepicker({
    format: "dd-mm-yyyy h:ii:ss"
});
$("#barcode").select2({
    placeholder: "Barcode",
    ajax: {
        url: getBarcodeUrl,
        type: "post",
        dataType: "json",
        data: params => ({
            barcode: params.term
        }),
        processResults: res => ({
            results: res
        }),
        cache: false
    }
});
$("#supplier").select2({
    placeholder: "Supplier",
    ajax: {
        url: supplierSearchUrl,
        type: "post",
        dataType: "json",
        data: params => ({
            supplier: params.term
        }),
        processResults: res => ({
            results: res
        }),
        cache: true
    }
});

$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
	$("#form").validate().resetForm()
	$(".tgl").show();
	$(".prd").show();
	$(".modal-title").html("Add Data");
	$('#sbt').attr('value','add');
	$('.modal button[type="submit"]').html("Add");
	$("#supplier").val('');
})
$(".modal").on("show.bs.modal", () => {
    let a = moment().format("D-MM-Y H:mm:ss");
	$("#tanggal").val(a)
});
