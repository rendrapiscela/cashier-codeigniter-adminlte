let tableData = $("#stok_gudang").DataTable({
	processing: true,
	dom: 'Bfrtip',
	buttons: [
		'csv', 'excel', 'pdf', 'print'
	],
    responsive:true,
    scrollX:true,
	ajax: { 
		url : readUrl,
		data : function(param){
			param.kategori = $('[name="kategori"]').val()
		}
	},
	language: {
		loadingRecords: '&nbsp;',
		processing: 'Tunggu sedang proses..'
	},      
    columnDefs: [{
        searcable: false,
        orderable: false,
        targets: 0
	}],
    order: [
        [6, "asc"]
    ],
    columns: [{
        data: "no"
    }, {
        data: "kategori"
    }, {
        data: "barcode"
    }, {
        data: "nama_produk"
    }, {
        data: "total_stok_masuk"
	}, {
		data : "total_stok_keluar"
	},{
		data : "stok_gudang"
	}]
});

function reloadTable() {
    tableData.ajax.reload()
}

$("#kategori").select2({
    placeholder: "Pilih",
    ajax: {
        url: kategoriUrl,
        type: "post",
        dataType: "json",
        data: paras => ({
            satuan: paras.term
        }),
        processResults: data => ({
            results: data
        }),
        cache: true
    }
});

tableData.on("order.dt search.dt", () => {
    tableData.column(0, {
        search: "applied",
        order: "applied"
    }).nodes().each((el, val) => {
        el.innerHTML = val + 1
    })
});

$(".modal").on("hidden.bs.modal", () => {
    $("#form")[0].reset();
    $("#form").validate().resetForm()
});
$(".modal").on("show.bs.modal", () => {
    let a = moment().format("D-MM-Y H:mm:ss");
    $("#tanggal").val(a)
});

function changeKategori(){
	reloadTable();
}
