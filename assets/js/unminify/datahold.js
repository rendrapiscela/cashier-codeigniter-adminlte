let url, datahold = $("#transaksihold").DataTable({
    responsive: true,
    scrollX: true,
    ajax: readUrl,
    columnDefs: [{
        searcable: false,
        orderable: true,
        targets: 0
    }],
    columns: [
	{
		data : 'no'
	}, 
	{
        data: "tanggal"
    }, {
        data: "nota"
    }, {
        data: "nama_produk"
    }, {
        data: "qty"
    }, {
        data: "action"
    }]
});

function reloadTable() {
    datahold.ajax.reload()
}

function remove(id) {
    Swal.fire({
        title: "Hapus",
        text: "Hapus data transaksi ini?",
		type: "warning",
		cancelButtonColor: '#d33',
        showCancelButton: true
    }).then((result) => {
		if (result.value === true) {
			$.ajax({
				url: deleteUrl,
				type: "post",
				dataType: "json",
				data: {
					id: id
				},
				success: a => {
					Swal.fire("Sukses", "Sukses Menghapus Data", "success");
					reloadTable()
				},
				error: a => {
					console.log(a)
				}
			})
		}
    })
}

function lanjutTransaksi(id) {
    Swal.fire({
        title: "Notif",
        text: "Apakah akan melanjutkan transaksi ini ?",
		type: "warning",
		cancelButtonColor: '#d33',
        showCancelButton: true
    }).then((result) => {
		if (result.value === true) {
			window.location.href = 'edit_transaksi_hold/'+id;
		}
    })
}


